**PrimTux**
===========
[PrimTux] (http://primtux.fr) est une distribution éducative basée sur Debian Jessie adaptée à **l'école primaire**.

- 4 utilisateurs et 4 environnements.
- Un menu d'accueil permet de finaliser son installation et de trouver les tutos nécessaires à l'utilisation de la distribution.
- [Filtrage intégré] (http://wiki.primtux.fr/doku.php/filtrage)
- Lanceurs de logiciels éducatifs: handymenu-primtux de base, maternelle, CP-CE1, CE2-CM1-CM2.
- Panneau d'administration des applications
- Panneau d'installation des logiciels non-libres.

----------

- Version live **(mot de passe: tuxprof pour l'ouverture de session administrateur en live, pas de mot de passe pour les sessions élèves)** et installable sur systèmes PAE et non PAE
- Base: Debian Stretch
- Bureau: fluxbox + rox + xfce4 panel + lxpanel.

----------

**- Logiciels de base inclus (liste non-exhaustive):**

- Bureautique : Libreoffice (Traitement de texte, tableur) avec une interface adpatée à chaque niveau (libreoffice des écoles), dictionnaire (qdictionnaire), Agenda (Osmo), annotateur de fichiers pdf (Xournal)
- Graphisme : Visionneuse d'images, Editeur d'images, Capture d'écran, Explorateur d'images.
- Internet : Navigateur internet, filtrage intégré, Explorateur ftp.
- Son et vidéo : Éditeur de fichiers son (Audacity), Convertisseur de fichiers vidéo, Convertisseur de fichiers audio, VLC (lecteur de fichiers audio-vidéo), gmplayer (lecteur vidéo), Logiciel de gravure, Logiciel de montage vidéo
- De nombreux outils de configuration du système

**- Logiciels éducatifs inclus (liste non-exhaustive):**

- Lecture : Aller, Associations, imageo.
- Calcul : à nous les nombres, calcul mental, calcul réfléchi, calculatrice, calculette capricieuse, contour, fukubi, la course aux nombres, le nombre cible, opérations, problèmes, suites, suites arithmétiques, tierce, TuxMath.
- Clavier-souris : Jnavigue , mulot, Pysycache, klettres.
- Compilations : Childsplay, GCompris, Omnitux, pysiogame.
- Dessin : Tux Paint...
- Géométrie-Logique : chemin, epi: labyrinthe, labyrinthe caché, comparaison, piles, symcolor, tangrams (gtans).
- Sciences : Stellarium, microscope virtuel, Scratch.
- Jeux: blobby volley, frozen-bubble, Hannah's horse, monsterz, Mr Patate, ri-li, seahorse adventures, supertux.
- Pour le maître: pylote (logiciel pour TBI), l'administration de tuxpaint, de GCompris, de Pysycache.
- Utilitaires: Après installation de PrimTux sur disque dur, mise à jour, installation de Flash et Java, personnalisation, installation de logiciels non-libres, Systemback permet de copier PrimTux sur un autre disque dur.

**- Primtux peut être agrémentée de nombreux logiciels libres et non-libres (liste non-exhaustive):**  
- Outils: Oracle Java et Free, libdvdcss, photofiltre, xnview, les polices écoles, ardora, webstrict, systemback.
- Éducatifs: activités Jclic, raconte-moi, clicmenu, le matou matheux, la course aux nombres, kiwix-vikidia, je lis avec biba, les exoos d'aleccor, calculatice, chewingword, le conjugueur, atlas Houot, exercices Beaunis et free, solitaire, primaths, lettergames... 

----------

**- Poids:** 3,3 go / 9 go installée - **Consommation RAM:** 250 Mo au chargement. 768 Mo de ram (et swap de 1.5 fois la ram, habituel sous linux) sont conseillés pour la faire tourner correctement.

----------

**Support :**

- <http://primtux.fr>
- <http://forum.primtux.fr>
- <http://wiki.primtux.fr>
- <http://wiki.primtux.fr/doku.php/primtux2>
- <http://ressources.primtux.fr>
- Sources de la distribution: <https://git.framasoft.org/Steph/primtux3>
- Page Debian Derivative de PrimTux: <https://wiki.debian.org/Derivatives/Census/PrimTux>
- Page Distrowatch de PrimTux: <http://distrowatch.com/primtux>

----------

**Changelog :**
### 2018-10-26 PrimTux4


- Toutes les versions se nomment désormais PrimTux4:
  - PrimTux2 devient PrimTux4-Debian8-i586-i686
  - PrimTux3 devient PrimTux4-Debian9-i686/amd64-DG
  - La version incluant le contrôle parental le plus évolué se nomme PrimTux4-Debian9-i686/amd64-CTP
- Un script de mise à jour permet de passer de Primtux 2 et 3 à PrimTux4 8 ou 9 DG
- Passage à 2 versions différentes de contrôle parental:
  - PrimTux4-Debian9-CTP: e2guardian remplace dansguardian, privoxy remplace tinyproxy. Le même navigateur est utilisé pour tous car le filtrage se fait au niveau utilisateur en mode transparent, le https est filtré, des plages horaires d'utilisation peuvent être appliquées. Un processeur multicoeur est dans ce cas conseillé.
  - PrimTux4-Debian9(8)-DG: dansguardian et tinyproxy sont utilisés, le https est filtré par le proxy.
- Nouvelle interface de Libreoffice des écoles
- Uniformisation des bureaux
- Réécriture des handymenus
- Le papier peint de mothsart devient le fond d'écran des différentes sessions. Il améliore la cohérence graphique de PrimTux.
- Mise à jour de l'accueil
- Le gestionnaire de logiciels est de retour
- Calcul@tice passe à la dernière version sans serveur
- Gnome-calculator remplace Qalculate!
- Openboard remplace Open Sankoré
- freiOr-plugins intégrés pour openshot
- Géotortue est intégré
- Toutenclic est intégré en français
- Remplacement du curseur de souris
- Suppression du serveur Lampp (PrimTux4 CTP)
- Suppression des fichiers de l'historique de LibreOffice
- Les ods sont ouverts avec libreofficecalc
- Scratch2 maximisé
- Polices de Raconte-moi corrigées
- Pysiogame au centre
- Firefox devient firefox-esr dans les raccourcis
### 2018-05-04 PrimTux3

- Gnome-calculator remplace Qalculate!
- Toutenclic en français
- Scratch2 maximisé
- Démarrage automatique du serveur lampp
- Polices de Raconte-moi corrigées

### 2018-05-01 PrimTux3

- Pysiogame au centre
- Toutenclic intégré
- Les ods sont ouverts avec libreofficecalc
- Configurateur de grub supprimé
- FreiOr-plugins intégrés pour openshot
- Correction de la position des fenêtres d'abuledu-suites
- Les wallpapers de mothsart sont utilisés par défaut
- Openboard remplace Open Sankoré
- Géotortue est intégré
- Uniformisation des bureaux

### 2017-11-18 PrimTux3

- Ajout de drivers vidéo

### 2017-11-02 PrimTux3

- Un menu des applications éditable par l'administrateur est disponible sous super et maxi
- Changement des wallpapers (monuments de France)
- Openboard remplace Open-sankoré
- Le répertoire /home de chaque session élève est accessible en session administrateur
- Ajout de tuxguitar, Krita, Xpaint, Musescore, Exelearning, Geonext, Wordsearchcreator

### 2017-01-01 PrimTux2 - mise à jour

- nouvelle version de Firefox plus rapide au démarrage
- kde-l10n-fr pour avoir monsieur Patate en français
- ntp pour avoir l'heure synchronisée avec un serveur de temps
- correctifs : les handymenus peuvent se fermer après exécution
- documentation à jour
- nouvelle version de lampp: intégration du php7

### 2016-10-28 PrimTux2-officielle

- Préférences firefox cachées aux élèves
- systemback en français


### 2016-10-22 PrimTux2

- Pour obtenir une version totalement libre, enlever la partie des firmware non-free de /config/package-lists/primtux.list.chroot
- Version GPL (sauf firmware propriétaires) avec panneau d'installation des logiciels non-free
- Retour à une configuration multi-utilisateurs
- Changement de la charte graphique
- Connexion des élèves sans mot de passe, prof avec mot de passe
- Apparition d'un panneau de contrôle des élèves par le prof (contrôle parental, verrouillage des bureaux, contrôle à distance, paramétrage des handymenus, réglages des applications qui le permettent)
- Les abuledu "réglables par utilisateur" sont dans le répertoire home de chaque utilisateur, les réglages sont possibles par cycle.
- Les mots de passe sont pré-enregistrés ("tuxprof", peut être changé via le panneau d'accueil)
- Activation par défaut du pavé numérique
- Seamonkey est le navigateur admin non filtré
- Qwant junior est le moteur de recherche par défaut des élèves
- Nouvelle application pour l'autologin
- Changement de couleur des logo mini et super des handymenus
- Barres des tâches en couleur
- Passage du menu prof entièrement en français
- Un répertoire "Public" permet de partager des documents entre prof et élèves


### 2016-08-12-Beta1 (nom de code à définir)


- Retour à une configuration multi-utilisateurs
- Changement de la charte graphique
- Connexion des élèves sans mot de passe, prof avec mot de passe
- Post-installation simplifiée
- Apparition d'un panneau de contrôle des élèves par le prof (contrôle parental, verrouillage des bureaux, contrôle à distance, paramétrage des handymenus, réglages des applications qui le permettent)
- Les abuledu "réglables par utilisateur" sont dans le répertoire home de chaque utilisateur, les réglages sont possibles par cycle.
- Les mots de passe sont pré-enregistrés ("tuxmaitre", peut être changé via le panneau d'accueil)
- Activation par défaut du pavé numérique
- Seamonkey est le navigateur admin non filtré
- Qwant junior est le moteur de recherche par défaut des élèves
- Nouvelle application pour l'autologin
- Changement de couleur des logo mini et super des handymenus
- Barres des tâches en couleur
- Passage du menu prof entièrement en français
- Un répertoire "Public" permet de partager des documents entre prof et élèves

### 2016-04-02 PrimTux-Eiffel

- Correction de bugs divers (abuledu aller, matou matheux... merci D52fr).
- Proxy-protect corrigé et accessible via mot de passe en live.
- Firefox remplace Iceweasel.

### 2016-03-05 PrimTux-Eiffel RC2

- Les clicmenus sont intégrés, mais veillez à installer les dépendances (onglet installation de l'accueil) pour les faire fonctionner.
- La configuration des handymenus est accessible avec le mot de passe root.
- Iceweasel remplace seamonkey.
- BNE supprimé.
- Changements de couleur du lxpanel pour chaque bureau.
- L'accueil est un handymenu.
- Menu principal réorganisé.
- Bugs Abuledu Aller corrigés.
- Intégration du proxy-protect qui empêche la mofidification des paramètres proxy du navigateur (merci Philippe).
	
### 2016-02-20 PrimTux-Eiffel RC

- Classement plus fin (lecture, calcul...) des logiciels éducatifs (merci Vincent).
- Fusion du sélecteur de bureau et du choix des lanceurs au démarrage de session.
- Ajout de tuxpaint-config.
- Mise à jour des droits de rox (merci Cep).
- Mise à jour de la gestion des sources (merci Cep).
- Installation automatisée de grub en simple et multiboot.

### 2016-02-18 PrimTux-Eiffel Beta 2

- Le système est désormais compilé à l'aide de live-build, ce qui facilite son installation, plus classique et moins déroutante que systemback.
- Un utilisateur, mais 4 environnements, ce qui permet un allègement du poids total.
- Un sélecteur de bureau permet de choisir l'environnement adapté à l'âge de l'utilisateur.
- Un menu d'accueil permet de finaliser son installation et de trouver les tutos nécessaire à l'utilisation de la distribution.
- Le réseau est configuré automatiquement.
- Les mêmes applications que PrimTux Liberté, sauf Open Sankoré, qui sera disponible dans les dépôts.
- Ajout de Gimp, avec le mode fenêtre unique par défaut.
- Des pilotes d'imprimantes et des greffons multimédias ont été ajoutés.
- Le gestionnaire de paquets synaptic se recharge à son démarrage.
- Filtrage intégré
- Gestionnaire de logiciels de solydx
- 2 lanceurs de logiciels éducatifs: Le BNE-linux et handymenu-primtux.

### 2015-10-20

- un changement de thème fluxbox
- un script permet d'ajouter/enlever handymenu, le panel, pcmanfm au démarrage de session
- un changement de curseur de souris
- un changement de wallpaper (avec le dernier Primaud)
- handymenu seul au démarrage de mini sans panel
- handymenu réorganisé avec une page d'accueil pour y mettre ce qui est prioritaire pour une période
- les favoris de PrimTux dans les marque-pages de seamonkey prof
- possibilité de lancer pcmanfm sous prof en tant qu'un autre utilisateur
- handymenu-configuration: le bouton "trouver une application" ouvre le dossier applications de rox
- filtrage inclus et fonctionnel (il faudra juste installer le configurateur pour personnaliser) => passage à 150 mo de ram au démarrage
- Monsieur Patate de KDE à la place de celui d'Abuledu
- mini, super et maxi ne peuvent rien lancer en tant que super-utilisateur

### 2015-09-26

- Passage à Debian 8.2
- Intégration du handymenu-primtux
- Wine et les exe supplémentaires nécessaires au fonctionnement de clicmenu et raconte-moi sont intégrés à tous les utilisateurs.
- Ajout de synapse, lanceur d'applications sémantique
- Problèmes d'association de fichiers corrigés
- Dépôt Multisystem ajouté
- Lecture multimedia sous open-sankoré corrigée
- Gigolo (explorateur réseau) intégré
- Choix du clavier intégré
- lxpanel est à gauche et masqué
- La barre de réduction des fenêtres est en bas
- Grandes icônes pour tous les utilisateurs   


### 2015-08-01

- Libreoffice avec l'interface libreoffice école remplace ooo4kids
- Insertion multimedia d'open-sankoré corrigée
- Intégration d'Osmo (organiseur personnel), de Xournal (annotation de fichiers pdf), de Xnview (explorateur, éditeur de fichiers graphiques), de Openshot (éditeur vidéo).
- Retrait de pepit (pour alléger l'iso de base)
- Ajout du dépôt pour PrimTux

### 2015-06-30

**Première mise à jour**

- Libreoffice avec l'interface http://libreofficedesecoles.free.fr/ remplace
ooo4kids.
- Ajout d'un raccourci lxpanel pour configurer le réseau en session prof.
- Correction du phonon gstreamer pour open sankoré.
- Correction des associations de fichiers html.
- Simple-clic pour toutes les sessions.
- Mode d'iconification de la barre des taches à gauche plutôt que relatif.
- Ajout de l'outil "system-config-samba" qui permet de partager facilement des dossiers dans un réseau local.	

### 2015-06-10

- **Compatible PAE ET NON PAE**	

### 2015-06-08

- Version majeure **NON COMPATIBLE NON PAE**
- correction de bugs mineurs (bouton de configuration dans lxpanel, login)
- passage à "prof" au lieu de "quidam" pour la connexion maître, les mots de passe restent les mêmes
- agrandissement des icônes dans toutes les sessions
- Tuxs classés par niveau au login
- Ajout du bouton de fermeture de session

### 2015-06-03

- ooo4kids => ajout des extensions Lirecouleur et PicoSvoxOOo et python 2.5
- google no-porn filtre le web
- Login sans mot de passe
- Ajout du support imprimantes
- Amélioration du bureau mini
- Ajout de goldendict et du conjugeur
- lighdm assure la connexion	
	
### 2015-05-22
	
- Ajout de PrimTux-Light, gravable sur CDROM (690 Mo).
- Ajout des paquets éducatifs PrimTux
	
### 2015-05-09
	
- Intégration de lxpanel en tant que dock lanceur d'applications
- gnucap installé
- mise à jour des dépôts de debian Jessie vers Debian stable
	
### 2015-04-05

- Installation modifiée de flashplayer et de java, en passant par l'installateur d'applications
- Mise à jour du lien "Traitement de texte" dans le menu principal.

### 2015-05-01

- le son est fonctionnel dans gcompris
- hotkeys de l'eeepc intégré
- le menu démarrer de fluxbox se réduit au clic sur le bureau
- changement de système pour changer de papier peint (tuto à venir)
- passage de open-jre à oracle java 8
- intégration de klettres
- xscreensaver s'occupe de la gestion d'énergie

### 2015-04-26

- Intégration de OOO4Kids
- Tests en cours sur l'intégration de Kiwix

### 2015-04-24

- Associations est revenu en version 4 sans mot de passe
- 4 utilisateurs peuvent se connecter en version live avec un environnement adapté : **mini (mot de passe : tuxmini), super (mot de passe : tuxsuper), maxi (mot de passe : tuxmaxi), quidam (mot de passe : tuxmaitre), on doit parfois utiliser le mot de passe « root » pour des taches d'administration**.
- Ajout des catégories Arts et Découverte du monde
- Ajout de pepit, matou matheux, exercices free, atlas Houot, exercices Beaunis, microscope virtuel, scratch, pylote, géotortue, le jeu du pendu, le mot le plus long
- Seamonkey remplace Opera
- Raccourcis éditables selon le niveau de l'utilisateur (application ezame, voir tuto)
- « Packs » utilisateurs pour personnaliser une version installée (voir tuto)

### 2015/04/12

- Associations est repassé en version 3.0 pour éviter d'entrer un mot de passe pour le démarrer et éditer.
- Nouveau thème d'icônes pour les marque-pages.
- Intégration de drivers commun et des dépôts non-free et contrib de debian jessie.
- Retrait de à nous les nombres, calculs, tableaux (on a mieux dans d'autres logiciels intégrés et ils sont vraiment anciens).
- Ajout de logiciels: epi: labyrinthe, labyrinthe caché, comparaison, piles / Stellarium / Informations système.
- Ajout des catégories sciences et logique.
- Bootsplash personnalisé en version live.
- Augmentation de la taille des icônes, vue miniature par défaut dans pcmanfm.
- Montage des périphériques internes dans pcmanfm sans message d'erreur, mais après avoir entré le mot de passe / non nécessaire par le gestionnaire avancé spacefm, inclus).
- Autologin, le mot de passe reste le même.
- Toutes les fenêtres n'étaient pas réduites dans la barre des taches, problème résolu.
- Ajout des thèmes de fenêtres Murrine et Pixbuf.



### 2015/04/11

- Open sankore est fonctionnel (merci Tomasi).
- Flash installé.
- L'aperçu dans Ardora s'ouvre avec Opera.
- Plus besoin de se connecter en root pour installer.
- Les librairies Jclic sont supprimées, seul Jclic reste, il y a tellement d'activités existantes sur le net qu'il est préférable que l'utilisateur installe ce qui lui convient, l'iso s'en trouve allégée et on peut de ce fait enregistrer les bibliothèques dans le répertoire utilisateur. On évite aussi les problèmes de droits.

