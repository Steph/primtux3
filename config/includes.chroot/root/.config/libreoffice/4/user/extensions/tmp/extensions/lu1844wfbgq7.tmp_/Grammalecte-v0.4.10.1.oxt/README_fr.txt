_______________________________________________________________________________

    GRAMMALECTE
    Correcteur grammatical pour le français
    version 0.4.10.1
_______________________________________________________________________________

    Basé sur Lightproof grammar checker extension generator
        de László Németh
        http://numbertext.org/lightproof/

    Modifications de Lightproof et règles grammaticales :
        Olivier R. - dicollecte<at>free<dot>fr
        Dicollecte : http://www.dicollecte.org/

        Certaines des règles grammaticales sont reprises du correcteur
        LanguageTool, notamment celles de Dominique Pellé.

    Licence :
        GPL : GNU General Public License
        version 3 ou supérieure -- http://www.gnu.org/licenses/gpl-3.0.html

    Ce correcteur emploie le dictionnaire Hunspell créé par Dicollecte :
    http://www.dicollecte.org/home.php?prj=fr
