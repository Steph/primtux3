############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editordinal.tcl,v 1.1.1.1 2004/04/16 11:45:44 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome

set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"

global arg
set arg [lindex $argv 0]



. configure -background white
frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left

#variables
#################################################g�n�ration interface
proc interface { what} {
global sysFont listanimaux listhabitat arg compteligne typexo

set compteligne 1
set ext .gif
image create photo wagon80 -file [file join images wagon80$ext]


catch {destroy .geneframe}

catch {destroy .leftframe}

frame .leftframe -background white -height 480 -width 100 
pack .leftframe -side left -anchor n

frame .geneframe -background white -height 300 -width 200
pack .geneframe -side left -anchor n
label .leftframe.lignevert1 -bg red -text "" -font {Arial 1}
pack .leftframe.lignevert1 -side right -expand 1 -fill y

######################leftframe.frame1
frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side top 

label .leftframe.frame1.lab1 -foreground red -bg white -text [mc $arg] -font $sysFont(l)
pack .leftframe.frame1.lab1 -side top -fill x -pady 5

label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Scenario}] -font $sysFont(l)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 15 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame1.listsce <ButtonRelease-1> "changelistsce %x %y"

############################leftframe.frame2
frame .leftframe.frame2 -background white -width 100 
pack .leftframe.frame2 -side top 
button .leftframe.frame2.but0 -text [mc {Nouveau}] -command "additem" -activebackground white
pack .leftframe.frame2.but0 -pady 5 -side top
button .leftframe.frame2.but1 -text [mc {Supprimer}] -command "delitem" -activebackground white
pack .leftframe.frame2.but1 -pady 5 -side top


########################leftframe.frame3
frame .leftframe.frame3 -background white -width 100
pack .leftframe.frame3 -side top 
label .leftframe.frame3.lab0 -foreground white -bg blue -text [mc {    Type    }] -font $sysFont(l)
pack .leftframe.frame3.lab0 -pady 5 -side top -fill x -expand 1

radiobutton .leftframe.frame3.rbcommande0 -text [mc {Modele visible}] -bg white  -variable typeexo -value 0 -font $sysFont(s)
pack .leftframe.frame3.rbcommande0 -side top
radiobutton .leftframe.frame3.rbcommande1 -text [mc {Modele cache}] -bg white  -variable typeexo -value 1 -font $sysFont(s)
pack .leftframe.frame3.rbcommande1 -side top


button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white
pack .leftframe.frame3.but0 -side bottom

##########################################################################################################
set tmp 3
for {set i 0} {$i < $compteligne} {incr i 1} {
  label .geneframe.labll$i -bg red -text "" -font {Arial 1}
  grid .geneframe.labll$i -column 0 -row $tmp -columnspan 6 -sticky "w e"
  incr tmp 2 

}

#############colonne wagons
  label .geneframe.lab0_0 -bg white -text [mc {Wagons}] -font $sysFont(l)
  grid .geneframe.lab0_0 -column 0 -row 0 -sticky "w e"
  label .geneframe.labl1 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl1 -column 0 -row 1 -columnspan 6 -sticky "w e"
set tmp 2
for {set i 0} {$i < $compteligne} {incr i 1} {
  label .geneframe.btnwagon$i -image wagon80 -activebackground grey 

 grid .geneframe.btnwagon$i -column 0 -row $tmp
  incr tmp 2
}

################colonne nombre
  label .geneframe.lab3_0 -bg #78F8FF -text [mc {Nombre (Min Max)}] -font $sysFont(l)
  grid .geneframe.lab3_0 -column 1 -row 0 -sticky "w e" -columnspan 2

set tmp 2
for {set i 0} {$i < $compteligne} {incr i 1} {
  scale .geneframe.scale$i -orient horizontal -length 80 -from 5 -to 15 -tickinterval 10 -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s)
  grid .geneframe.scale$i -column 1 -row $tmp -sticky "w e"
  bind .geneframe.scale$i <ButtonRelease-1> "changescalemin $i"
  scale .geneframe.scale$i$i -orient horizontal -length 80 -from 5 -to 15 -tickinterval 10 -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s)
  grid .geneframe.scale$i$i -column 2 -row $tmp -sticky "w e"
  bind .geneframe.scale$i$i <ButtonRelease-1> "changescalemax $i" 
  incr tmp 2
}


###############colonne D�j� present
#  label .geneframe.lab1_0 -bg white -text "D�j� pr�sents" -font $sysFont(l)
#  grid .geneframe.lab1_0 -column 3 -row 0 -columnspan 2 -sticky "w e"
#set tmp 2
#for {set i 0} {$i < $compteligne} {incr i 1} {
#  checkbutton .geneframe.rb$i -text "D�j� pr�sents" -bg white -variable dejap$i 
#  grid .geneframe.rb$i -column 3 -row $tmp  -sticky "w e"
#  checkbutton .geneframe.rbb$i -text "Pr�s des bornes" -bg white -variable borne$i  
#  grid .geneframe.rbb$i -column 4 -row $tmp  -sticky "w e"
#incr tmp 2
#}

#################################sp�cifiques trains


  checkbutton .geneframe.ckinverse -bg white  -variable posinverse -text [mc {Convoi inverse}]
  grid .geneframe.ckinverse -column 0 -row $tmp  -sticky "w n s e" 
##################################################"
 charge 0
}


####################################################################"""
proc charge {index} {
global listdata activelist indexpage totalpage arg compteligne Home
variable typeexo
variable posinverse
#variable correction
#variable commandefenetre
#variable pauseauto




set ext .conf
for {set i 0} {$i < $compteligne} {incr i 1} {
.geneframe.scale$i set 0
.geneframe.scale$i$i set 0
}

set f [open [file join $Home reglages $arg$ext] "r"]
set listdata [gets $f]
close $f


set totalpage [llength $listdata] 
set indexpage $index
	.leftframe.frame1.listsce delete 0 end
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.leftframe.frame1.listsce insert end [lindex [lindex [lindex $listdata $k] 0] 0]
	}
set activelist [lindex $listdata $indexpage]
.leftframe.frame1.listsce selection set $indexpage
	set typeexo [lindex [lindex $activelist 0] 1]
################################################################################modifier

	.geneframe.scale00 set [lindex [lindex $activelist $i] 1]
	.geneframe.scale0 set [lindex [lindex $activelist $i] 0]
######################################################################
	set posinverse [lindex [lindex $activelist 0] 2]
}



###################################################################################"
proc enregistre_sce {} {
global indexpage listdata activelist totalpage arg compteligne Home
set ext .conf
variable typeexo
variable posinverse
set activelist \173[.leftframe.frame1.listsce get $indexpage]\040$typeexo\040$posinverse\175
lappend activelist [.geneframe.scale0 get]\040[.geneframe.scale00 get]
set listdata [lreplace $listdata $indexpage $indexpage $activelist]
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f
}


###################################################################
proc changescalemin {min} {
if {[.geneframe.scale$min get] > [.geneframe.scale$min$min get] } { 
.geneframe.scale$min$min set [.geneframe.scale$min get] 
}
}

proc changescalemax {max} {
if {[.geneframe.scale$max$max get] < [.geneframe.scale$max get] } { 
.geneframe.scale$max set [.geneframe.scale$max$max get] 
}
}


proc changelistsce {x y} {
enregistre_sce
set ind [.leftframe.frame1.listsce index @$x,$y]
charge $ind
}

proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global listdata indexpage totalpage arg Home
set ext .conf
	if {$totalpage < 2} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [mc $arg]
	return
	}
set response [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer la fiche %1$s ?}] [lindex [lindex [lindex $listdata $indexpage] 0] 0]] -type yesno -title [mc $arg]]
#set response [tk_messageBox -message "Voulez-vous vraiment supprimer la fiche [lindex [lindex [lindex $listdata $indexpage] 0] 0]?" -type yesno -title $arg]
	if {$response == "yes"} {
	set totalpage [expr $totalpage - 1]
		set listdata [lreplace $listdata $indexpage $indexpage]
     		if {$indexpage > 0} {
     		set indexpage [expr $indexpage - 1]						
		} 
	set f [open [file join $Home reglages $arg$ext] "w"]
	puts $f $listdata
	close $f
	charge $indexpage

	} 

}


proc additem {} {
global listdata indexpage totalpage 
enregistre_sce
catch {destroy .nomobj}
toplevel .nomobj -background grey -width 250 -height 100
wm geometry .nomobj +50+50
frame .nomobj.frame -background grey -width 250 -height 100
pack .nomobj.frame -side top
label .nomobj.frame.labobj -font {Helvetica 10} -text [mc {Nom du scenario :}] -background grey
pack .nomobj.frame.labobj -side top 
entry .nomobj.frame.entobj -font {Helvetica 10} -width 10
pack .nomobj.frame.entobj -side top 
button .nomobj.frame.ok -background gray75 -text [mc {Ok}] -command "verifnomobj"
pack .nomobj.frame.ok -side top -pady 10
}

proc verifnomobj {} {
global nom listdata indexpage totalpage arg tabcoulwagon compteligne Home
set ext .conf
set nom [join [.nomobj.frame.entobj get] ""]
if {$nom !=""} {
set indexpage $totalpage
incr totalpage
lappend listdata \173$nom\0400\0400\175\040\1735\0405\175
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f
charge $indexpage
}
catch {destroy .nomobj}
}


interface $arg
