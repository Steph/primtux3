#!/bin/sh
# chemin_noeud_code_immediat.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : chemin_noeud_code_immediat.tcl
#  Author  : Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: chemin_noeud_code_immediat.tcl,v 1.12 2006/03/27 13:13:15 abuledu_andre Exp $
#  @author     Andr� Connes
#  @modifier
#  @project    Le terrier
#  @copyright  Andr� Connes
#
#***********************************************************************
global sysFont glob  categorie

set categorie "noeud_code_immediat"


source chemin.conf
source msg.tcl
source fonts.tcl
source eval.tcl

  #
  # langue par d�faut
  #
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]
  #
  # couleurs des cases
  #
  set f [open [file join $glob(home_reglages) couleurs.conf] "r"]
  gets $f vcouleurs
  close $f
  if { $vcouleurs == "Daltonien" } {
    set glob(celcolor) $glob(celDaltonien)
    set glob(doncolor) $glob(donDaltonien)
    set glob(errcolor) $glob(errDaltonien)
  } else {
    set glob(celcolor) $glob(celNonDaltonien)
    set glob(doncolor) $glob(donNonDaltonien)
    set glob(errcolor) $glob(errNonDaltonien)
  }   

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.89)]x[expr int([winfo vrootheight .]*0.8)]+0+0
. configure -background $glob(bgcolor)
wm title . "[mc {Codage_immediat}]"

################################################

proc recommencer { } {
  global glob
  set glob(boucle) 0
  for {set i 1} {$i <= $glob(bouclemax)} {incr i 1} {
    .bframe.tete$i configure -image pneutre -width 80
  }
  .bframe.but_gauche configure -state disable
}                    

proc init_dir_exos {} {
  global glob
  catch {set f [open [file join $glob(home_reglages) dir_exos.conf] "r"]}
  set glob(dir_exos) [gets $f]
  close $f
  if { $glob(dir_exos) == "Commun" } {
    set glob(repert) [file join grilles]
  } else {
    set glob(repert) [file join $glob(home_chemin) grilles]
  }
}

proc verifier_vent_suivant {fc clic_vent} {
  global fb glob
  set k $glob(case_suivante)
  set yo [lindex $glob(trace) [expr 2*$k-2]]
  set xo [lindex $glob(trace) [expr 2*$k-1]]
  set yn [lindex $glob(trace) [expr 2*$k]]
  set xn [lindex $glob(trace) [expr 2*$k+1]]
  if { [expr $xn-$xo] == 1 } {
        set vent "est"
  }
  if { [expr $xn-$xo] == -1 } {
        set vent "ouest"
  }
  if { [expr $yn-$yo] == 1 } {
        set vent "sud"
  }
  if { [expr $yn-$yo] == -1 } {
        set vent "nord"
  }
  if { $clic_vent == $vent } {
    $fc create oval \
        [expr $glob(org) + $xn*$glob(wlen) +1 -9] [expr $glob(org) + $yn*$glob(hlen) +1 -9] \
        [expr $glob(org) + $xn*$glob(wlen) +1 +9] [expr $glob(org) + $yn*$glob(hlen) +1 +9] \
        -fill $glob(doncolor) -width 1
    set glob($yn,$xn) $glob(doncolor)
    incr glob(case_suivante)
  } else {
    $fc create oval \
        [expr $glob(org) + $xn*$glob(wlen) +1 -9] [expr $glob(org) + $yn*$glob(hlen) +1 -9] \
        [expr $glob(org) + $xn*$glob(wlen) +1 +9] [expr $glob(org) + $yn*$glob(hlen) +1 +9] \
        -fill $glob(errcolor) -width 1
    after 100 {
        set attente_erreur non
    }
    vwait attente_erreur
    $fc create oval \
        [expr $glob(org) + $xn*$glob(wlen) +1 -9] [expr $glob(org) + $yn*$glob(hlen) +1 -9] \
        [expr $glob(org) + $xn*$glob(wlen) +1 +9] [expr $glob(org) + $yn*$glob(hlen) +1 +9] \
        -fill $glob($yn,$xn) -width 1
    incr glob(nberreurs)
  }
  # verifier si termine
  if { [expr 2*$glob(case_suivante)] >= [llength $glob(trace)] } {
    $fb.nord configure -state disable
    $fb.sud configure -state disable
    $fb.est configure -state disable
    $fb.ouest configure -state disable
    bind . <Up> ""
    bind . <Down> ""
    bind . <Left> ""
    bind . <Right> ""
    if { $glob(nberreurs) == 0 } {
      set ptete pbien
    } elseif { $glob(nberreurs) <= $glob(nbepassable) } {
      set ptete ppass
    } else {
      set ptete pmal
    }
    $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
        -image $ptete
    sauver_trace_parcours
    incr glob(boucle)
    .bframe.tete$glob(boucle) configure -image $ptete -width 80
    $fb.consigne configure -state disable
    
      after [expr $glob(attente) * 1000] {
	if { $glob(boucle) < $glob(bouclemax) } {
	  set glob(session_finie) non
	} else {
          set glob(session_finie) oui
	} 
      }
      vwait glob(session_finie)

    if { $glob(session_finie) == "non" } {
      open_and_code
    } else {
      .bframe.but_gauche configure -state normal
      if { $glob(nberreurs) <= 2 } {
        $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
          -image [image create photo -file [file join sysdata sourire.png]]
      } else {
        $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
          -image [image create photo -file [file join sysdata pleurer.png]]
      }
      # exit
    }
  }
} ;# verifier_vent_suivant

##########################################################################
#
  proc open_and_code { } {
#
##########################################################################
  global fc fb glob
  #
  # ouvrir le fichier des cases du chemin
  #

  # charger la liste des chemins du niveau disponibles
  set fnames [glob [file join $glob(repert) *$glob(boucle)_*]] 
  # choisir un chemin au hasard dans cette liste
  set fname [lindex $fnames [expr int(rand()*[llength $fnames])]]
  set glob(trace_fname) [file tail $fname]
  catch {set f [open $fname "r"]}
  catch {set glob(trace) [gets $f]}
  if {[catch {close $f} r]} {
        exit
  }
  # eventuellement renverser ce chemin
  set r [expr rand()]
  if { $r < 0.5 } {
    # renverser le chemin
    set tmp_liste {}
    for { set i 0 } { [expr 2*$i] < [llength $glob(trace)] } { incr i} {
      set tmp_liste [linsert $tmp_liste 0 [lindex $glob(trace) [expr 2*$i+1]]]
      set tmp_liste [linsert $tmp_liste 0 [lindex $glob(trace) [expr 2*$i]]]
    }  
    set glob(trace) $tmp_liste
  }
  # on cree la grille (ATTENTION : i=ligne et j=colonne)
  #
  for {set i 0} {$i < $glob(nbrow)} {incr i 1} {
    for {set j 0} {$j < $glob(nbcol)} {incr j 1} {
      $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
        [expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
        -fill $glob(celcolor) -width 1 ;# -tag rect($i,$j)
    }
  }
  #
  # on cree les noeuds
  #
  for {set i 0} {$i <= $glob(nbrow)} {incr i 1} {
    for {set j 0} {$j <= $glob(nbcol)} {incr j 1} {
      $fc create oval \
        [expr $glob(org) + $j*$glob(wlen) +1 -9] [expr $glob(org) + $i*$glob(hlen) +1 -9] \
        [expr $glob(org) + $j*$glob(wlen) +1 +9] [expr $glob(org) + $i*$glob(hlen) +1 +9] \
        -fill #00ffD0 -width 1
      set glob($i,$j) #00ffD0
    }
  }
  #
  # afficher les noeuds du chemin
  #
  for { set n 1 } { [expr 2*$n] < [llength $glob(trace)] } { incr n } {
    set yn [lindex $glob(trace) [expr 2*$n]]
    set xn [lindex $glob(trace) [expr 2*$n+1]]
    $fc create oval \
        [expr $glob(org) + $xn*$glob(wlen) +1 -9] [expr $glob(org) + $yn*$glob(hlen) +1 -9] \
        [expr $glob(org) + $xn*$glob(wlen) +1 +9] [expr $glob(org) + $yn*$glob(hlen) +1 +9] \
        -fill yellow -width 1
    set glob($yn,$xn) yellow
  }
  #
  # creer le canvas pour le codage
  #
  catch { destroy $fb }
  set fb .frame.b
  canvas $fb -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fb -expand true

  label $fb.consigne \
        -text [mc {choisir_fleche}] -bg $glob(bgcolor)
  grid $fb.consigne -row 0 -column 4
  #
  # placer les vents de direction en bas
  #
  set vent nord
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "verifier_vent_suivant $fc $vent"
  grid $fb.$vent -row 1 -column 6
  set vent ouest
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "verifier_vent_suivant $fc $vent"
  grid $fb.$vent -row 2 -column 5
  set vent est
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "verifier_vent_suivant $fc $vent"
  grid $fb.$vent -row 2 -column 7
  set vent sud
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "verifier_vent_suivant $fc $vent"
  grid $fb.$vent -row 3 -column 6
  #
  # les fleches du clavier
  #
  bind . <Up> "verifier_vent_suivant $fc nord"
  bind . <Down> "verifier_vent_suivant $fc sud"
  bind . <Left> "verifier_vent_suivant $fc ouest"
  bind . <Right> "verifier_vent_suivant $fc est"
  #
  # placer le noeud de depart
  #
  set yn [lindex $glob(trace) 0]
  set xn [lindex $glob(trace) 1]
  $fc create oval \
        [expr $glob(org) + $xn*$glob(wlen) +1 -9] [expr $glob(org) + $yn*$glob(hlen) +1 -9] \
        [expr $glob(org) + $xn*$glob(wlen) +1 +9] [expr $glob(org) + $yn*$glob(hlen) +1 +9] \
        -fill $glob(doncolor) -width 1
  set glob($yn,$xn) $glob(doncolor)
  #
  # on passe la main a l'eleve
  #
  set glob(case_suivante) 1
  set glob(nberreurs) 0
  # top depart
  set glob(heure_debut) [clock seconds]

} ;# open_and_code

########################################################################
#                           programme principal                        #
########################################################################

  bind . <Control-q> {exit}

  # Relire le nom r�gl� de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    catch {set f [open [file join $glob(home_chemin) reglages trace_user] "r"]}
    gets $f glob(trace_user)
    close $f
  }
  
  init_dir_exos

  # creation du frame
  ###################
  . configure -background $glob(bgcolor)
  frame .frame -background $glob(bgcolor) -height $glob(height) -width $glob(width)
  pack .frame -side top -fill both -expand yes

  # creation du canvas
  ####################

  set fc .frame.c
  canvas $fc -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fc -expand true

  # ###########################################################
  # on cree une frame en bas en avant-derniere ligne avec
  #   le score affiche sous forme de tetes (bien passable mal)
  #   un bouton 'continuer'
  # ###########################################################

  frame .bframe -bg $glob(bgcolor)
  pack .bframe -side bottom -expand true

  image create photo pbien -file [file join sysdata pbien.gif] 
  image create photo ppass -file [file join sysdata ppass.gif]
  image create photo pmal -file [file join sysdata pmal.gif]
  image create photo pneutre -file [file join sysdata pneutre.gif]

  for {set i 1} {$i <= $glob(bouclemax)} {incr i 1} {
    label .bframe.tete$i -bg $glob(bgcolor) -width 4
    grid .bframe.tete$i -column [expr $i -1] -row 1 -sticky e
    .bframe.tete$i configure -image pneutre -width 80
  }

  button .bframe.but_gauche -image \
    [image create photo fgauche -file [file join sysdata fgauche.gif]] -command "recommencer; open_and_code"
  grid .bframe.but_gauche -column [expr $glob(bouclemax)+2] -row 1
  .bframe.but_gauche configure -state disable

  button .bframe.but_quitter -image \
    [image create photo fquitter \
    -file [file join sysdata quitter_minus.gif]] \
    -command exit
  grid .bframe.but_quitter  -column [expr $glob(bouclemax)+3] -row 1

open_and_code