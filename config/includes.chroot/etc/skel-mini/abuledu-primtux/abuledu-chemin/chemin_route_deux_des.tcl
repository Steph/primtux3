#!/bin/sh
# chemin_route_deux_des.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : chemin_route_deux_des.tcl
#  Author  : Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: chemin_route_deux_des.tcl,v 1.13 2006/03/27 13:13:16 abuledu_andre Exp $
#  @author     Andr� Connes
#  @modifier
#  @project    Le terrier
#  @copyright  Andr� Connes
#
#***********************************************************************
global sysFont glob  categorie

set categorie "route_deux_des"


source chemin.conf
source msg.tcl
source fonts.tcl
source eval.tcl

  #
  # langue par defaut
  #
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]
  #
  # couleurs des cases
  #
  set f [open [file join $glob(home_reglages) couleurs.conf] "r"]
  gets $f vcouleurs
  close $f
  if { $vcouleurs == "Daltonien" } {
    set glob(celcolor) $glob(celDaltonien)
    set glob(doncolor) $glob(donDaltonien)
    set glob(errcolor) $glob(errDaltonien)
  } else {
    set glob(celcolor) $glob(celNonDaltonien)
    set glob(doncolor) $glob(donNonDaltonien)
    set glob(errcolor) $glob(errNonDaltonien)
  } 

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.89)]x[expr int([winfo vrootheight .]*0.8)]+0+0
. configure -background $glob(bgcolor)
wm title . "[mc {DeuxDes}]"

################################################

proc recommencer { } {
  global glob
  set glob(boucle) 0
  for {set i 1} {$i <= $glob(bouclemax)} {incr i 1} {
    .bframe.tete$i configure -image pneutre -width 80
  }
  .bframe.but_gauche configure -state disable
}

proc init_dir_exos {} {
  global glob
  set f [open [file join $glob(home_reglages) dir_exos.conf] "r"]
  set glob(dir_exos) [gets $f]
  close $f
  if { $glob(dir_exos) == "Commun" } {
    set glob(repert) [file join grilles]
  } else {
    set glob(repert) [file join $glob(home_chemin) grilles]
  }
}

proc sur_route { j i } {
  global glob
  # ajouter sentinelle
  set route $glob(trace)
  lappend route $i $j
  set n -1
  set trouve 0
  while { ! $trouve } {
    incr n
    set xn [lindex $route [expr 2*$n]]
    set yn [lindex $route [expr 2*$n+1]]
    set trouve [expr $yn == $j && $xn == $i]
  }
  return $n

} ;# sur_route

proc verifier_case_suivante {fc x y} {
  global fb glob
  set j [expr int(([$fc canvasx $x]-$glob(org)-1) / $glob(wlen))]
  set i [expr int(([$fc canvasy $y]-$glob(org)-1) / $glob(hlen))]
  set n [expr $glob(case_actuelle) + $glob(constellation)]
  if { $i == [lindex $glob(trace) [expr 2*$n]] && \
       $j == [lindex $glob(trace) [expr 2*$n+1]] } {
    set glob(case_actuelle) $n
    $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
        [expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
        -fill $glob(doncolor) -width 1
    $fc create image \
        [expr $glob(org) + $j*$glob(wlen) +int($glob(wlen)/2) +1] \
        [expr $glob(org) + $i*$glob(hlen) +int($glob(hlen)/2) +1] \
        -image [image create photo -file [file join sysdata [lindex $glob(portions) $n ]]]
    set glob($i,$j) $glob(doncolor)
    # verifier si termine
    if { [expr 2*$glob(case_actuelle)+4] >= [llength $glob(trace)] } {
    if { $glob(nberreurs) == 0 } {
      set ptete pbien
    } elseif { $glob(nberreurs) <= $glob(nbepassable) } {
      set ptete ppass
    } else {
      set ptete pmal
    }
      $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
        -image $ptete
      sauver_trace_parcours
      incr glob(boucle)
      .bframe.tete$glob(boucle) configure -image $ptete -width 80
      $fb.consigne configure -state disable
    
      after [expr $glob(attente) * 1000] {
	if { $glob(boucle) < $glob(bouclemax) } {
	  set glob(session_finie) non
	} else {
          set glob(session_finie) oui
	} 
      }
      vwait glob(session_finie)

      if { $glob(session_finie) == "non" } {
        open_and_deux_des
      } else {
        .bframe.but_gauche configure -state normal
        if { $glob(nberreurs) <= 2 } {
          $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
            -image [image create photo -file [file join sysdata sourire.png]]
        } else {
          $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
            -image [image create photo -file [file join sysdata pleurer.png]]
        }
        # exit
      }

    } else {
      set amas [hasard]
      set glob(constellation1) [lindex $amas 0]
      set glob(constellation2) [lindex $amas 1]
      set glob(constellation) [expr $glob(constellation1)+$glob(constellation2)]
      $fb.d�1 configure \
        -image [image create photo \
         -file [file join sysdata de_$glob(constellation1).png]] \
        -borderwidth 2 -bg $glob(bgcolor)
      $fb.d�2 configure \
        -image [image create photo \
         -file [file join sysdata de_$glob(constellation2).png]] \
        -borderwidth 2 -bg $glob(bgcolor)
    }
  } else {
    $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
        [expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
        -fill $glob(errcolor) -width 1
    after 100 {
        set attente_erreur non
    }
    vwait attente_erreur
    $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
        [expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
        -fill $glob($i,$j) -width 1 -tag rect($i,$j)
    #
    # si sur la route, restaurer la portion
    #
    set n [sur_route $j $i]
    if { [expr 2*$n] < [llength $glob(trace)] } {
      $fc create image \
        [expr $glob(org) + $j*$glob(wlen) +int($glob(wlen)/2) +1] \
        [expr $glob(org) + $i*$glob(hlen) +int($glob(hlen)/2) +1] \
        -image [image create photo -file [file join sysdata [lindex $glob(portions) $n ]]] \
        -tag route($i,$j)
    }

    incr glob(nberreurs)
  }
} ;# verifier_case_suivante


proc hasard {} {
  global glob
  # attention : ne pas d�passer les limites du chemin
  # b = distance au but
  set b [ expr int([llength $glob(trace)]/2) - $glob(case_actuelle) - 2]
  set bmin [expr $b < 12 ? $b : 12]
  # r = random 2..bmin
  set r1 99 ;# bidon
  set r2 99 ;# bidon
  while { [expr $r1+$r2] > $bmin} {
    set r1 [ expr int(rand()*6) + 1 ]
    set r2 [ expr int(rand()*6) + 1 ]
  }
  # 2 <= r1+r2 <= bmin
  set r [expr $r1+$r2]
  if { $r == $b } {
    return [list $r1 $r2]
  }
  if { $r == [expr $b - 1] && $r < 11} {
    if { $r1 < 6 } {
        incr r1
    } else {
        incr r2
    }
    return [list $r1 $r2]
  }
  if { $r == [expr $b - 1] } {
    if { $r > 1 } {
        incr r1 -1
    } else {
        incr r2 -1
    }
  }
  return [list $r1 $r2]
} ;# hasard

proc faire_portions { } {
  global glob
  set glob(portions) ""
  set vent_debut "" ;# caract�re vide avant le d�part
  set yn [lindex $glob(trace) 0]
  set xn [lindex $glob(trace) 1]
  for { set k 0 } { $k < [expr [llength $glob(trace)]/2 -2] } { incr k } {
    set ys [lindex $glob(trace) [expr 2*$k+2]]
    set xs [lindex $glob(trace) [expr 2*$k+3]]
    set dx [expr $xs-$xn]
    set dy [expr $ys-$yn]
    if {$dx == 1 } {
        set vent_fin "e"
    } elseif {$dx ==-1 } {
          set vent_fin "w"
    } elseif {$dy == 1 } {
        set vent_fin "s"
    } elseif {$dy ==-1 } {
        set vent_fin "n"
    }
    #
    # ajouter le nom de l'image de la portion de route
    #
    if { $vent_debut < $vent_fin } {
        lappend glob(portions) chemin_$vent_debut$vent_fin.png
    } else {
        lappend glob(portions) chemin_$vent_fin$vent_debut.png
    }
    set dx [expr -$dx]
    set dy [expr -$dy]
    if {$dx == 1 } {
        set vent_debut "e"
    } elseif {$dx ==-1 } {
          set vent_debut "w"
    } elseif {$dy == 1 } {
        set vent_debut "s"
    } elseif {$dy ==-1 } {
        set vent_debut "n"
    }
    set xn $xs
    set yn $ys
  }
  #
  # traiter l'arrivee
  #
  set vent_fin ""
    #
    # ajouter le nom de l'image de la portion de route
    #
    if { $vent_debut < $vent_fin } {
        lappend glob(portions) chemin_$vent_debut$vent_fin.png
    } else {
        lappend glob(portions) chemin_$vent_fin$vent_debut.png
    }
} ;# faire_portions

##########################################################################
#
  proc open_and_deux_des { } {
#
##########################################################################
  global fc fb glob
  #
  # ouvrir le fichier des cases du chemin
  #

  # charger la liste des chemins du niveau disponibles
  set fnames [glob [file join $glob(repert) *$glob(boucle)_*]] 
  # choisir un chemin au hasard dans cette liste
  set fname [lindex $fnames [expr int(rand()*[llength $fnames])]]
  set glob(trace_fname) [file tail $fname]
  catch {set f [open $fname "r"]}
  catch {set glob(trace) [gets $f]}
  if {[catch {close $f} r]} {
        exit
  }
  # eventuellement renverser ce chemin
  set r [expr rand()]
  if { $r < 0.5 } {
    # renverser le chemin
    set tmp_liste {}
    for { set i 0 } { [expr 2*$i] < [llength $glob(trace)] } { incr i} {
      set tmp_liste [linsert $tmp_liste 0 [lindex $glob(trace) [expr 2*$i+1]]]
      set tmp_liste [linsert $tmp_liste 0 [lindex $glob(trace) [expr 2*$i]]]
    }  
    set glob(trace) $tmp_liste
  }
  # on cree la grille (ATTENTION : i=ligne et j=colonne)
  #
  for {set i 0} {$i < $glob(nbrow)} {incr i 1} {
    for {set j 0} {$j < $glob(nbcol)} {incr j 1} {
      $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
        [expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
        -fill $glob(celcolor) -width 1 -tag rect($i,$j)
      set glob($i,$j) $glob(celcolor)
      $fc bind rect($i,$j) <1> {verifier_case_suivante $fc %x %y}
    }
  }
  #
  # lister les portions de la route : renvoie glob(portions)
  #
  faire_portions
  #
  # afficher les cases du chemin
  #
  for { set n 0 } { [expr 2*$n+2] < [llength $glob(trace)] } { incr n } {
    set yn [lindex $glob(trace) [expr 2*$n]]
    set xn [lindex $glob(trace) [expr 2*$n+1]]
    $fc create image \
        [expr $glob(org) + $xn*$glob(wlen) +int($glob(wlen)/2) +1] \
        [expr $glob(org) + $yn*$glob(hlen) +int($glob(hlen)/2) +1] \
        -image [image create photo -file [file join sysdata [lindex $glob(portions) $n ]]] \
        -tag route($yn,$xn)
    $fc bind route($yn,$xn) <1> {verifier_case_suivante $fc %x %y}
  }
  #
  # creer le canvas pour le codage
  #
  catch { destroy $fb }
  set fb .frame.b
  canvas $fb -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fb -expand true

  label $fb.consigne \
        -text [mc {cliquer_case}] -bg $glob(bgcolor)
  grid $fb.consigne -row 0 -column 4
  #
  # placer 2 des en bas
  #
  set glob(case_actuelle) 0
  set amas [hasard]
  set glob(constellation1) [lindex $amas 0]
  set glob(constellation2) [lindex $amas 1]
  set glob(constellation) [expr $glob(constellation1)+$glob(constellation2)]
  label $fb.d�1 \
        -image [image create photo \
         -file [file join sysdata de_$glob(constellation1).png]] \
        -borderwidth 2 -bg $glob(bgcolor)
  grid $fb.d�1 -row 1 -column 6
  label $fb.d�2 \
        -image [image create photo \
         -file [file join sysdata de_$glob(constellation2).png]] \
        -borderwidth 2 -bg $glob(bgcolor)
  grid $fb.d�2 -row 1 -column 7
  #
  # on marque la case de depart
  #
  set yn [lindex $glob(trace) 0]
  set xn [lindex $glob(trace) 1]
  $fc create rect [expr $glob(org) + $xn*$glob(wlen) +1] [expr $glob(org) + $yn*$glob(hlen) +1] \
        [expr $glob(org) + ($xn+1)*$glob(wlen) +1] [expr $glob(org) + ($yn+1)*$glob(hlen) +1] \
        -fill $glob(doncolor) -width 1 ;# -tag rect($i,$j)
  $fc create image \
        [expr $glob(org) + $xn*$glob(wlen) +int($glob(wlen)/2) +1] \
        [expr $glob(org) + $yn*$glob(hlen) +int($glob(hlen)/2) +1] \
        -image [image create photo -file [file join sysdata [lindex $glob(portions) $glob(case_actuelle) ]]]
  set glob($yn,$xn) $glob(doncolor)
  #
  # on passe la main a l'eleve
  #
  set glob(nberreurs) 0
  # top depart
  set glob(heure_debut) [clock seconds]

} ;# open_and_deux_des

########################################################################
#                           programme principal                        #
########################################################################

  bind . <Control-q> {exit}

  # Relire le nom r�gl� de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    catch {set f [open [file join $glob(home_chemin) reglages trace_user] "r"]}
    gets $f glob(trace_user)
    close $f
  }

  init_dir_exos

  # creation du frame
  ###################
  . configure -background $glob(bgcolor)
  frame .frame -background $glob(bgcolor) -height $glob(height) -width $glob(width)
  pack .frame -side top -fill both -expand yes

  # creation du canvas
  ####################

  set fc .frame.c
  canvas $fc -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fc -expand true

  # ###########################################################
  # on cree une frame en bas en avant-derniere ligne avec
  #   le score affiche sous forme de tetes (bien passable mal)
  #   un bouton 'continuer'
  # ###########################################################

  frame .bframe -bg $glob(bgcolor)
  pack .bframe -side bottom -expand true

  image create photo pbien -file [file join sysdata pbien.gif] 
  image create photo ppass -file [file join sysdata ppass.gif]
  image create photo pmal -file [file join sysdata pmal.gif]
  image create photo pneutre -file [file join sysdata pneutre.gif]

  for {set i 1} {$i <= $glob(bouclemax)} {incr i 1} {
    label .bframe.tete$i -bg $glob(bgcolor) -width 4
    grid .bframe.tete$i -column [expr $i -1] -row 1 -sticky e
    .bframe.tete$i configure -image pneutre -width 80
  }

  button .bframe.but_gauche -image \
    [image create photo fgauche -file [file join sysdata fgauche.gif]] -command "recommencer; open_and_deux_des"
  grid .bframe.but_gauche -column [expr $glob(bouclemax)+2] -row 1
  .bframe.but_gauche configure -state disable

  button .bframe.but_quitter -image \
    [image create photo fquitter \
    -file [file join sysdata quitter_minus.gif]] \
    -command exit
  grid .bframe.but_quitter  -column [expr $glob(bouclemax)+3] -row 1

open_and_deux_des

