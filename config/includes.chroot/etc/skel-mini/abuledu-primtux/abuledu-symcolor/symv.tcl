#!/bin/sh
#symv.tcl
#Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : symv.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version $Id: symv.tcl,v 1.1.1.1 2005/12/27 13:08:40 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 12/12/2005
#
#
#########################################################################

global tid canvw canvh listdata nomf indexlibre CanvasGere Home basedir bgn bgl nbessai fautes categorie

set categorie $argv
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source path.tcl
source msg.tcl
source eval.tcl

initapp $plateforme
inithome
initlog $plateforme $ident



set nomf ""
set imgid 0
set id 0
set nbessai 0
set fautes 0
set c .frame.c
set cc .frame.cc

set bgn #ffff80
set bgl #ff80c0
set font2 ""
set tool 0
set indexlibre 0
set CanvasGere 0
set listdata {{} {set indexlibre $indexlibre}}
#wm overrideredirect . 1


#. configure -background #ffff80
source tool2.tcl
source enveloppe.tcl
source utils.tcl
source polygon.tcl
#source choixpoly.tcl
#wm protocol . WM_DELETE_WINDOW "quitte $c"

####################################################""
#proc�dure principale
#######################################################"

proc cplace {c cc} {
   global tid canvw canvh taillefont tabobj tabevent indexlibre
      $c delete all
	$cc delete all

$c configure -background white
$cc configure -background white
creetool typepolygon $c
set indexlibre 0
 }


###############################################################
#appel de la proc�dure principale
##############################################################"
cplace $c $cc
focus $cc
charge2 $c $cc temp.kds

#############################################################
#gestion des ev�nements
##########################################################
bind $cc <ButtonPress-1> "CanvasStart $cc %x %y"
bind $cc <B1-Motion> "CanvasDrag $cc %x %y"
bind $cc <ButtonRelease-1> "CanvasStopDrag $cc %x %y"

############################################################################
#gestion du jeu et des d�placements
#############################################################################
##############################################################################
proc CanvasStart {c x y} {

global lastX lastY CanvasGere tid
set CanvasGere 0
set lastX [$c canvasx $x]
set lastY [$c canvasy $y]
$c focus ""
#combien y a-t-il d'objets sous le pointeur au moment du click?
if {[llength [$c find overlapping [expr $x -1] [expr $y -1] [expr $x + 1] [expr $y + 1] ] ] ==0 } {
#if {([lsearch [$c gettags current] drag] ==-1) && ([lsearch [$c gettags current] enveloppe] ==-1) } 
# 0, CanvasGere passe � 1, on d�truit l'enveloppe rouge et on pr�pare l'enveloppe grise de s�lection multiple
catch {$c dtag all cible}
catch {$c delete enveloppe}
#mise � jour barre des boutons du bas
#catch {destroy .waction}
set CanvasGere 1
#ajout du tag CanvasGroup � l'enveloppe grise
$c create rectangle $x $y $x $y -outline gray50 -width 3 -stipple gray50 -tags "CanvasGroup"
} elseif { (([llength [$c find withtag cible]] ==0) || ([llength [$c find withtag cible]] ==1)) && ([lsearch [$c gettags current] enveloppe] ==-1) } {
#autrement, c'est qu'il y a des objets
# s'il n'y a pas d'objet d�j� s�lectionn� sous le pointeur (cible==0), ou un objet s�lectionn� (cible==1) sans enveloppe autour (enveloppe==-1)
if {[lsearch -regexp [$c gettags current] uident*] !=-1} {
#On le s�lectionne, avec une enveloppe rouge et on cr�e la barre d'outils du bas
catch {$c dtag all cible}
catch {$c delete enveloppe}
$c addtag cible withtag current
set typ [lindex [$c gettags current] [lsearch -regexp [$c gettags current] type*]]

#creetool $typ $c
$c focus current


createenveloppe $c current
}
}
}


proc CanvasDrag {c x y} {
global lastX lastY CanvasGere tid
return
#si on est en train de tracer le rectangle de s�lection
if {$CanvasGere == 1} {
# pour modifier le rectangle de s�lection gris
$c coords CanvasGroup $lastX $lastY $x $y
# si on n'est pas en train de tracer un rect de s�lection et qu'on est pas entrain de redimmensionner l'enveloppe (current enveloppe -1)
} elseif {[lsearch [$c gettags current] enveloppe] ==-1} {
$c focus ""
# on d�place tous les objets concernes (groupe ou seul)
foreach tg [$c find withtag cible] {
if {[lsearch -regexp [$c gettags $tg] uident*] !=-1} {
set typ [lindex [$c gettags $tg] [lsearch -regexp [$c gettags $tg] type*]]
switch $typ {


   typetext {
         textmove $c $tg $x $y $lastX $lastY
         }
   typebouton2 {
	bouton2move $c $tg $x $y $lastX $lastY
   	}
   default {
         $c move $tg [expr $x-$lastX] [expr $y-$lastY]}
         }
   }}
$c move enveloppe [expr $x-$lastX] [expr $y-$lastY]
set lastX $x
set lastY $y
}
}


proc CanvasStopDrag {c x y} {
global lastX lastY CanvasGere tid
if {$CanvasGere == 1} {
	# si on construisait le rectangle de s�lection gris
	set lcordr [$c coords CanvasGroup]
	#overlapping? or enclosed?
	# on tague tous les objets avec cible
	foreach it [$c find overlapping [lindex $lcordr 0] [lindex $lcordr 1] [lindex $lcordr 2] [lindex $lcordr 3] ] {
		if {[lsearch -regexp [$c gettags $it] uident*] !=-1} {
			$c addtag cible withtag $it
			}
	}
#overlapping?
#on r�initialise la barre d'outils si pas d'objet s�lectionn� (<2-> le canvas seul)
	if {[llength [$c find overlapping [lindex $lcordr 0] [lindex $lcordr 1] [lindex $lcordr 2] [lindex $lcordr 3] ]] < 2} {
		#creetool canvas $c
		}
#on efface le rectangle gris et on met une enveloppe rouge si besoin
	$c delete CanvasGroup
	if {[llength [$c find withtag cible]] !=0} {
		createenveloppe $c cible
		}
} else {

# si on n'�tait pas en train de tracer le rectangle de s�lection multiple
#On met le focus sur le texte si le widget en dispose
	set CanvasGere 0
	if {[llength [$c find withtag cible]] ==1} {
		set typ [lindex [$c gettags current] [lsearch -regexp [$c gettags current] type*]]

		switch $typ {
			typebouton2 {
			$c focus [lindex $tid([lindex [$c gettags current] [lsearch -regexp [$c gettags current] uident*]]) 4]
			focus $c
			}
			typetext {$c focus current}
		}
	}
}

}



