#!/bin/sh
#compagnon.tcl
#\
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2006 David Lucardi <davidlucardi@aol.com>
#   
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : compagnon.tcl
#  Author  : David Lucardi <davidlucardi@aol.com>
#  Modifier:
#  Date    : 30/12/2006
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: compagnon.tcl,v 1.2 2007/01/07 06:53:11 david Exp $
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi
# 
#***********************************************************************

proc tux_presente {} {
.wtux.speech configure -wraplength 300
wm protocol .wtux WM_DELETE_WINDOW ""
.wtux.tux configure -image tux
}

proc tux_commence {typesuite pas reverse} {
global user
if {$reverse == 0} {set what plus} else { set what moins}

switch $typesuite {
0 {set mess "Tu dois compter de $pas en $pas."}
1 {set mess "A chaque d�placement, le nombre est multipli� par $pas."}
2 {set mess "Tu dois trouver un nombre $what grand pour te d�placer."}
3 {set mess "Tu dois compter de $pas en $pas."}
4 {set mess "Tu dois trouver les expressions num�riques qui font $pas."}
}
.wtux.speech configure -text "[format [mc {Bonjour %1$s, Tu te trouves sur la case jaune. D�place-toi avec les touches fl�ch�es pour aller sur la case verte.}] [string map {.log \040} [file tail $user]]] $mess"
.wtux.tux configure -image tux_bonjour
}

proc tux_reussi {} {
.wtux.speech configure -text [mc {Bien joue!}]
.wtux.tux configure -image tux_bien
}


proc tux_continue_bien {erreur total} {
.wtux.speech configure -text "C'est juste! Pour le moment, tu as $erreur erreur(s) pour $total r�ponse(s) juste(s)."
.wtux.tux configure -image tux_bien
}

proc tux_devinette {mess mess1 mess2} {
.wtux.speech configure -text "$mess\n$mess1\n$mess2"
.wtux.tux configure -image tux
}

proc tux_consigne_question {} {
.wtux.speech configure -text "Tu dois taper un nombre convenable et appuyer ensuite sur la touche entr�e."
.wtux.tux configure -image tux_pasmal
}

proc tux_sorti {} {
.wtux.speech configure -text [mc {Attention! Tu sors du labyrinthe!}]
.wtux.tux configure -image tux
}

proc tux_mauvaisedir {} {
.wtux.speech configure -text [mc {Ce n'est pas la bonne direction.}]
.wtux.tux configure -image tux
}

proc tux_continue {} {
.wtux.speech configure -text ""
.wtux.tux configure -image tux
}

proc tux_echoue1 {} {
.wtux.speech configure -text [mc {C'est faux, recommence.}]
.wtux.tux configure -image tux_echoue
}

proc tux_echoue2 {} {
.wtux.speech configure -text [mc {C'est toujours faux!}]
.wtux.tux configure -image tux_rechoue
}

proc tux_triste {score} {
.wtux.speech configure -text [format [mc {C'est pas terrible! Ton score est de %1$s sur 100. Clique sur un bouton pour continuer, recommencer ou quitter.}] $score]
.wtux.tux configure -image tux_rechoue
}

proc tux_moyen {score} {
.wtux.speech configure -text [format [mc {C'est pas mal! Ton score est de %1$s sur 100. Clique sur un bouton pour continuer, recommencer ou quitter.}] $score]
.wtux.tux configure -image tux_pasmal
}

proc tux_content {score} {
.wtux.speech configure -text [format [mc {C'est bien! Ton score est de %1$s sur 100.Clique sur un bouton pour continuer, recommencer ou quitter.}] $score]
.wtux.tux configure -image tux_bien
}


proc destroy_tux {} {
catch {destroy .wtux}
}

proc init_tux {} {
catch {destroy .wtux}
toplevel .wtux
wm geometry .wtux +0+340

set compagnon [expr int(rand()*2) +1]
switch $compagnon {
1 {
image create photo tux -file [file join sysdata tux.gif] 
image create photo tux_bonjour -file [file join sysdata tux.gif] 
image create photo tux_bien -file [file join sysdata tux_bien.gif] 
image create photo tux_echoue -file [file join sysdata tux_echoue1.gif] 
image create photo tux_rechoue -file [file join sysdata tux_echoue2.gif] 
image create photo tux_pasmal -file [file join sysdata tux_pasmal.gif] 
wm title .wtux Tux
}

2 {
image create photo tux -file [file join sysdata bob.gif] 
image create photo tux_bonjour -file [file join sysdata bob_bonjour.gif] 
image create photo tux_bien -file [file join sysdata bob_bien.gif] 
image create photo tux_echoue -file [file join sysdata bob_echoue1.gif] 
image create photo tux_rechoue -file [file join sysdata bob_echoue2.gif] 
image create photo tux_pasmal -file [file join sysdata bob_pasmal.gif] 
wm title .wtux Bob
}
}
label .wtux.speech -wraplength 120
pack .wtux.speech -side top -fill both -expand 1
label .wtux.tux -image tux
pack .wtux.tux -side top -fill both -expand 1
wm protocol .wtux WM_DELETE_WINDOW "catch {return}"
update
wm transient .wtux .
bind .wtux <FocusIn> "focus .frame.c"
}