#!/bin/sh
#pluie.tcl   ESSAI DE RECONSTRUCTION D'UN Logiciel IPT (auteur ?)
#\
exec wish "$0" ${1+"$@"}
###########################################################################
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier:
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
set basedir [file dir $argv0]
cd $basedir
source calculs.conf
source i18n
source path.tcl
set basedir [pwd]

##bind . <F1> "showaide {}"
global sysFont , env  , DOSSIER_EXOS1
source msg.tcl
source fonts.tcl
set bgcolor #808080
set bgl #ff80c0
wm geometry . +0+0
. configure -background $bgcolor
##wm resizable . 0 0
wm  title . "[mc {cal_pluie}] - sous GPL"
set date [clock format [clock seconds] -format "%H%M%D"]

### outre frame, 3 autres cadres :
# .bframe,pour les calculs
# .mframe, pour le nom, score, test de debuggage
# .hframe pour la tomb�e des expressions par grid
catch {destroy .frame}
catch {destroy .bframe}
catch {destroy .mframe}
catch {destroy .hframe}
set bgcolor orange
frame .frame -width 1024 -height 800 -background  $bgcolor
#bframe pour les calculs (bframe.res)
frame .bframe -background $bgcolor -relief raised -bd 3 -height 6
pack .bframe -side bottom -fill both
label .bframe.lab_consigne -background $bgcolor -text [mc {calc_somm_diff}]
grid .bframe.lab_consigne -padx 20 -column 1 -row 1
button .bframe.but_calcul -text [mc {lancer_calc}] \
-command ".bframe.but_calcul configure -state disable ; calcul"
grid .bframe.but_calcul -column 3 -row 1 -padx 100
entry .bframe.res -width 8 -justify center  -textvariable rep  -font {arial 22}
grid .bframe.res -column 2 -row 1 ; 
#essai de bind control� par return, mais ne sert � rien pour l'instant"
#bind .bframe.res <Keypress-Return>  [set rep1 [.bframe.res get]] ;#[set rep1 $rep]
#mframe pour le nom et le score et les tests de deboggage !!
frame .mframe -background $bgcolor -relief raised -bd 3
pack .mframe -side bottom -fill both 
set nom_elev  [lindex $argv 1 ] ;#init nom �l�ve et nom clase
set nom_classe [lindex $argv 2 ]
initlog   $tcl_platform(platform)  $nom_elev
label .mframe.lab_nom  -text "[mc {bonjour}] $nom_elev"       -background  $bgcolor -anchor e
  #AC# column 1 par 5
  grid .mframe.lab_nom -column 1 -row 1

#Initialisations fichier de calcul, nom de l'�l�ve, nbre colonnes, de lignes
#set n_lig 10 ; set n_col 4
proc vieux_fichier {expr} {
set e1 [regsub -all  {\/}  $expr ":" var]
set e2 [regsub -all  {\*} $var "x" var1 ]
set e3 [regsub -all  {\.} $var1 "," var2 ]
return $var2
}
##set oper [ vieux_fichier [lindex  $listdata 0] ]
set fich [lindex $argv 0 ]
set DOSSIER_EXOS1  [lindex $argv 4 ]
cd  $DOSSIER_EXOS1
set f [open [file join $fich] "r"  ]
set listdata [gets $f] ;#init des param�tres du scenario : la liste des couples,..
set listcouples [ vieux_fichier [lindex $listdata 1] ]
if { $listcouples  == {} } {
 tk_messageBox -message [mc {mess_list3}] -title "[mc {Message mauvaise liste}]" -icon error -type ok
exit  }
foreach el $listcouples {
 if { [ regexp {^[0-9]+[\+|\-|\:|x][0-9]*[\+|\-|\:|x].+$} $el ] } {
 tk_messageBox -message [mc {mess_list1}]  -title "[ mc {Message mauvaise liste}]" -icon error -type ok
 exit
 }
 }
 foreach el $listcouples {
 if { [ regexp {^.+\,.+[\+|\-|\:|x].+$} $el ] } {
 tk_messageBox -message [mc {mess_list2}] -title "[mc {Message mauvaise liste}]" -icon error -type ok
 exit
 }
 }
 foreach el $listcouples {
 if { [ regexp {^.+[\+|\-|\:|x].+\,.+$} $el ]  } {
 tk_messageBox -message [mc {mess_list2}] -title "[mc {Message mauvaise liste}]" -icon error -type ok
 exit
 }
 }
#set fich $f
#"set fich [file tail $f]
close $f
cd $basedir
set oper [lindex  $listdata 0]
set n_lig [lindex  $listdata 2]
set n_col [lindex  $listdata 3]
if { $n_col > 9 } {
set n_col 8
}
if { $n_lig > 17 } {
set n_lig 17
}
proc traite_expr { expr} {
if { [string last + $expr] != -1  || [string last - $expr] != -1  \
 || [string last x $expr] != -1  || [string last : $expr] != -1 } {
return "\(${expr}\)"
} else {
return $expr
}
}
proc traite_expr1 {expr} {
 if { [string last "\(" $expr ] == -1  } {
 return $expr
    } else {
	return [expr $expr]
	}
}
proc change_oper {expr} {
set e1 [regsub -all  {:}  $expr "\/" var]
set e2 [regsub -all  {x} $var "\*" var1 ]
set e3 [regsub -all  {,} $var1 "\." var2 ]
return $var2
}
#hframe pour la tomb�e des expressions.  par grid
frame .hframe -background $bgcolor
pack .hframe -side bottom -fill both
for {set i 1} {$i <= $n_lig} {incr i 1} {
    for {set j 1} {$j <= $n_col} {incr j 1} {
        label .hframe.lab_bilan$i$j -text "_________"  -background $bgcolor -font {helvetica 20}
        grid .hframe.lab_bilan$i$j -column $j -row $i
    }}
##set fl_b [ image create photo fleche_b	-file [file join  images "fl_b.gif"]]	
##button .ordres.fl_ab   -image $fl_b  -command "coder $code_b "	
	
set fin [ image create photo fin	-file [file join  images "fin.gif"]]	
button .hframe.exit   -image $fin -command "exit"	
grid .hframe.exit -column [expr $n_col + 1 ] -row $n_lig
##button .bframe.exit -text [mc {exit}]  -command "exit"
##grid .bframe.exit -column 4 -row 1 
label .hframe.rep -text "_________" -font {helvetica 20}  -background white  ; grid .hframe.rep -column [expr $n_col + 4] -row 4 ;# essai pour les bad calculs
#"set listdata {+ {1 2 5 3 14 5 7 8 4 5 1 5 14 5 23 5 4 15 8 9 0 3 4 5  78 1}}
#init des couples de nombre en jeu, de l'op�ration, des couples au d�part
#des bons calculs et des mauvais
set oper [ vieux_fichier [lindex $listdata 0]]
if { $oper == "x" } { set oper_c "*" } 	elseif { $oper == ":" } {set oper_c "/" } else {set oper_c $oper }
set ndepart [expr int($n_col / 2) ]
#set ndepart 1; #< nbre colonnes -1  nbe de colonnes remplies au d�part
##set listcouples [lindex $listdata 1]
set uu [llength $listcouples]
set nbrecouples [expr $uu /2]
for  {set i 0} {$i <= [expr 2*($ndepart - 1)]} {incr i 2 } { ;# ndepart couples  0  2 4 ie 1 3 5" � param�trer
    set nx [traite_expr [lindex [lrange $listcouples  $i [expr  1 + $i]] 0] ]
    set ny [traite_expr [lindex [lrange $listcouples $i [expr 1 + $i ]] 1] ]
    set ii [expr ($i /2) + 1]
.hframe.lab_bilan1${ii}  configure -text "${nx}${oper}${ny}"  -background $bgcolor -font {helvetica 20}

 }
set listcouples [lrange $listcouples [expr 2*$ndepart] end]
set nbrecouplesrestant [expr $nbrecouples - $ndepart ]
set indexcol [expr $ndepart + 1]; #attention � ne pas d�passer $depart"
set score 0 ; set echec 0 ; set reussite 0 ; set mrep 0 ; set bonneliste {} ; set echecliste {} ;   set troptardliste {}
set tempo [lindex $argv 3] 
if { ![regexp {^[0-9]+$} $tempo ] } {
set tempo 1000  } 
 set drapeau_bon 0
#"destroy .hframe  #" de foreach
proc memorise {} {
global input
set input [.bframe.res get]
.bframe.res delete 0 end
}
proc solution { op b_lis t_lis e_lis  nom   fich  date } {
global env tcl_platform w2
catch {destroy .top2 }
set w2 [toplevel .top2]
wm geometry $w2 +250+0
wm title $w2 [mc {bil_bon_calc}]
###grab ${w2}
text ${w2}.text -yscrollcommand "${w2}.scroll set" -setgrid true -width 60 -height 15 \
 -wrap word -background black -font   {Helvetica  12}
scrollbar ${w2}.scroll -command "${w2}.text yview"
pack ${w2}.scroll -side right -fill y
pack ${w2}.text -expand yes -fill both
##lower .frame  ${w2} 
${w2}.text tag configure green -foreground green
${w2}.text tag configure red -foreground red
${w2}.text tag configure yellow -foreground yellow
${w2}.text tag configure normal -foreground black
${w2}.text tag configure white -foreground white -underline yes
 ${w2}.text tag configure white1 -foreground white     -font  {helvetica 16}
proc marge {} {
global w2
${w2}.text insert end  "     "
}

${w2}.text insert end  "  Le $date, exercice : $fich\n"     red
${w2}.text insert end   "[mc {resum}]  $nom :\n\n"   white1
marge ; ${w2}.text insert end  "[mc {bon_calc}]\n"   white
foreach cal $b_lis {
marge ; ${w2}.text insert end "[mc {ton_calc}]  $cal =[expr 1.0 * [change_oper $cal] ]\n" green
}
${w2}.text insert end  "\n"
marge ;  ${w2}.text insert end  "[mc {mauv_calc}]\n"   white
##foreach traite $elis
marge ; marge ; ${w2}.text insert end "[mc {result_calc}]\n" red
marge
foreach cal [non_doublons $e_lis ]  {
 ${w2}.text insert end "  [lindex $cal 0]\-" red
}
##traitet_lis
${w2}.text insert end  "\n"
marge ; ${w2}.text insert end  "[mc {trop_tard}]\n"   white
    foreach cal $t_lis {
${w2}.text insert end "   $cal =[expr 1.0 * [ change_oper $cal]]\n" yellow
}
  if  {  $tcl_platform(platform)  == "unix"  }  {
###after 1000
set rep [tk_messageBox  -message "[mc {imprim}]" -type yesno -icon question  -default no -title Imprimer? ]
 if {  $rep  == "yes"  }  {
### set  t    [ ${w2}.text  get  0.0  end  ]
 exec  lpr   <<  [ ${w2}.text  get  0.1  end  ]
 ###  $b_lis  $e_lis
 ### ${w2}.text  get  0 end  ]
 }
##raise ${w2} .
 }
 raise ${w2} .
}
proc non_doublons { liste} {
if { $liste == {} } {
return $liste
} else {
if { [lsearch [lrange $liste 1 end] [lindex $liste 0]] == -1 } {
    return [ list [lindex $liste 0] [ non_doublons [lrange $liste 1 end]] ]
   } else {
     return [non_doublons [lrange $liste 1 end]]
    }   
}
}
bind .bframe.res <Return> "memorise"
bind .bframe.res <KP_Enter> "memorise"
set input {}
##############################################################################################
# programme principal avec usage pseudo recursif. R�le de after pas tr�s clair encore
# A l'arr�t : travail sauv� ds un fichier qui doit exister
##############################################################################################
proc calcul {} {
global fich tempo nom_elev n_col n_lig listcouples nbrecouples score echec avreu reussite  bgcolor \
mrep nbrecouplesrestant indexcol input oper  bonneliste echecliste troptardliste date bon_lig bon_col drapeau_bon
focus -force .bframe 
#"update
        if  { ( $echec == $nbrecouples ) || \
( $reussite >= $nbrecouples ) || \
( [expr $echec + $reussite] >= $nbrecouples )} {
#focus -force .mframe.ent_bilan
###.mframe.ent_bilan insert 0 $reussite 
label .mframe.lab_bilan  -text "$nom_elev, [mc {reussite}] $reussite calcul(s)." \
        -background  $bgcolor -anchor e
    grid .mframe.lab_bilan -column 5 -row 1 -padx 10
    sauver $date $nom_elev $fich $oper $bonneliste $troptardliste  $mrep
 button .bframe.sol -text [mc {R�sum� du travail ? }] -foreground yellow\
-command "solution $oper {$bonneliste} {$troptardliste}  {$echecliste} $nom_elev [file tail $fich]  $date"
 grid .bframe.sol -column 4 -row 1
button .bframe.bis -text [mc {Recommencer ? }] -foreground blue  -command "recommencer "
grid .bframe.bis -column 5 -row 1
  .bframe.but_calcul configure -text [mc {quitter}] -state active -command { exit }
return 1
        }
#            for  {set t 1} {$t <= $n_col} {incr t} {
#                .hframe.lab_bilan${n_lig}${t} configure -text "_______" -background  $bgcolor -font {helvetica 20}
#            }
        if  { ($nbrecouplesrestant != 0 ) && ($indexcol <= $n_col )  } {
            set acal [ lrange $listcouples 0 1 ]
            set listcouples [lrange $listcouples 2 end ]
            set nx [traite_expr [lindex $acal 0]] ; set ny [traite_expr [lindex $acal 1]]
            .hframe.lab_bilan1${indexcol}  configure -text "$nx$oper$ny"  -background $bgcolor -font {helvetica 20}
            incr indexcol ; if {$indexcol > $n_col } { set indexcol 1  } ;
            set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
        } ;
#"        if {$indexcol > $n_col } { set indexcol 1  } ;
if { $drapeau_bon == 1} {
.hframe.lab_bilan${bon_lig}${bon_col}  configure -text "_________" -background $bgcolor -font {helvetica 20}
set drapeau_bon 0
}
set avreu $reussite ; avancer  ;
after $tempo  calcul     
 ; #"return 1
};

### gestion des calculs de l'avan��e...
proc avancer {} {
global rep1 n_lig n_col bon_col bon_lig drapeau_bon listcouples nbrecouplesrestant oper \
 avreu reussite echec mrep score bonneliste echecliste indexcol input   troptardliste bgcolor
focus  .bframe.res ;# redondant ?
set rep1 $input
###[.bframe.res get] 
if { ![ regexp {^[0-9]+\,?[0-9]*$} $rep1]  } {
            set rep1 {}  ; set input {}
}
# receuil de la r�ponse de l'�l�ve. La dur�e de la frappe d�pend de after 1500
# pour chaque case jusqu'� la ligne n_lig-1 faire :
for  {set col 1 } {$col <= $n_col } {incr col } {
        set drap_bon 0
    for  {set lig [expr $n_lig - 1]} {$lig > 0 } {incr lig -1} {
        set val [.hframe.lab_bilan${lig}${col} cget -text] ; set lig_entree 0  ;# ce qu'il y a dans la case!
 ###traitement de val 45+56 ou(4+5)+56 ou...        
   if { [regexp {(\(?\d*[+|\-|x|\:]{0,1}\d*\)?)[+|\-|x|\:](\(?\d*[+|\-|x|\:]{0,1}\d*\)?)} $val tout prem deux]  } {      
            set nx2 [traite_expr $prem ] ; set ny2 [ traite_expr $deux ]
         set nx $prem  ; set ny $deux 
#".mframe.ent_nom insert 0 $oper ;.mframe.ent_nom insert 0 $nx ;.mframe.ent_nom insert 0 $ny ; # pour les tests
#  set som1 [expr $nx +$ny]
              if  { $rep1 != "" &&  [expr 1.0 * [ change_oper $val ]] == [expr [ change_oper $rep1 ] ]}  { ; #bonne r�ponse. 
           #"clignote BRAVO $lig $col 10 ;
	    .bframe.res delete 0 end  ;# on efface la bonne r�ponse de l'input.
            incr score ; incr reussite ; #" .mframe.bil insert end $reussite
	    clignote [mc {bon_calcul}] $lig $col  2
	    .hframe.lab_bilan${lig}${col}  configure -text "___________" -background $bgcolor -font {Arial 20} -foreground black
             set lig_entree $lig      ;  set drap_bon 1
set bonneliste [ linsert $bonneliste end  $val]
# [list $prem $deux ]]
set bon_lig $lig ; set bon_col $col ; set drapeau_bon 1
.hframe.lab_bilan${lig}${col}  configure -text [mc {bon_calcul}]  -background #ffffff -font {helvetica 18}
if  { ($nbrecouplesrestant != 0)  && ([string compare .hframe.lab_bilan1${col} "_________" ] == 0 ) } {
    set acal [ lrange $listcouples 0 1 ]
    set listcouples [lrange $listcouples 2 end ]
    set nx1 [traite_expr [lindex $acal 0] ] ; set ny1 [traite_expr [lindex $acal 1]]
    set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
    .hframe.lab_bilan1${col} configure -text "${nx1}${oper}${ny1}" -background #000000 -font {helvetica 20}
## ${nx1}${oper}${ny1}   
}
#".hframe.lab_bilan${lig}${col}  configure -text "___ " -background $bgcolor -font {helvetica 14}
##.bframe.res delete 0 end
set input {}
set col [expr $n_col + 1] ; break
	} else {
 #mauvaise r�ponse, on fait descendre! l'expression. echecliste ne sert pas
   if {  $rep1 != {} &&  $drap_bon  == 0 } {
    set echecliste [ linsert $echecliste end  $rep1 ]
    }
    set k [expr $lig + 1]
.hframe.lab_bilan${k}${col} configure -text "${val}" -background $bgcolor -font {helvetica 20}
.hframe.lab_bilan${lig}${col}  configure -text "_________" -background $bgcolor -font {helvetica 20}
#.hframe.rep configure -text [mc {Mauvais calcul}] -font {helvetica 20}  -background $bgcolor b
set input {}
##.bframe.res delete 0 end ;# on efface la mauvaise r�ponse de l'input !.
		}

}
}
}
#il reste la derni�re ligne : les trop tard !
for  {set t 1} {$t <= $n_col} {incr t} {
    set val [.hframe.lab_bilan${n_lig}${t} cget -text]
    if { [regexp {(\(?\d*[+|\-|x|\:]{0,1}\d*\)?)[+|\-|x|\:](\(?\d*[+|\-|x|\:]{0,1}\d*\)?)} $val tout prem deux] } {
               .hframe.lab_bilan${n_lig}${t} configure -text "[mc {trop_tard1}] " -background #ffffff -font {helvetica 20}
             incr echec
#    set nx [traite_expr [ lindex [split $val $oper] 0] ]
#    set ny  [traite_expr [ lindex [split $val $oper] 1 ]]
    set troptardliste [ linsert $troptardliste end  $val]
    ##[list $prem $deux ] ]
      if  { ($nbrecouplesrestant != 0 ) && ($t != $indexcol) } {
        set acal [ lrange $listcouples 0 1 ]
        set listcouples [lrange $listcouples 2 end ]
        set nx [traite_expr [lindex $acal 0]] ; set ny [traite_expr [lindex $acal 1]]
        .hframe.lab_bilan1${t}  configure -text "$nx${oper}$ny"  -background $bgcolor -font {helvetica 20}
        set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
     }
}
}
# si le nbe de reussites est inchang� c'est une mauvaise r�ponse sinon bonne r�ponse.
#essai de mentionner un mauvais calcul : non concluant !
if  { $avreu == $reussite && $rep1 != "" } {
	incr mrep 
.hframe.rep configure -text "${mrep} [mc {echecs}]   " -font {helvetica 20}  -background white
###clignote "${mrep} [mc {echecs}]"  5
} else  {
   .hframe.rep configure -text "_________" -font {helvetica 20}   -background white

}
return 1
}
proc clignote {texte lig col  nclic} { ; #after supprim� car semble pos� des pbs de // avec l'autre after
global bgcolor
for  {set cl 1} {$cl <= $nclic} {incr cl} {
after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background $bgcolor -font {helvetica 20} -foreground green
update
after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background $bgcolor -font {helvetica 20} -foreground red
update
}
}
# sauver les r�sultats
proc sauver {dat nom fic oper listereussite troptard mrep} { ; # � s�curiser
global savingFile nom_classe TRACEDIR user env  choix_lieu LogHome DOSSIER_EXOS1 basedir
set TRACEDIR $LogHome
cd $TRACEDIR
#set types {
#    {"Cat�gories"		{.txt}	}
#}
#catch {set file [tk_getSaveFile -filetypes $types]}
#set f [open [file join $file] "a"]
#set travail  "date : $dat nom : $nom fichier : $fic $oper bon : $listereussite echec : $listechec  mauvais cal
set basefile [file tail $fic]
#set savingFile ${nom_classe}.bil
set ou [lrange [ split $DOSSIER_EXOS1 "/" ]  end-1 end]
if { [lsearch -exact $ou "C:" ]  >= 0 } {
set ou [ lreplace $ou  [lsearch -exact $ou "C:" ] [lsearch -exact $ou "C:"  ] "C" ]
}
##"$dat&$nom&{[file tail ${fic}] de $ou}&$oper&{$liste_rep}&$listetard&$listechec&un"
set savingFile [file join $basedir data  ${nom_classe}.bil ]
  catch {set f [open [file join $savingFile] "a"]}
#  set travail  ":$dat:$nom:$basefile de  ${choix_lieu}:$oper:$listereussite:$troptard:$mrep:$LogHome:pl"
set travail  "$dat&$nom&{[file tail ${fic}] de $ou}&$oper&{$listereussite}&$troptard&$mrep&pl"
puts $f $travail
close $f
catch {set flog  [open [file join $user] "a+"]}
set ou [lindex [ split $DOSSIER_EXOS1 "/" ]  end]
set log "$dat&$nom&{[file tail ${fic}] de $ou}&$oper &$listereussite&$troptard&$mrep&pl"
puts  $flog $log
close $flog
##exec   /usr/bin/leterrier_log  --message=$log   --logfile=$user
}
 
 proc recommencer {} {
 global basedir DOSSIER_EXOS1 argv  iwish nom_elev nom_classe tempo
 set exo [lindex $argv 0 ]  ; destroy .
 exec  $iwish  [file join $basedir pluie.tcl ] [file join $DOSSIER_EXOS1 $exo ] $nom_elev $nom_classe $tempo  $DOSSIER_EXOS1 &
}
 ##exec  $iwish  [file join $basedir pluie.tcl ] [file join $DOSSIER_EXOS1 $exo ] [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree  get]   $DOSSIER_EXOS1







