#!/bin/sh
#piquet.tcl
#\
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Date    : 01/02/2005
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version 0.1
#  @author     Jean-Louis Sendral. 
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  ##############################################################
source bande.conf ; ##calculs.conf ???
source msg.tcl
source path.tcl
set basedir  [pwd]
##############################################################
##Situation d�velopp�e par Brousseau (didactique des maths)
## Objectif : reste de la division euclidienne et raisonnement math�matique
## Il s'agit d'atteindre une cible en ajoutant ou 1 2 ..pas-1
###2 �leves : chacun a  son tour ajoute : symbolis� sur une bande num�rique
proc fais_grille { } {
global init tab pas
 for {set y 0} {$y <=  $init(nbrow) } {incr y} {
		 for {set x 0} {$x < $init(nbcol) } {incr x} {
		 set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ]
		set tab(rect${y}_${x})	[ .frame_middle.can create rectangle  $ne_x $ne_y   [expr $ne_x + $init(wlen)]  [expr $ne_y  +$init(hlen) ]  -tags rect${y}_${x} -outline red]
						 }
 }
}





proc main { } {
  global init tcl_platform  cible  pas  tab_n nom_autre nom_elev  tab n_essais n_session
global   dernier_n  max_n  font_mess font_case_jeu font_case progaide

  
  if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
set font_mess1 {helvetica 14 bold} ; set font_mess2 {helvetica 10 bold} ; set font_case_jeu {helvetica 15 bold}
set font_case {helvetica 14 bold}
} else {
set font_mess1 {helvetica 10 bold} ; set font_mess2 {helvetica 10 bold} ; set font_case_jeu {helvetica 18  bold}
set font_case {helvetica 18 bold}
 set nom_elev eleve
 set nom_classe classe
}
set nom_autre ""
initlog $tcl_platform(platform) $nom_elev
##set font_mess {helvetica 12 bold}
set largeur $init(width) ; set hauteur  $init(height) 
 . configure -width $largeur -height $hauteur
 ###-width 600   -height 460
wm title . "[mc  {Calculs sur  bande. Course � n pas de p (cf Brousseau)}]"
wm geometry  . +0+0  
wm resizable . 1 1
catch {destroy .frame_up   .frame_middle .frame_down}
frame .frame_up  -borderwidth 1 -height 90 -width $largeur -background blue 
set haut_middle [expr $hauteur - 90 - 90]
set init(wlen)   [ expr [expr $largeur - 150 - 90]  /  $init(nbcol) ] 
set init(hlen)  [ expr [expr $hauteur - 180]  /  [expr $init(nbrow)  + 1]] 
#if { $init(wlen)   <=  $init(hlen) }  { set  init(hlen)  $init(wlen) } else {  set init(wlen) $init(hlen) }
#set init(wlen) [expr $init(wlen)  ] ; ###pour adapter taille quadr
set n_session 0 ; set n_essais 0
if { [expr $init(nbrow)  % 2] !=0 } {set mult [expr [expr $init(nbrow) / 2] +1] ; set reste [expr $init(nbrow) / 2] }  else  {
							set mult [expr $init(nbrow) / 2]  ;  set reste [expr [expr $init(nbrow) / 2]  - 1] }
set max_n [  expr [ expr $mult * $init(nbcol) ] +  $reste  ]
 frame .frame_middle -width  $largeur -height $haut_middle -bg bisque2
 frame .frame_down  -height 90 -width $largeur -background bisque1 
frame .frame_middle.right -width [expr  210+ 30] -height $haut_middle -bg bisque2 ; ###210=150+65
canvas .frame_middle.can  -height $haut_middle -width [expr $largeur - 150 -65 -25] -background orange ; ###65 ajustement
label .frame_up.t_nom_elev1 -text "[mc {El�ve1 :}]" -font {helvetica 11 bold} -bg bisque1
label .frame_up.t_nom_elev2 -text "[mc {El�ve2 :}]" -font {helvetica 11  bold} -bg bisque1
label .frame_up.t_cible -text "[mc {Cible :}]" -font {helvetica 11 bold} -bg bisque1
label .frame_up.t_pas -text "[mc {Pas   :}]" -font {helvetica 11 bold} -bg bisque1
entry .frame_up.nom_elev1 -width 20 -font {helvetica 10 bold} -justify center
 entry .frame_up.nom_elev2 -width 20 -font {helvetica 10 bold} -justify center
entry .frame_up.cible  -width 3 -font {helvetica 12 bold} -justify center
entry .frame_up.pas  -width 2 -font {helvetica 12 bold} -justify center
 .frame_up.cible  configure -state disabled ; .frame_up.pas  configure -state disabled 
message .frame_up.message1 -text "[mc {C'est ?? qui d�bute puis ??}]\n[mc {Il faut atteindre ??  pas  � pas.}]"   -font $font_mess1  -aspect 700
message .frame_up.message2 -text "[mc {Comment �tre s�r d'atteindre en premier?}]" -font $font_mess2   -aspect 1200 -bg red
button .frame_up.quit -text "Quitter" -command exit
button .frame_up.rec -text "[ mc {Autre Session?}]" -command n_session
.frame_up.rec configure -state disabled
label  .frame_middle.right.calculs -text "[ mc {CALCULS des �l�ves : }]" -bg red -font {helvetica 14 bold}
label  .frame_middle.right.elev1 -text "[mc {Choix de }]\n" -bg pink -font {helvetica 12 bold}
entry .frame_middle.right.jeu1 -width 4  -font {helvetica 22 bold} -justify center -bg pink -bd 3 -highlightcolor red -highlightthickness 3
label  .frame_middle.right.elev2 -text "[mc {Choix de }]\n" -bg green -font {helvetica 12 bold}
entry .frame_middle.right.jeu2 -width 4 -font {helvetica 22 bold} -justify center -bg green -bd 3 -highlightcolor red -highlightthickness 3
label  .frame_middle.right.t_deroul -text "[ mc {Nombres atteints :}]"  -bg bisque -font {helvetica 12 bold}
 .frame_middle.right.jeu1 configure  -state disabled ; .frame_middle.right.jeu2 configure  -state disabled 
entry .frame_middle.right.deroul  -width 40 -font {helvetica 10 bold} -xscrollcommand  ".frame_middle.right.scroll1 set"
###debug
##.frame_middle.right.deroul insert end $max_n
scrollbar .frame_middle.right.scroll1  -command ".frame_middle.right.deroul  xview" -orient horizontal
button  .frame_middle.right.lancer  -text  "[ mc {Allons y!}]" -font {helvetica 12 bold} -command "lancer"
button .frame_middle.right.n_essai -text "[mc {Autre Essai ?}]" -command "n_essai"
button .frame_down.aide -text [mc {Aide}]
message .frame_down.bilan -text "[mc {Bilan : }]" -aspect 400 -font 10x20 -bg bisque -justify center
text .frame_down.avis -relief sunken -height 6 -width 50 -yscrollcommand  ".frame_down.scroll set" -font {helvetica 10}
scrollbar .frame_down.scroll -command ".frame_down.avis yview" -orient vertical

.frame_middle.right.lancer configure -state disabled 
.frame_middle.right.n_essai  configure -state disabled
place .frame_up  -x 0 -y 0
place  .frame_middle -x 0 -y 90
place .frame_down  -x 0 -y [expr $haut_middle +90]
place  .frame_up.t_nom_elev1 -x 0 -y 0 ; place  .frame_up.nom_elev1 -x 70 -y 0
place .frame_up.t_nom_elev2 -x 0 -y 25 ; place .frame_up.nom_elev2 -x 70 -y 25 
place .frame_up.t_cible -x 0 -y 55 ;  place .frame_up.cible -x 60 -y 55
place .frame_up.t_pas -x 100 -y  55 ; place .frame_up.pas -x 150 -y  55
place .frame_up.message1 -x 220  -y 0 ; place .frame_up.message2 -x 220 -y 50
 place .frame_middle.can -x 0 -y 0 
 place .frame_middle.right -x  [expr $largeur - 220 -20 ]  -y 0 ; ##220=150+70
place  .frame_up.quit  -x [expr $largeur -100] -y 5 ; place .frame_up.rec -x [expr $largeur -110] -y 50
place .frame_middle.right.calculs -x 5 -y 5
 
 if { $tcl_platform(platform) != "unix" } {
 place  .frame_middle.right.elev1 -x 10 -y 100; place  .frame_middle.right.jeu1 -x 10  -y 140
 place  .frame_middle.right.elev2  -x 140 -y 100  ; place .frame_middle.right.jeu2 -x 135 -y 140
 place  .frame_middle.right.t_deroul -x 30 -y 200
 place .frame_middle.right.deroul -x 10 -y 225
 place .frame_middle.right.scroll1 -x 10 -y 245 -width 235
 place  .frame_middle.right.lancer -x 5 -y 265
 place .frame_middle.right.n_essai -x 120 -y 265
 } else {
place  .frame_middle.right.elev1 -x 10 -y 90; place  .frame_middle.right.jeu1 -x 10  -y 130
 place  .frame_middle.right.elev2  -x 140 -y 90  ; place .frame_middle.right.jeu2 -x 135 -y 130
 place  .frame_middle.right.t_deroul -x 30 -y 180
 place .frame_middle.right.deroul -x 10 -y 205
 place .frame_middle.right.scroll1 -x 10 -y 225 -width 235
 place  .frame_middle.right.lancer -x 5 -y 255
 place .frame_middle.right.n_essai -x 120 -y 255


}
 place .frame_down.aide -x [expr $largeur -60]  -y 5
	switch $tcl_platform(platform) {
		"windows" {
bind .frame_down.aide <1>  "exec $progaide ${basedir}/aide/calculs_sur_bandes.html & "
			}
		"unix"  {
	  bind .frame_down.aide <1>  "exec $progaide [pwd]/aide/calculs_sur_bandes.html & "
			}
				    }

place  .frame_down.bilan  -x 0 -y 0
 place .frame_down.avis -x 250 -y 0
# place  .frame_down.scroll -x 550  -y 0 -height 85
 .frame_up.nom_elev1 insert end $nom_elev
 focus -force .frame_up.nom_elev2
 bind .frame_up.nom_elev2 <Return> " .frame_up.cible configure -state normal ; .frame_up.nom_elev1 configure -state disabled ;\
						 focus -force .frame_up.cible "
bind .frame_up.nom_elev2 <KP_Enter> " .frame_up.cible configure -state normal ; .frame_up.nom_elev1 configure -state disabled ;\
						 focus -force .frame_up.cible"
 bind .frame_up.cible <Return> "valide_cible"
 bind .frame_up.cible <KP_Enter> "valide_cible"
 bind .frame_up.pas <Return> "init0"
 bind .frame_up.pas <KP_Enter> "init0"
 bind  .frame_middle.right.jeu1 <Return> "  valide1"
 bind  .frame_middle.right.jeu1 <KP_Enter> "valide1"
 bind  .frame_middle.right.jeu2 <Return> " valide2"
bind  .frame_middle.right.jeu2 <KP_Enter> "valide2"
 fais_grille
for {set y 0  } {$y < $init(nbrow) } {incr y } {
		set reste [expr $y % 4] ; set quot [expr $y / 4]  ; set m [expr $y / 2] ; set k 0 ; set k_deux 0
		 for {set x 0} {$x < $init(nbcol) } {incr x} {
			switch $reste  { 
				 0 { 
				
				  set i [expr [ expr $m * $init(nbcol) ] + $m + $k ]
				 set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ] 
				set tab_n(text_$i)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2 ]    -text $i -font  $font_case -fill black -tags tag_${i}  ]    
				
				.frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1 
				 incr k
				  if { $x == [expr $init(nbcol) -1] } {
				  incr i
					set tab_n(text_$i)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2  +$init(hlen)]    -text $i -font $font_case -fill black   -tags tag_${i}  ]   
								incr y 
				
				.frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1
				set k 0 
				}
				  }
				 
				 2 { 			 
				 set j  [expr [ expr $m * $init(nbcol) ] + $m  +$init(nbcol) - 1 -  $k_deux ]
				   set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ] 
				set tab_n(text_$j)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2 ]    -text $j -font $font_case -fill black -tags tag_${j}  ]    
				 .frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1
				 incr k_deux
				 if { $x == 0 } {
				  incr j
				set tab_n(text_$j)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2  +$init(hlen)]    -text $j -font $font_case -fill black -tags tag_${j}  ]   
				set z [expr $y +1] 
				
				.frame_middle.can  itemconfigure	 $tab(rect${z}_${x})  -fill bisque1
				set k_deux 1 
				}
				 
				 
				 }
				 default {}
				 }
		 
		 
				 }
 
}
##recup du dernier i bon si nbre col impair � v�rifier
if { $i >= $j }  { set dernier_n [ expr $init(nbcol) + $i] } else {set dernier_n [ expr $init(nbcol) +$j] }
 ## debug.frame_middle.right.deroul insert end $dernier_n
 }
main

proc valide_cible {} {
global nom_elev nom_autre premier max_n
set rep [.frame_up.cible get]
if { ![regexp {^\d+$} $rep ] || $rep <15  || $rep > $max_n } {
  .frame_up.cible delete 0 end  ; focus  .frame_up.cible
 tk_messageBox -message "[format [mc {Ou %1$s  n'est pas un nombre ou est trop petit (<15) ou trop grand (> %2$s)}] $rep $max_n ]" -title "[mc {Contraintes cible}]" -icon info -type ok 
 return
}
set nom_autre [.frame_up.nom_elev2 get]
##label .frame_up.choix_prem -text "[mc {Choix de l'�l�ve qui commence :}]" -bg bisque -font {helvetica 11}
##radiobutton .frame_up.prem -text  "$nom_elev" -variable premier  -value $nom_elev -fg black -bg blue -font {helvetica 11}
##radiobutton .frame_up.autre -text  " [.frame_up.nom_elev2 get]" -variable premier  -value $nom_autre -fg black -bg blue -font {helvetica 11}
##set premier  $nom_elev 
##place  .frame_up.choix_prem   -x 120 -y 0
##place .frame_up.prem  -x 140 -y 23  ; place .frame_up.autre -x 140 -y 45 
.frame_up.cible configure -state disabled
.frame_up.pas configure -state normal
focus -force .frame_up.pas 

}
###lancer par pas. Mais aussi Pour initialiser une nouvelle session avec autre cible et pas
proc init0 {} {
global nom_elev nom_autre pas cible tab_n premier n_session init max_n font_case tcl_platform
##bind  .frame_up.pas <Return> ""
.frame_up.nom_elev2 configure -state disabled 
set rep [.frame_up.pas get]
if { ![regexp {^\d+$} $rep ]  || [expr  [.frame_up.cible get] / $rep ] <= 4 || $rep >  [.frame_up.cible get] } {
 tk_messageBox -message "[format [mc {Ou %1$s  n'est pas un nombre. ou trop pr�s de %2$s ou > %3$s}] $rep [.frame_up.cible get] [.frame_up.cible get] ] " -title "[mc {Contraintes cible}]" -icon info -type ok 
.frame_up.pas delete 0 end ; focus .frame_up.pas ; return
}
tk_messageBox -message "\  \  \  \  \  \  \  \  \  \  \  $nom_elev , $nom_autre\n[mc {Choisissez celui qui d�bute, puis cliquer sur Allonsy!Pensez � VALIDER vos r�ponses par Entr�e}]" -title "[mc {Infos g�n�rales}]" -icon info -type ok 
incr n_session ; .frame_down.avis insert end  "Session n� $n_session, avec $nom_elev [  .frame_up.nom_elev2 get] cible : [.frame_up.cible get] ; pas :  [.frame_up.pas  get]\n"
##attention a nouvelle session  existe d�j�
	if {$n_session == 1} {
label .frame_middle.right.choix_prem -text "[mc {Choix de l'�l�ve qui effectue le premier calcul :}]" -bg bisque -font {helvetica 10 bold}
radiobutton .frame_middle.right.prem -text  "$nom_elev" -variable premier  -value $nom_elev -fg black  -font {helvetica 10}
radiobutton .frame_middle.right.autre -text  " [.frame_up.nom_elev2 get]" -variable premier  -value $nom_autre -fg black   -font {helvetica 10}
set premier  $nom_elev 
if { $tcl_platform(platform) != "unix" } {
place  .frame_middle.right.choix_prem   -x 0 -y 30
place .frame_middle.right.prem  -x  0 -y 75  ; place .frame_middle.right.autre -x 120 -y 75
				} else {
place  .frame_middle.right.choix_prem   -x 0 -y 30
place .frame_middle.right.prem  -x  0 -y 65  ; place .frame_middle.right.autre -x 120 -y 65
}
				}
set premier  $nom_elev	; set nom_autre  [.frame_up.nom_elev2 get]
.frame_middle.right.lancer configure -state normal 
.frame_up.nom_elev1 configure -state disabled
.frame_up.nom_elev2 configure -state disabled 
.frame_middle.right.lancer configure -state normal
.frame_up.cible configure -state disabled ; .frame_up.pas configure -state disabled
set cible [.frame_up.cible get] ; set pas [.frame_up.pas  get] ; set nom_autre [.frame_up.nom_elev2 get]
##.frame_middle.right.elev1 configure -text "Choix de\n$premier"
##.frame_middle.right.elev2 configure -text "Choix de\n$nom_autre"
## �param�trer 100 $dernier_n
if { [expr $init(nbrow) /2 ] != 0 } { set n_lig [expr  $init(nbrow) +1] } else { set n_lig $init(nbrow) }   
 set dernier [ expr [ expr $n_lig  / 2 * $init(nbcol) ] +  [ expr $init(nbrow) / 2] ]
 if { [expr $init(nbrow) /2 ] == 0 } {set dernier [expr $dernier - 2] } ; ##1+1(le 0)
 for {set k [expr $cible + 1] } { $k <=  $max_n } {incr k } {
.frame_middle.can  itemconfigure $tab_n(text_$k)  -fill  bisque1
 }
 .frame_middle.can  itemconfigure $tab_n(text_0)  -fill  bisque1
 
 for {set k 1 } { $k <=  $cible} {incr k }  {
 .frame_middle.can  itemconfigure $tab_n(text_0)  -font $font_case  
 }
 }
 if { [lindex $argv 1 ] == 1} {
 ##si lancement par bouton
set nom_autre "ordinateur"  ; .frame_up.nom_elev2 insert end $nom_autre
set cible 20 ; .frame_up.cible configure -state normal ; .frame_up.cible insert end $cible ; valide_cible
set pas 3 ;  .frame_up.pas insert end $pas
init0
}
 
 
####jeu du premier
 proc valide1 {} {
global nom_elev nom_autre pas cible partiel indice_p tab_n premier premier_coup autre n_essais font_case_jeu basedir
set rep [.frame_middle.right.jeu1 get] 
if { ![regexp {^\d+$} $rep ]  || $rep >= $pas || $rep == 0  || $rep == {} } {
tk_messageBox -message "[format [mc {%1$s est >= %2$s ou n'est pas un nombre ou est nul.}] $rep $pas]" -title "[mc {Contraintes saut}]" -icon info -type ok 
.frame_middle.right.jeu1 delete 0 end 
focus .frame_middle.right.jeu1 ; return
}
set partiel [expr $partiel + $rep]
if { [expr [ expr $partiel  == [expr $cible - $pas] ]  || [expr $partiel ==  [expr $cible -$pas -1] ] ] &&  $n_essais >= 3} {
set sur [	tk_messageBox -message "$premier : [mc {Est tu s�r(e) d'atteindre}] $cible. ?" -title "[mc {Strat�gie gagnante ?}]" -icon info -type yesno ]
	if { $sur == "no" } { 
	tk_messageBox -message "[mc {Tu peux modifier ta r�ponse}] $rep. " -title "[mc {Test conviction}]" -icon info -type ok 
	.frame_middle.right.jeu1 delete 0 end ; set partiel [expr $partiel - $rep]
	focus .frame_middle.right.jeu1 ; return
	}
}
##set partiel [expr $partiel + [.frame_middle.right.jeu1 get]]
if {  $partiel  > $cible } {
tk_messageBox -message "$cible [mc {est d�pass�e!!Recommence.}]" -title "[mc {Contraintes saut}]" -icon info -type ok 
##set partiel [expr $partiel - [.frame_middle.right.jeu2 get]]
.frame_middle.right.jeu1 delete 0 end ; focus .frame_middle.right.jeu1 ;  set partiel [expr $partiel - $rep] ; return
}
.frame_middle.right.jeu1 configure -state disabled
##set partiel [expr $partiel + [.frame_middle.right.jeu1 get]]
.frame_middle.right.deroul configure -state normal
.frame_middle.right.deroul insert end "$partiel "
.frame_middle.right.deroul configure -state disabled 
for {set i [expr $partiel - $rep + 1] } {$i <= $partiel } {incr i} {
 .frame_middle.can  itemconfigure $tab_n(text_$i)  -fill  red -font $font_case_jeu
} 
##fini
	


	if { $partiel == $cible} {
	tk_messageBox -message "[format [mc {BRAVO!%1$s a atteint %2$s.}] $premier $cible ] " -title "[mc {Fin de partie}]" -icon info -type ok 
	.frame_middle.can create image  200 100     -image [image create photo -file [file join  $basedir images bien.png]]  -tags img
	.frame_middle.right.jeu1 configure -state disabled
	.frame_middle.right.jeu2 configure -state disabled
	.frame_down.bilan configure -text "[format [mc {Bilan:%1$s a atteint %2$s}] $premier $cible ] .\n$premier ayant d�but�. Avec : \n[.frame_middle.right.deroul  get] "  -font {helvetica 10}
		.frame_up.rec configure -state normal
		.frame_middle.right.n_essai configure -state normal
##		.frame_middle.right.lancer configure -state normal
		.frame_down.avis insert end "$premier [ .frame_middle.right.deroul get] $premier [mc {a atteint}] $cible\n"
				return
		}
if {$autre == "ordinateur" } {
		.frame_middle.right.jeu2 configure -state normal 
		.frame_middle.right.jeu2 delete 0 end
		set reste0 [expr $cible % $pas] ; set reste_p [expr $partiel % $pas] ; set quot [expr $partiel / $pas]
	
		if { $reste0 == 0} {
						if { $reste_p == 0 } {
								set rep1 1 } else {
									set rep1 [ expr [expr $quot * $pas  + $pas ]  - $partiel ] } 
						} else {
								if { $reste_p == $reste0 } {
										set rep1 1 }  else {
											set rep1 [expr  $reste0 - $reste_p ]
												}		
	}	

	if { $reste_p == $reste0 } { set rep1 1} else { 
				if  { $reste0 < $reste_p} {
				set rep1 [ expr $pas - $reste_p +$reste0] }
				if { $reste_p < $reste0 }  {
				set rep1 [expr $reste0 - $reste_p ] }
				}
				.frame_middle.right.jeu2 insert end $rep1
##			set partiel [expr $partiel + $rep1]
			valide2 ; return
		
		}
		.frame_middle.right.jeu2 configure -state normal
		.frame_middle.right.jeu2  delete 0 end
		focus .frame_middle.right.jeu2
##.frame_middle.right.jeu1  delete 0 end
}
proc valide2 {} {
global nom_elev nom_autre pas cible  partiel indice_p tab_n premier autre font_case_jeu n_essais basedir
.frame_middle.right.jeu1 delete 0 end
set rep [.frame_middle.right.jeu2 get]
if { ![regexp {^\d+$} $rep ] || $rep >= $pas  || $rep == 0} {
tk_messageBox -message "[format [mc {%1$s est >= %2$s ou n'est pas un nombre ou est nul.}] $rep $pas]" -title "[mc {Contraintes saut}]" -icon info -type ok 
.frame_middle.right.jeu2 delete 0 end ; focus .frame_middle.right.jeu2 ; return
}
set partiel [expr $partiel + $rep ]
if { [expr [ expr $partiel  == [expr $cible - $pas] ]  || [expr $partiel ==  [expr $cible -$pas -1] ] ]  &&  $n_essais >= 3} {
##erreur $premier n'et pas forc�m�nt celui qui est lr 1er � jouer
set sur [	tk_messageBox -message "$autre : [mc {Est tu s�r(e) d'atteindre}] $cible. ?" -title "[mc {Strat�gie gagnante ?}]" -icon info -type yesno ]
	if { $sur == "no" } { 
	tk_messageBox -message "$autre [mc {Tu peux modifier ta r�ponse}] $rep. " -title "[mc {Test conviction}]" -icon info -type ok 
	.frame_middle.right.jeu2 delete 0 end ; set partiel [expr $partiel - $rep]
	focus .frame_middle.right.jeu2 ; return
	}
}

if { $partiel > $cible } {
tk_messageBox -message "$cible [mc {est d�pass�e!!Recommence.}]" -title "[mc {Contraintes saut}]" -icon info -type ok 
set partiel [expr $partiel - $rep]
.frame_middle.right.jeu2 delete 0 end 
 focus .frame_middle.right.jeu2
return
}
.frame_middle.right.jeu2 configure -state disabled
.frame_middle.right.deroul configure -state normal
.frame_middle.right.deroul insert end "$partiel "
.frame_middle.right.deroul configure -state disabled

for {set i [expr $partiel - $rep + 1 ]} {$i <= $partiel } {incr i} {
 .frame_middle.can  itemconfigure $tab_n(text_$i)  -fill green1 -font $font_case_jeu
} 
##fini
	if { $partiel == $cible} {
	tk_messageBox -message "[format [ mc {BRAVO!%1$s a atteint %2$s. %3$s ayant d�but�}] $nom_autre $cible $premier]\nAvec [.frame_middle.right.deroul  get]" -title "[mc {Fin de partie}]" -icon info -type ok 
.frame_middle.can create image  200 100     -image [image create photo -file [file join  $basedir images bien.png]]  -tags img
	.frame_down.bilan configure -text "[format [mc {Bilan : %1$s a atteint %2$s Avec}] $nom_autre $cible  ] [.frame_middle.right.deroul  get]" -font {helvetica 10}
	.frame_middle.right.jeu1 configure -state disabled
	.frame_middle.right.jeu2 configure -state disabled
	.frame_up.rec configure -state normal
		.frame_middle.right.n_essai configure -state normal
##		.frame_middle.right.lancer configure -state normal
		.frame_middle.right.n_essai configure -state normal
		.frame_down.avis insert end "$premier [ .frame_middle.right.deroul get]  $autre [mc {a atteint}] $cible\n"
		return
		
		}

	if  { $premier == "ordinateur" } {
###doit gagner sauf s reste0 = 0 auquel cas il faut essayer	
		set reste0 [expr $cible % $pas] ; set reste_p [expr $partiel % $pas] ; set quot [expr $partiel / $pas]	
		if { $reste0 == 0 } {
					if {  $reste_p  == 0}  {
								set rep1 1} else {	
											set rep1 [ expr [expr $quot * $pas  + $pas ]  - $partiel ] }  
						 }  else  {
								set rep1 [ expr $pas - $rep]
							
						}									
										
		.frame_middle.right.jeu1 configure -state normal
		.frame_middle.right.jeu2 delete 0 end 
		.frame_middle.right.jeu1 delete 0 end 
		.frame_middle.right.jeu1 insert end $rep1
##		set partiel [expr $partiel + $rep1] 
###		focus .frame_middle.right.jeu2 ;  .frame_middle.right.jeu2  delete 0 end 

			 valide1 ; return
		}  else  {	focus .frame_middle.right.jeu1 }
	      .frame_middle.right.jeu1 configure -state normal
	      .frame_middle.right.jeu1   delete 0 end
		 focus .frame_middle.right.jeu1 
		.frame_middle.right.jeu2  delete 0 end
}
#####Pour lancer le ping pong et finir une partie
proc lancer {} {
global nom_elev nom_autre pas cible tab_n init  partiel indice_p premier n_essais premier_coup choix_prem autre
if { $premier == $nom_autre } {
	 set autre $nom_elev} else {
	set autre [ .frame_up.nom_elev2 get]
	}
 
	.frame_middle.right.jeu1 configure    -state normal ; .frame_middle.right.jeu2 configure  -state normal
	.frame_middle.right.jeu1   delete 0 end ;  .frame_middle.right.jeu2   delete 0 end
.frame_middle.right.elev1 configure -text "[mc {Choix de}]\n$premier"
.frame_middle.right.elev2 configure -text "[mc {Choix de}]\n$autre"
if { $premier == $nom_elev } {set coul_p "pink" ; set coul_a "green" } else { set coul_p green ; set coul_a pink}
.frame_middle.right.prem configure -bg $coul_p
.frame_middle.right.autre configure -bg $coul_a
.frame_up.message2 configure -text "[format [mc {Comment  �tre s�r d'atteindre %1$s en premier ?}] $cible]"
.frame_up.message1 configure -text "[format [mc {C'est %1$s qui d�bute puis %2$s.Il faut atteindre %3$s par pas de}] $premier $autre $cible  ] 1,2.. [expr $pas -1]"
focus -force .frame_middle.right.jeu1 ; .frame_middle.right.jeu2 configure -state disabled
.frame_middle.right.lancer configure -state disabled
.frame_middle.right.prem configure -state disabled ; .frame_middle.right.autre configure -state disabled 
.frame_middle.right.n_essai configure -state disabled
.frame_up.rec configure -state disabled
set  partiel 0 ; set indice_p 0 ; incr n_essais
if { $premier == "ordinateur" } {
		set reste0  [expr $cible  % $pas ]
		if { $reste0 == 0  } { set premier_coup 1 } else {set premier_coup $reste0 }
		.frame_middle.right.jeu1 insert end $premier_coup ; focus .frame_middle.right.jeu1
##		.frame_middle.right.deroul insert end $premier_coup
		valide1
		}
}

proc  n_session {} {
global nom_elev nom_autre pas cible tab_n premier n_session n_essais font_case max_n
catch {.frame_middle.can delete tags img }
.frame_up.cible configure -state normal ; .frame_up.pas configure -state normal
.frame_middle.right.prem configure -state normal ; .frame_middle.right.autre configure -state normal
#.frame_middle.right.jeu2 configure -state disabled ; .frame_middle.right.jeu1  configure -state normal
.frame_middle.right.deroul configure -state normal
.frame_middle.right.deroul delete 0 end ; .frame_middle.right.jeu1 delete 0 end ; .frame_middle.right.jeu2 delete 0 end
.frame_up.cible delete 0 end ; .frame_up.pas delete 0 end ; .frame_down.avis delete @0,0  end
.frame_up.rec configure -state disabled
.frame_middle.right.n_essai configure   -state disabled
 .frame_up.message1 configure  -text "[mc {C'est qui d�bute puis Il faut atteindre ??  pas  � pas.}]"  
 .frame_up.message2 configure -text  "[mc {Comment �tre s�r d'atteindre en premier?}]" 
set n_essais 0
##refaire la bande avant cible
for {set k 1 } { $k <=  $max_n } {incr k } {
.frame_middle.can  itemconfigure $tab_n(text_$k)  -fill  black -font $font_case 
 }
focus  -force .frame_up.cible
##incr n_session
}
proc n_essai {} {
global nom_elev nom_autre pas cible tab_n premier n_session n_essais font_case 
catch {.frame_middle.can delete tags img }
.frame_middle.right.prem configure -state normal ; .frame_middle.right.autre configure -state normal
.frame_middle.right.deroul configure -state normal
.frame_middle.right.deroul delete 0 end ; .frame_middle.right.jeu1 delete 0 end ; .frame_middle.right.jeu2 delete 0 end
.frame_up.rec configure -state disabled
.frame_middle.right.lancer configure -state normal
 .frame_middle.right.jeu1   delete 0 end ;  .frame_middle.right.jeu2   delete 0 end
.frame_middle.right.jeu1 configure -state disabled ; .frame_middle.right.jeu2 configure -state disabled
.frame_down.bilan configure -text "Bilan : "
incr n_essais
for {set k 1 } { $k <=  $cible } {incr k } {
.frame_middle.can  itemconfigure $tab_n(text_$k)  -fill  black -font $font_case 
 }
tk_messageBox -message "[mc {ATTENTION Choisir le premier puis cliquer sur Allons y!}] " -title "[mc {Nouvel essai}]" -icon info -type ok
 }
proc clignote {  qui nclic} {
global tab_n font_case
 for  {set cl 1} {$cl <= $nclic} {incr cl} {
after 200
.frame_middle.can itemconfigure $qui   -font  $font_case  -fill yellow 
 update
after 200
 .frame_middle.can itemconfigure  $qui -font $font_case  -fill [lindex $tab_chaleur($chaleur) 1] 
update
}

}
