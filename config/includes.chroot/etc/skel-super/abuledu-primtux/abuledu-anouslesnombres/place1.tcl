############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : calapa.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 08/02/2003
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: place1.tcl,v 1.1.1.1 2004/04/16 11:45:52 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#calapa.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome

global listdata arg1 arg2 arg3 arg4 gettext cat nbcat listanimaux listhabitat listgen reussite

#listgen : liste cat�gorie (barque, calapa, etc ...)
#listdata : liste pour le sc�nario en cours
# activit� index_dans_le_sc�nario flag_parcours index_dans_le_parcours
#arg1 : type de activit� (calapa, barque, etc)
#arg2 : num�ro de l'activit� dans le sc�nario 
set c .frame.c
set arg1 [lindex $argv 0]
set arg2 [lindex $argv 1]
set arg3 [lindex $argv 2]
set arg4 [lindex $argv 3]
set reussite 0
#ouverture du fichier des noms des animaux, r�cup�r� dans listanimaux
set ext .ani
set f [open [file join $arg1$ext] "r"]
set listanimaux [gets $f]
close $f

#ouverture du fichier des noms des habitats, r�cup�r� dans listhabitat

set ext .hab
set f [open [file join $arg1$ext] "r"]
set listhabitat [gets $f]
close $f

#interface
. configure -background #ffff80 
frame .frame -width 640 -height 420 -background #ffff80
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 400 -background #ffff80 -highlightbackground #ffff80
pack $c

#ouverture du fichier calapa.conf ou barque.conf ...
set ext .conf
set f [open [file join $Home reglages $arg1$ext] "r"]
set listgen [gets $f]
close $f

image create photo bien -file [file join sysdata pbien.gif] 
image create photo pass -file [file join sysdata ppass.gif]
image create photo mal -file [file join sysdata pmal.gif]
image create photo neutre -file [file join sysdata pneutre.gif]

#procedures
####################################################""
proc recommence {c} {
after cancel animation $c
place $c
}

###########################################################################
proc animation {c} {
global tabanim
#set suffix g
#tabanim : id animal, deplacement horizontal, d�placement vertical
foreach item [array names tabanim] {
#$c raise $item
if {[$c itemcget $item -state] != "hidden"} {
set coord [$c bbox $item]
if {[lindex $coord 0] < 0 } {set tabanim($item) 1\040[lindex $tabanim($item) 1]}
if {[lindex $coord 2] > 600 } {set tabanim($item) [expr -1]\040[lindex $tabanim($item) 1]}
if {[lindex $coord 1] < 0 } {set tabanim($item) [lindex $tabanim($item) 0]\0401}
if {[lindex $coord 3] > 190 } {set tabanim($item) [lindex $tabanim($item) 0]\040[expr -1]}

if {[lsearch [$c gettags $item] anim1] != -1} {
if {[llength [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]]] < 3} {    
$c move $item [lindex $tabanim($item) 0] [lindex $tabanim($item) 1]
} else {
set tmp [expr int(rand()*2)]
switch $tmp {
0 {
#mise � jour param d�placement
set tabanim($item) [expr [lindex $tabanim($item) 0]*(-1)]\040[lindex $tabanim($item) 1]
################################################
#set imgtmp [$c itemcget $item -image]
#if {[string index $imgtmp end] != $suffix} {
#$c itemconfigure $item -image $imgtmp$suffix
#}
################################################
}
1 {
#mise � jour param d�placement
set tabanim($item) [lindex $tabanim($item) 0]\040[expr [lindex $tabanim($item) 1]*(-1)]
###########################################################
#set imgtmp [$c itemcget $item -image]
#if {[string index $imgtmp end] == $suffix} {
#$c itemconfigure $item -image [string range $imgtmp 0 end-1]
#}
####################################################
}
}
#deplacement animal si pas de collision avec un autre
if {[llength [$c find overlapping [expr [lindex $coord 0] + [lindex $tabanim($item) 0] ]  [expr [lindex $coord 1] + [lindex $tabanim($item) 1]] [expr [lindex $coord 2] + [lindex $tabanim($item) 0]] [expr [lindex $coord 3] + [lindex $tabanim($item) 1] ]]] < 3} {
$c move $item [lindex $tabanim($item) 0] [lindex $tabanim($item) 1]
}
}
}
}
}
#appel r�cursif
after 100 animation $c

}


#proc�dure principale
proc place {c} {
global cat nbens indens serieencours nbcat listdata arg1 arg2 gettext listanimaux listhabitat tabanim listgen classens user basedir progaide
#listeanimaux : liste interm�diaire pour le melange des animaux
#nbens : tableau du nombre d'animaux par ensemble (nbens(i))
#indens : tableau de l'index de l'animal par ensemble (indens(i))
#cat : tableau des ensembles (cat(0) pour le ciel, cat(1) et/ou cat(2)) cat(0) = {{0 1} {0 1}}
#classens tableau du nombre d'animaux par classe (classens(1) classens(2))
#recup de l'activit� dans listdata
set listdata [lindex $listgen $arg2]
wm title . "[mc $arg1] [lindex [lindex $listdata 0] 0]"
#wm title . "$arg1 $listdata"
catch {destroy .bframe}
frame .bframe -background #ffff80
pack .bframe -side bottom

set nbcat [expr [llength $listdata] - 2]
set cat(0) [lindex $listdata 1]
for {set i 1} {$i <= $nbcat} {incr i 1} {
set cat($i) [lindex $listdata [expr $i + 1]]
#classens est rep�r� par sa position dans l'editeur : ensemble1 -> classens 3
set classens($i) [expr [lindex [expr \$cat($i)] 1] +2]
}
switch $arg1 {
"calapa" {
	switch [lindex [lindex $listdata 0] 1] {
	2 {source calapaenum.tcl
	label .bframe.consigne -background #ffff80 -text [mc {Clique sur tous les animaux d'un meme habitat, puis clique sur un habitat}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	}
	1 {source calapaenum.tcl
	label .bframe.consigne -background #ffff80 -text [mc {Clique sur tous les animaux d'un meme habitat, puis clique sur un habitat}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	}
	0 {source calapatri.tcl
	label .bframe.consigne -background #ffff80 -text [mc {Clique sur un ou plusieurs animaux d'un meme habitat, puis clique sur un habitat}]
	grid .bframe.consigne -column 1 -padx 10 -row 0
	}
	}
set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"

}
"constellation" {
source constellation.tcl
label .bframe.consigne -background #ffff80 -text [mc {Clique sur les animaux, puis clique sur un habitat}]
grid .bframe.consigne -column 1 -padx 10 -row 0
set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"
}
"nombre" {
source constellation.tcl
label .bframe.consigne -background #ffff80 -text [mc {Clique sur les animaux, puis clique sur un habitat}]
grid .bframe.consigne -column 1 -padx 10 -row 0
set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"
}
"barque" {
	source barque.tcl
set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"
	if {[lindex [lindex $listdata 0] 1] != 2} {
	label .bframe.consigne -background #ffff80 -text [mc {Place les barques}]
	} else {
	label .bframe.consigne -background #ffff80 -text [mc {Observe puis appuie sur le pouce}]
	}
	grid .bframe.consigne -column 1 -padx 10 -row 0
	}
}

label .bframe.tete -background #ffff80 -image neutre
grid .bframe.tete -column 0 -padx 10 -row 0

set listeanimaux ""
for {set i 0} {$i <= 2} {incr i 1} {
set nbens($i) 0
set indens($i) 0
}
set serieencours ""

#animal mare et autre
	for {set i 1} {$i <= $nbcat} {incr i 1} {
	set intervalle [expr [lindex $cat($i) 4] - [lindex $cat($i) 3] + 1] 
	set nbens($i) [expr int(rand()*$intervalle) + [lindex $cat($i) 3]]
	if {$arg1 != "calapa"} {
	set nbens($i) [expr int($nbens($i)/$classens($i))*$classens($i)]
	}
	}

	for {set j 1} {$j <= $nbcat} {incr j 1} {
		for {set i 1} {$i <= $nbens($j)} {incr i 1} {
		#format : nom de l'animal / ensemble de reference/ facteur d'animation
		switch [lindex $cat($j) 0] {
		1 {set tmpind 0}
		2 {set tmpind 1}
		3 {set tmpind [expr int(rand()*2)]}
		}
############################################################"
		#if {[lindex $cat($j) 0] != 2 } {
		#set tmpind [lindex $cat($j) 0]
		#} else {
		#set tmpind [expr int(rand()*2)]
		#}
########################################################""
		lappend listeanimaux [lindex [lindex $listanimaux [expr [lindex $cat($j) 1] +1] ] $tmpind ]\040ens($j)\040[lindex $cat($j) 2]
		}
	}

#animaux brouilleurs, entre 3 et 10 au total, (ou aucun,suivant le sc�nario)
if {[llength $cat(0)] != 0} {
set nbens(0) [expr [lindex [lindex $cat(0) 0] 2] - 2]
} else {
set nbens(0) [expr int(rand()*7) + 3]
}
	if {[llength $cat(0)] ==1} {
		for {set i 1} {$i <= $nbens(0)} {incr i 1} {
		lappend listeanimaux [lindex [lindex $listanimaux 0] [lindex [lindex $cat(0) 0] 0]]\040ens(0)\040[lindex [lindex $cat(0) 0] 1]
		}
	}

	if {[llength $cat(0)] ==2} {
	set nbbrouilleur1 [expr int(rand()*($nbens(0) +1))]
	set nbbrouilleur2 [expr $nbens(0) - $nbbrouilleur1]
		for {set i 1} {$i <= $nbbrouilleur1} {incr i 1} {
		lappend listeanimaux [lindex [lindex $listanimaux 0] [lindex [lindex $cat(0) 0] 0]]\040ens(0)\040[lindex [lindex $cat(0) 0] 1]
		}
		for {set i 1} {$i <= $nbbrouilleur2} {incr i 1} {
		lappend listeanimaux [lindex [lindex $listanimaux 0] [lindex [lindex $cat(0) 1] 0]]\040ens(0)\040[lindex [lindex $cat(0) 0] 1]
		}
	}

#on complete la liste avec des trous
	for {set i 1} {$i <= [expr 70 - [llength $listeanimaux]]} {incr i 1} {
	lappend listeanimaux {trou null}
	}

#on efface et on place les images, apr�s avoir touill�
	for {set i 1} {$i <= [llength $listeanimaux]} {incr i 1} {
	set t1 [expr int(rand()*[llength $listeanimaux])]
	set t2 [expr int(rand()*[llength $listeanimaux])]
	set tmp [lindex $listeanimaux $t1]
	set listeanimaux [lreplace $listeanimaux $t1 $t1 [lindex $listeanimaux $t2]]
	set listeanimaux [lreplace $listeanimaux $t2 $t2 $tmp]
	}

$c delete all
# le ciel
$c create rect 0 0 650 190 -fill #78F8FF
#set suffix g
	for {set i 1} {$i <= [llength $listeanimaux]} {incr i 1} {
		if {[lindex [lindex $listeanimaux [expr $i -1]] 0] != "trou"} {
		image create photo [lindex [lindex $listeanimaux [expr $i -1]] 0]$i -file [file join images [lindex [lindex $listeanimaux [expr $i -1]] 0]]
		set ypos [expr int([expr $i - 1]/14)*45 + 25]
		set xpos [expr fmod([expr $i -1],14)*45 +25]
		#les animaux sont taggu�s avec drag, uidens(i) pour l'appartenance � ens(i) et anim1 ou anim0
		set id [$c create image $xpos $ypos -image [lindex [lindex $listeanimaux [expr $i -1]] 0]$i -tags "drag uid[lindex [lindex $listeanimaux [expr $i -1]] 1] anim[lindex [lindex $listeanimaux [expr $i -1]] 2]"] 
		if {[lindex [lindex $listeanimaux [expr $i -1]] 2]==1} {
		#l'id de l'image est r�cup�r�e pour tabanim
		set tabanim($id) "1 1"
		}
		}
	}
#les mares, pr�s et autres sont taggu�s avec tgensi
	switch $nbcat {
		1 { $c create image 310 300 -image [image create photo imgens1 -file [file join images [lindex $listhabitat [lindex $cat(1) 1]] ]] -tags tgens1}

		2 {	$c create image 160 300 -image [image create photo imgens1 -file [file join images [lindex $listhabitat [lindex $cat(1) 1]]]] -tags tgens1
			$c create image 480 300 -image [image create photo imgens2 -file [file join images [lindex $listhabitat [lindex $cat(2) 1]]]] -tags tgens2
		}
	}
set str ""
switch $arg1 {
	"barque" {set str "\173[mc {Nombre d'animaux brouilleurs :}]$nbens(0)\175\040\173[mc {Nombre d'animaux ensemble 1 :}]$nbens(1)\175\040\173[mc {Taille de la grande barque :}][lindex [lindex $listdata 0] 2]\175"}
	default {
		switch $nbcat {
		1 {set str "\173[mc {Nombre d'animaux brouilleurs :}]$nbens(0)\175\040\173[mc {Nombre d'animaux ensemble 1 :}]$nbens(1)\175"}
		2 {set str "\173[mc {Nombre d'animaux brouilleurs :}]$nbens(0)\175\040\173[mc {Nombre d'animaux ensemble 1 :}]$nbens(1)\175\040\173[mc {Nombre d'animaux ensemble 2 :}]$nbens(2)\175"}
		}
	}

	
}
enregistreeval $arg1 [lindex [lindex $listdata 0] 0] $str $user 

animation $c
}


#appel de la proc�dure principale
place $c

#gestion des ev�nements
if {$arg1== "calapa" || $arg1== "constellation" || $arg1== "nombre"} {
$c bind tgens1 <ButtonRelease-1> "verif $c ens(1)"
$c bind tgens2 <ButtonRelease-1> "verif $c ens(2)"


$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
$c bind drag <ButtonPress-1> "itemStartDrag $c %x %y"
$c bind drag <B1-Motion> "itemDrag $c %x %y"
}


#a la fin de l'exercice
proc geresuite {c} {
global arg2 arg3 arg4 listgen Home reussite
$c dtag all tgens1
$c dtag all tgens2

catch {destroy .bframe}
frame .bframe -background #ffff80
pack .bframe -side bottom
label .bframe.consigne -background #ffff80 -text [mc {Bravo, c'est gagne!}]
grid .bframe.consigne -column 1 -padx 30 -row 0
if {$arg2 < [expr [llength $listgen] -1] && $arg3 == 0} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suite $c"
grid .bframe.suite -column 2  -padx 20 -row 0 -sticky w

}

set f [open [file join $Home reglages parcours.conf] "r"]
set listparcours [gets $f]
close $f

if {$arg4 < [expr [llength $listparcours] -1] && $arg3 == 1} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suiteparcours $c $arg4"
grid .bframe.suite -column 2  -padx 20 -row 0 -sticky w
}

button .bframe.recommencer -image [image create photo -file [file join sysdata again2.gif] ] -background #ff80c0 -command "recommence $c"
grid .bframe.recommencer -column 3  -padx 20 -row 0 -sticky w
label .bframe.reussite -background #ffff80 -text "[mc {Nombre d'erreurs :}] $reussite"
grid .bframe.reussite -column 4 -padx 30 -row 0

}
