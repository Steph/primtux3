set etapes 1
set niveaux {0 1 2 4}
::1
set niveau 2
set ope {{4 3} {16 3}}
set interope {{4 16 4} {8 28 4}}
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1])*4 + [lindex [lindex $ope 1] 0]]
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1])*4 + [lindex [lindex $ope 0] 0]]
set volatil 0
set operations [list [list [expr $ope1 ]+[expr $ope2 - $ope1]] [list [expr $ope2 - $ope1]+[expr $ope1]] [list [expr $ope2]-[expr $ope1]]]
set editenon "($ope1 timbres sont d�j� plac�s)"
set enonce "L'album.\nJ'ai $ope2 timbres en tout dans mon album.\nCombien y en a-t-il qu'on ne voit pas?"
set cible [list [list 6 5 [list timbrep.gif $ope1] source0]]
set intervalcible 20
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 80
set orgsourcexorig 600
set source {timbre.gif}
set orient 1
set labelcible {Album}
set quadri 0
set reponse [list [list {1} [list {Il y a} [expr $ope2 - $ope1] {timbres qu'on ne voie pas.}]]]
set ensembles [list [expr $ope2 - $ope1]]
set canvash 350
set c1height 160
set opnonautorise {0 1}
::






