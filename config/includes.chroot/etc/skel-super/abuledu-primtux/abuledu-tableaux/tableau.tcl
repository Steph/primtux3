#!/bin/sh
#tableau.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: andre.connes@toulouse.iufm.fr
#  Date    : 24/04/2002 modification : 16/08/2004
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   Andre Connes
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
# 
#  *************************************************************************

source tableaux.conf
set didacticiel [lindex $argv 0]
source msg.tcl

if { $glob(platform) == "windows" } {
  set f [open [file join $glob(home_tableaux) reglages trace_user] "r"]
  set glob(trace_user) [gets $f]
  close $f
}

# Décommenter la ligne suivante si Img est installée
#package require Img

########################### On crée la fenêtre principale###########################

# placement de la fenêtre en haut et à gauche de l'écran

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.99)]x[expr int([winfo vrootheight .]*0.9)]+0+0
. configure -bg $glob(bgcolor)
wm title . $didacticiel

frame .frame -width $glob(width) -height $glob(height) -bg $glob(bgcolor)
pack .frame -side top -fill both -expand yes

##############################################
# init_difficulte_max
####
proc init_difficulte_max {} {
  global glob c didacticiel difficulte_max

  #lecture du parcours de l'élève
  set lt_array($didacticiel) "-1 -1 -1 -1 -1" ;# sentinelle
  #lire le fichier utilisateur
  set f [open [file join $glob(trace_user).level] "r"]
  while { ! [eof $f] } {
    set line [gets $f]
    set lt_array([lindex $line 0]) [lrange $line 1 end] 
  }
  close $f
  #traiter le didacticiel
  for {set i 0} {$i <= 4} {incr i 1} {
    set parcours($i) [lindex $lt_array($didacticiel) $i]
  }
  #
  #rechercher la difficulté maximale
  #
  for { set i 0 } { $i <= 4 } { incr i } {
    set ktmp [expr $parcours($i) + 1]
    if { $ktmp < 4 } {
      set difficulte_max($i) $ktmp
    } else {
      set difficulte_max($i) 4
    }
  }
} ;# init_difficulte_max


########################On crée un canvas###########################################

set c .frame.c
canvas $c -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
pack $c -expand true
# pack $c

init_difficulte_max

for { set i 0 } { $i <= 4 } { incr i } {
  button $c.menu${i} -image [image create photo -file [file join sysdata menu${i}.gif]] \
    -cursor heart \
    -command "init_difficulte_max; exec wish tableau$i.tcl $didacticiel $difficulte_max($i)"
  label $c.lab_menu${i} -text [mc sub_menu${i}]
  grid $c.menu${i} -column 0 -row $i -padx 4 -pady 4 -sticky e
  grid $c.lab_menu${i} -column 1 -row $i
}

  button $c.quitter \
	-image [image create photo -file [file join sysdata quitter.gif]] \
	-borderwidth 5 -cursor heart \
	-command exit
  grid $c.quitter -column 2 -row 4 -sticky e
  
