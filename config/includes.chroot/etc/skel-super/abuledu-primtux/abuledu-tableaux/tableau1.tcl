#!/bin/sh
#tableau1.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: andre.connes@toulouse.iufm.fr
#  Date    : 24/04/2002 modification : 16/08/2004
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   Andre Connes
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
# 
#  *************************************************************************

set didacticiel [lindex $argv 0]
set difficulte [lindex $argv 1]

source tableaux.conf
source msg.tcl

if { $glob(platform) == "windows" } {
  set f [open [file join $glob(home_reglages) trace_user] "r"]
  set glob(trace_user) [gets $f]
  close $f
}

# Decommenter la ligne suivante si Img est installe
# package require Img

# nbquestions : nbre de questions a poser
# iquestion : num�ro de la question courante
# nbessai : nbre d'essais pour repondre a une question
# nbjustes : nbre de reponses justes
#set iquestion 0
set nbquestions 5
#set nbjustes 0

########################### On cr�e la fen�tre principale###########################

# placement de la fen�tre en haut et � gauche de l'�cran

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.99)]x[expr int([winfo vrootheight .]*0.9)]+0+0
. configure -bg $glob(bgcolor)

frame .frame -width $glob(width) -height $glob(height) -bg $glob(bgcolor)
pack .frame -side top -fill both -expand yes

########################On cr�e un canvas###########################################
# charg� d'accueillir les sorties graphiques,
# qui peuvent �tre des images, des textes, des formes g�om�triques ...

set c .frame.c
canvas $c -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
pack $c -expand true 

# on cr�e une frame en bas en avant-derni�re ligne avec
#   le score affich� sous forme de t�tes (bien passable mal)
#   un bouton 'continuer'

frame .bframe -bg $glob(bgcolor)
pack .bframe -side top -expand true

  image create photo pbien -file [file join sysdata pbien.gif] 
  image create photo ppass -file [file join sysdata ppass.gif]
  image create photo pmal -file [file join sysdata pmal.gif]
  image create photo pneutre -file [file join sysdata pneutre.gif]

for {set i 1} {$i <= $nbquestions} {incr i 1} {
  label .bframe.lab$i -bg $glob(bgcolor) -width 4
  grid .bframe.lab$i -column [expr $i -1] -row 1 -sticky e
  .bframe.lab$i configure -image pneutre -width 80
}

button .bframe.but_gauche -image \
  [image create photo fgauche -file [file join sysdata fgauche.gif]] -command "recule; commence $c"
grid .bframe.but_gauche -column [expr $nbquestions + 1] -row 1
button .bframe.quitter_minus -image \
	[image create photo -file [file join sysdata quitter_minus.gif]] \
	-command exit
grid .bframe.quitter_minus -column [expr $nbquestions + 2] -row 1
button .bframe.but_droite -image \
  [image create photo fdroite -file [file join sysdata fdroite.gif]] -command "avance; commence $c"
grid .bframe.but_droite -column [expr $nbquestions + 3]  -row 1

# on cr�e une frame sur la derni�re ligne avec
#   une �tiquette de consigne/controle de la r�ponse (2e ligne)
frame .cframe -bg $glob(bgcolor)
pack .cframe -side bottom -expand true
label .cframe.control -bg $glob(bgcolor) -font 12x24
pack .cframe.control

##############################################
# ###
# recule
# ###
proc recule {} {
  global difficulte
  if { $difficulte > 0 } {
    incr difficulte -1 
  }
}

# ###
proc avance {} {
  global glob didacticiel difficulte

  # d�terminer les difficult�s max

  #lecture du parcours de l'�l�ve 
    #lire le fichier utilisateur
    set f [open [file join $glob(trace_user).level] "r"]
    while { ! [eof $f] } {
      set line [gets $f]
      set lt_array([lindex $line 0]) [lrange $line 1 end] 
    }
    close $f
    #traiter le didacticiel
    for {set i 0} {$i <= 4} {incr i 1} {
      set parcours($i) [lindex $lt_array($didacticiel) $i]
    }

  #autoriser/interdire
  for { set i 0 } { $i <= 4 } { incr i } {
    set ktmp [expr $parcours($i) + 1]
    if { $ktmp < 4 } {
      set difficulte_max($i) $ktmp
    } else {
      set difficulte_max($i) 4
    }
  }
  if { $difficulte < $difficulte_max(1) } {
    incr difficulte
  }
} ;# avance

####
# lire le fichier tableau de jeu
####
proc lire_tableau {} {
  global didacticiel taillerect imgperso globcol globrow  imgarr
  set f [open [file join $didacticiel tableau.jeu] "r"]
  set taillerect [gets $f]
  set imgperso [gets $f]
  set r [expr int(rand()*2)]
  if { $r == 0 } {
    set globcol [gets $f]
    for {set i 1} {$i <= $globcol} {incr i 1} {
      set imgarr(0,$i) [gets $f]
    }
    set globrow [gets $f]
    for {set i 1} {$i <= $globrow} {incr i 1} {
      set imgarr($i,0) [gets $f]
    }
    for { set i 1 } { $i <= $globrow } {incr i 1 } {
      for { set j 1 } { $j <= $globcol } {incr j 1 } {
        set imgarr($i,$j) [gets $f]
      }
    }
  } else {
    set globrow [gets $f]
    for {set i 1} {$i <= $globrow} {incr i 1} {
      set imgarr($i,0) [gets $f]
    }
    set globcol [gets $f]
    for {set i 1} {$i <= $globcol} {incr i 1} {
      set imgarr(0,$i) [gets $f]
    }
    for { set i 1 } { $i <= $globcol } {incr i 1 } {
      for { set j 1 } { $j <= $globrow } {incr j 1 } {
        set imgarr($j,$i) [gets $f]
      }
    }
  }
  close $f 
}

####
# permuter les images
####
proc permuter_images {} {
  global imgperso difficulte globcol globrow nbcol nbrow imgarr
  # permuter les lignes
  for {set i 1} {$i <= $globrow} {incr i 1} {
    set k [expr int(rand()*$globrow)+1]
    for { set j 0 } {$j <= $globcol } { incr j } {
      set imgtmp $imgarr($i,$j)
      set imgarr($i,$j) $imgarr($k,$j)
      set imgarr($k,$j) $imgtmp
    } 
  }
  # permuter les colonnes
  for {set i 1} {$i <= $globcol} {incr i 1} {
    set k [expr int(rand()*$globcol)+1]
    for { set j 0 } {$j <= $globrow } { incr j } {
      set imgtmp $imgarr($j,$i)
      set imgarr($j,$i) $imgarr($j,$k)
      set imgarr($j,$k) $imgtmp
    } 
  }
  
    #instancier nbcol et nbrow
  if       { $difficulte == "0" } { 
    set nbrow 3 ; set nbcol 1 
  } elseif { $difficulte == "1" } { 
   set nbrow 1 ; set nbcol 4 
  } elseif { $difficulte == "2" } {
   set nbrow 2 ; set nbcol 2 
  } elseif { $difficulte == "3" } {
   set nbrow 3 ; set nbcol 3 
  } elseif { $difficulte == "4" } {
   set nbrow 3 ; set nbcol 4 
  }

  if {$nbrow> $globrow} {
    set nbrow $globrow
  }
  if {$nbcol> $globcol} {
    set nbcol $globcol
  }
}

# ##
# sauver trace-�l�ve et parcours
# ##
proc sauver_trace_parcours {} {

  global glob didacticiel difficulte nbessais nbquestions nbjustes heure_debut

  ## trace
  # utilisateur/classe/date/dur�e/didacticiel/niveau/difficulte/version/nbjustes/nbquestions
  set eleve [lindex [split $glob(trace_user) /] end]
    set heure_fin [clock seconds]
    set duree [expr $heure_fin-$heure_debut]
    set date_heure [clock format [clock seconds] -format "%A %d %B %Y %H:%M"]
    set trace_eleve "\{$eleve\} classe \{$date_heure\} $duree \{$didacticiel 1 $difficulte\} $glob(version)"

  lappend trace_eleve "$nbjustes $nbquestions"

  set f [open [file join $glob(trace_user).log] "a+"]
  puts $f $trace_eleve
  close $f

  ## parcours
  # bug Tcl/Tk ? Ne peut utiliser w+. Ouvrir r puis w et renommer
  #lire les parcours en lecture
  set lt_array($didacticiel) "-1 -1 -1 -1 -1" ;#sentinelle
  set f [open [file join $glob(trace_user).level] "r"]
  while { ! [eof $f] } {
    set line [gets $f]
    set lt_array([lindex $line 0]) [lrange $line 1 end]
  }
  close $f

  set parcours [lindex $lt_array($didacticiel) 1]

  #parcours suffisant ? Tol�rance : 0 ou 1 erreur
  if { $parcours < $difficulte && $nbjustes >= 5 } {
    set lt_array($didacticiel) [lreplace $lt_array($didacticiel) 1 1 $difficulte]
    #sauver dans fichier temporaire
    set f [open [file join $glob(trace_user).tmp] "w"]
    foreach local_didacticiel [array names lt_array] {
      puts $f "$local_didacticiel $lt_array($local_didacticiel)"
    }
    close $f
    #renommer
    file rename -force $glob(trace_user).tmp $glob(trace_user).level 
  }
}

proc commence {c} {
  global glob didacticiel difficulte nbquestions nbjustes iquestion nbessai heure_debut
  set iquestion 0
  set nbjustes 0

  wm title . "$didacticiel $difficulte"
  for {set i 1} {$i <= $nbquestions} {incr i 1} {
    .bframe.lab$i configure -image pneutre -width 80 
  }

  permuter_images
    set heure_debut [clock seconds]

  main $c
}

##################################################################################
#proc�dure principale, charg�e de reboucler

proc main {c} {
# nbcol : nombre de colonnes du tableau
# nbrow : nombre de lignes 
# taillerect : taille d'une cellule du tableau
# on peut modifier le nb de lignes ou de colonnes du tableau, en tenant compte de la taille de la fen�tre
# en jouant sur la taille des cellules, en redimensionnant les images, en cr�ant d'autres images
# si l'on cr�e d'autres lignes ou colonnes
# indice1 et indice2 : num�ro des habits tir�s au hasard
# nbessai : num�ro essai ; 1 ou 2 essais pour r�pondre
# on r�cup�re les params du fichier tableau.jeu et on le m�morise dans les variables imgperso,imgarr
# respectivement le personnage, les images de la ligne, la colonne d'ent�te
# puis les images de la grille

global glob didacticiel nbcol nbrow taillerect indice1 indice2 nbessai iquestion \
	imgarr imgperso

set nbessai 0
incr iquestion

#.bframe.quitter_minus configure -state disabled
.cframe.control configure -text [mc move_it]
grid .cframe.control

# on efface la grille
$c delete all

####################Cr�ation du tableau###################################
# On utilise des rectangles pour dessiner les cases, on aurait pu utiliser des lignes
# De m�me, on aurait pu utiliser le 'grid manager' pour g�rer le tout, mais c'aurait �t� peut �tre
# un peu plus compliqu�. L'avantage des rectangles, c'est qu'ils peuvent constituer des objets
# ind�pendants, utile si on veut leur associer des comportements

for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
  for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
    $c create rect [expr $glob(org) + $i*$taillerect] [expr $glob(org) + $j*$taillerect ]  [expr $glob(org) + ($i+1)*$taillerect]  [expr $glob(org) + ($j+1)*$taillerect]  -width 2 -fill gray -tags rect$i$j
  }
}

##################### placement des images de la ligne 0 et de la colonne 0########

for {set i 1} {$i <= $nbcol} {incr i 1} {
  $c create image [expr $glob(org) + $i*$taillerect + int($taillerect/2)] [expr $glob(org) + int($taillerect/2)] \
	-image [image create photo -file [file join $didacticiel $imgarr($i,0)]]
}

for {set j 1} {$j <= $nbrow} {incr j 1} {
  $c create image [expr $glob(org) + int($taillerect/2) ] [expr $glob(org) + $j*$taillerect + int($taillerect/2)] \
	-image [image create photo -file [file join $didacticiel $imgarr(0,$j)]]
}

# fond clignotant dans la case (0,0)
  $c create image [expr $glob(org) + int($taillerect/2)] [expr $glob(org) + int($taillerect/2)] \
	-image [image create photo -file [file join sysdata cellule00.gif]]

####################Cr�ation du personnage#####################
# Initialisation de 2 variables al�atoires pour le choix de la tenue vestimentaire

set indice1 [expr int(rand()*$nbcol) + 1]
set indice2 [expr int(rand()*$nbrow) + 1]

# A chacune des images, on associe le tag 'drag' permettant de la reconna�tre plus tard
# pour la d�placer.

image create photo forme
$c create image 60 60 -image forme -tag drag
forme read [file join $didacticiel $imgarr($indice1,$indice2)]

#####################################Gestion des �v�nements############################
# On associe la gestion des d�placements avec l'item portant le tag 'drag'
# %x %y r�cup�rent la position de la souris.  

$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
$c bind drag <1> "itemStartDrag $c %x %y"
$c bind drag <B1-Motion> "itemDrag $c %x %y"
bind . <Destroy> "exit"

} ;# fin main

# proc�dure appel�e depuis 'Itemstopdrag' pour mettre en surbrillance les cellules
# � l'issue de la correction
     
proc highlight { c indice1 indice2} {
    for {set i 0} {$i < [expr $indice1]} {incr i 1} {
        $c itemconfigure rect$i$indice2 -fill pink
    }
    for {set i 0} {$i < [expr $indice2]} {incr i 1} {
        $c itemconfigure rect$indice1$i -fill pink
    }
    $c itemconfigure rect$indice1$indice2 -fill purple
}

# ############################Lorsque l'on clique sur le personnage (bouton appuy�)
# On capture la position de la souris dans les variables lastX et lastY
# de m�me que les coordonn�es du personnage 'sourcecoord'

proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    . config -cursor fleur
}

# #########################On rel�che la souris.
# analyse et traitement de la position du personnage

proc itemStopDrag {c x y} {
  global glob didacticiel taillerect nbrow nbcol sourcecoord indice1 indice2 nbessai iquestion nbquestions nbjustes
  incr nbessai

  . config -cursor left_ptr

  set coord [$c coords current]
  # On calcule le num�ro de la case o� se trouve le personnage (xc et yc)
  set xc [expr int([lindex $coord 0]/$taillerect)]
  set yc [expr int([lindex $coord 1]/$taillerect)]
  # On teste si le personnage est dans une case valide

  if {$xc>0 && $xc<=$nbcol && $yc>0 && $yc<=$nbrow} {
    # Si oui, on d�place le personnage             
    set i [$c find withtag drag] 
    $c coords $i [expr $xc*$taillerect + int($taillerect/2)] [expr $yc*$taillerect + int($taillerect/2)]
    
    if {$xc==$indice1 && $yc==$indice2} {
      set str [mc win]
      incr nbjustes
      $c create image [expr $xc*$taillerect + int($taillerect/2)] \
			[expr $yc*$taillerect + int($taillerect/2) ] \
			-image [image create photo -file [file join sysdata bien.gif]]
      if { $nbessai <2 } {
        .bframe.lab$iquestion configure -image pbien -width 80
      } else {
        .bframe.lab$iquestion configure -image ppass -width 80
      }
      if {$iquestion >= $nbquestions} {
        #################################################
        ####ajouter ici le code de sortie
        .bframe.but_gauche configure -state disable
        .bframe.quitter_minus configure -state disable
        .bframe.but_droite configure -state disable
        sauver_trace_parcours
        .cframe.control configure -text "Score $nbjustes/$nbquestions. [mc theend]"
        grid .cframe.control -padx 64 
        $c create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
          -image [image create photo -file [file join sysdata sourire.png]]
        update
        after [expr $glob(attente)*1000]
        .bframe.but_gauche configure -state normal
        .bframe.quitter_minus configure -state normal
        .bframe.but_droite configure -state normal
        avance; commence $c
	return
        ############################################"
      }
      $c itemconf [$c find withtag drag] -tag dead
      #.bframe.quitter_minus configure -state normal
      .cframe.control configure -text $str
      update
      after 2000
      main $c
    } else {
      # gestion des erreurs
      switch $nbessai {
        1 { set str [mc redo]
            $c create image [expr $xc*$taillerect + int($taillerect/2)] \
			[expr $yc*$taillerect + int($taillerect/2) ] \
			-image [image create photo -file [file join sysdata mal.gif]]
            set i [$c find withtag drag]
            $c coords $i [lindex $sourcecoord 0] [lindex $sourcecoord 1]
            
           .cframe.control configure -text $str
          }
        2 {
            # On positionne correctement le personnage
            set i [$c find withtag drag]
              $c coords $i [expr $xc*$taillerect + int($taillerect/2)] [expr $yc*$taillerect + int($taillerect/2)]
            
            # on met la figure mal dans la mauvaise case qui a �t� choisie
            $c create image [expr $xc*$taillerect + int($taillerect/2)] \
			[expr $yc*$taillerect + int($taillerect/2) ] \
			-image [image create photo -file [file join sysdata mal.gif]]
            # On met en surbrillance les cellules
            highlight $c $indice1 $indice2
            update
            # on d�place le personnage � la bonne cellule
            set str [mc lose]
            set i [$c find withtag drag] 
            $c coords $i [expr $indice1*$taillerect + int($taillerect/2)] [expr $indice2*$taillerect + int($taillerect/2)]
            
            .bframe.lab$iquestion configure -image pmal -width 80
            $c itemconf [$c find withtag drag] -tag dead

            if {$iquestion >= $nbquestions} {
              #################################################
              ####ajouter ici le code de sortie
              .bframe.but_gauche configure -state disable
              .bframe.quitter_minus configure -state disable
              .bframe.but_droite configure -state disable
              sauver_trace_parcours
              .cframe.control configure -text "Score $nbjustes/$nbquestions. [mc theend]"
              grid .cframe.control -padx 64 
              $c create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
                -image [image create photo -file [file join sysdata pleurer.png]]
              update
              after [expr $glob(attente)*1000]
              .bframe.but_gauche configure -state normal
              .bframe.quitter_minus configure -state normal
              .bframe.but_droite configure -state normal
              avance; commence $c
	      return
              ############################################"
            }

            .cframe.control configure -text $str
            update
            after 2000
            main $c
          }
      }
    }
    return
  } else {
    # Si non, le personnage n'est pas dans une case valide, on le renvoie � sa place                 
    set i [$c find withtag drag]
    $c coords $i [lindex $sourcecoord 0] [lindex $sourcecoord 1]
    
  }
} ;# fin itemStopDrag

# ##################D�placement du personnage, on g�re les coordonn�es, toujours avec lastX et lastY
proc itemDrag {c x y} {
  global lastX lastY
  set x [$c canvasx $x]
  set y [$c canvasy $y]
  # On deplace tous les objets poss�dant le tag 'drag'
  $c move drag [expr $x-$lastX] [expr $y-$lastY]
  set lastX $x
  set lastY $y
}

##################################################################"
  # Relire le nom r�gl� de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    catch {set f [open [file join $glob(home_tableaux) reglages trace_user] "r"]}
    gets $f glob(trace_user)
    close $f
  }
lire_tableau
commence $c
