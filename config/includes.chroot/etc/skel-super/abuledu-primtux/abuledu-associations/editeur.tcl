############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editeur.tcl,v 1.7 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

source fonts.tcl
source path.tcl
source msg.tcl

inithome

global categorie listdata ind ext total currentlist1 currentimg repbasecat sysFont basedir progaide baseHome


# Construction des widgets de l'interface, g�n�r� automatiquement par tkbuilder
. configure  -height 480 -width 640
frame .frame1 -background #ffff80 -height 480 -width 220
pack .frame1 -side left
listbox .frame1.list1 -background #c0c0c0 -height 10 -width 20 -yscrollcommand ".frame1.scroll1 set"
scrollbar .frame1.scroll1 -command ".frame1.list1 yview"
place .frame1.list1 -x 50 -y 60
place .frame1.scroll1 -x 177 -y 60 -height 162
#height 172
label .frame1.menu3 -text [mc {Double clic pour choisir une categorie}] -background #ffff80 -font $sysFont(s)
place .frame1.menu3 -x 30 -y 40
label .frame1.categorie -text [mc {Categories}]
place .frame1.categorie -x 90 -y 10
label .frame1.menu1 -text [mc {Ajouter ou supprimer une categorie :}] -background #ffff80 -font $sysFont(s)
place .frame1.menu1 -x 30 -y 240
button .frame1.ajouter -text [mc {Ajouter}] -font $sysFont(s)
place .frame1.ajouter -x 130 -y 290
button .frame1.supprimer -text [mc {Supprimer}] -font $sysFont(s)
place .frame1.supprimer -x 50 -y 290
entry .frame1.text1
place .frame1.text1 -x 50 -y 260
button .frame1.discri1 -text [mc {Exercice de discrimination 1}] -command "opendiscri 1" -font $sysFont(s)
place .frame1.discri1 -x 40 -y 360
button .frame1.discri2 -text [mc {Exercice de discrimination 2}] -command "opendiscri 2" -font $sysFont(s)
place .frame1.discri2 -x 40 -y 400
button .frame1.param -text [mc {Parametres pour la categorie}] -command "openparam" -font $sysFont(s)
place .frame1.param -x 40 -y 440


frame .frame2 -background #ffff80 -height 480 -width 430
pack .frame2 -side left
#grid .frame2 -column 1 -row 0
label .frame2.menu4 -text [mc {Double clic pour choisir une image}] -background #ffff80 -font $sysFont(s)
place .frame2.menu4 -x 30 -y 40
label .frame2.image -background #ff80c0
place .frame2.image -height 130 -width 130 -x 50 -y 60
entry .frame2.text2
place .frame2.text2 -width 130 -x 50 -y 200
button .frame2.prec -text << -font $sysFont(s)
place .frame2.prec -x 50 -y 230
label .frame2.index -text "1 / 1" -background #ffff80 -justify center -font $sysFont(s)
place .frame2.index -x 100 -y 230

button .frame2.suiv -text >> -font $sysFont(s)
place .frame2.suiv -x 140 -y 230



label .frame2.association -text [mc {Associations}]
place .frame2.association -x 90 -y 10
button .frame2.ajouter2 -text [mc {Ajouter}] -font $sysFont(s)
place .frame2.ajouter2 -x 195 -y 70
button .frame2.supprimer2 -text [mc {Supprimer}] -font $sysFont(s)
place .frame2.supprimer2 -x 185 -y 130
label .frame2.menu5 -text [mc {Mot principal (pour l'exercice lettres melangees)}] -background #ffff80 -font $sysFont(s)
place .frame2.menu5 -x 20 -y 270
entry .frame2.mot
place .frame2.mot -x 50 -y 290
label .frame2.menu6 -text [mc {Decoupage syllabique du mot (separateur : espace)}] -background #ffff80 -font $sysFont(s)
place .frame2.menu6 -x 0 -y 320
entry .frame2.syllabe
place .frame2.syllabe -x 50 -y 340
label .frame2.menu61 -text [mc {Mots de graphies voisines(separateur : espace)}] -background #ffff80 -font $sysFont(s)
place .frame2.menu61 -x 00 -y 360
entry .frame2.graphie
place .frame2.graphie -x 50 -y 380

	if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f lang
  	close $f
	set fich "_edit.htm"
	set fich $lang$fich
	set fichier [file join [pwd] aide $fich]
	} else {
	set fichier [file join [pwd] aide fr_edit.htm]
	}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_edit.htm]
}

button .frame2.aide -text [mc {Aide}] -command "exec $progaide file:$fichier &"
place .frame2.aide -x 90 -y 420
soundcap
################################################################"


# nom du fichier de cat�gorie,de l'image en cours,de l'�tiquette en cours 
set ind 0
set total 0
set categorie ""
set currentlist1 ""
set ext .cat
set currentimg ""
set repbasecat ""


set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set repbasecat [gets $f]
close $f


#ouverture de la fen�tre pour la gestion des images
proc openimg {} {

    global currentimg Home sysFont categorie

if {$categorie == ""} {
tk_messageBox -message [mc {Choisissez d'abord une categorie.}] -type ok -icon info 
return
}

    toplevel .w1
    
    wm title .w1 [mc {Choisir une image}] 
    frame .w1.frame -background #ffff80 -height 480 -width 420
    pack .w1.frame
    listbox .w1.frame.list2 -yscrollcommand ".w1.frame.scroll2 set"
    scrollbar .w1.frame.scroll2 -command ".w1.frame.list2 yview"
    place .w1.frame.list2 -height 130 -width 130 -x 60 -y 50
    place .w1.frame.scroll2 -x 180 -y 50 -height 130
    label .w1.frame.img -background #ff80c0
    place .w1.frame.img -height 130 -width 130 -x 240 -y 50
    button .w1.frame.ok -text [mc {OK}] -command "setimage .w1"
    place .w1.frame.ok -x 140 -y 400
    button .w1.frame.annul -text [mc {Annuler}] -command {destroy .w1}
    place .w1.frame.annul -x 200 -y 400
    listbox .w1.frame.list3 -yscrollcommand ".w1.frame.scroll3 set"
    scrollbar .w1.frame.scroll3 -command ".w1.frame.list3 yview"
    place .w1.frame.list3 -height 130 -width 130 -x 240 -y 240
    place .w1.frame.scroll3 -x 360 -y 240 -height 130
    label .w1.frame.labl3 -text [mc {Appartenant a la categorie}] -background #ffff80 -font $sysFont(s)
    place .w1.frame.labl3 -x 240 -y 210
    label .w1.frame.labl4 -text [mc {Dont le nom contient les lettres}] -background #ffff80 -font $sysFont(s)
    place .w1.frame.labl4 -x 40 -y 200
    label .w1.frame.labl5 -text [mc {(Entree pour valider.}] -background #ffff80 -font $sysFont(s)
    place .w1.frame.labl5 -x 70 -y 240
    label .w1.frame.labl6 -text [mc {Laissez le champ vide}] -background #ffff80 -font $sysFont(s)
    place .w1.frame.labl6 -x 65 -y 260
    label .w1.frame.labl7 -text [mc {pour afficher toutes les images)}] -background #ffff80 -font $sysFont(s)
    place .w1.frame.labl7 -x 55 -y 280

    entry .w1.frame.recherche
    place .w1.frame.recherche -x 60 -y 220

    button .w1.frame.import -text [mc {Importer une image}] -command "importimg .w1 "
    place .w1.frame.import -x 70 -y 330


    peuplelist2
    peuplelist3
    catch {
    image create photo kimage2 -file [file join $Home images [.w1.frame.list2 get 0]]
    .w1.frame.img configure -image kimage2
    } 

    bind .w1 <Control-q> {destroy .w1}
    bind .w1.frame.list2 <ButtonRelease-1> "changeimage %x %y"
    bind .w1.frame.list3 <ButtonRelease-1> "changelist2 %x %y"
    bind .w1.frame.recherche <Return> "changelist3"

}
#######################################################
proc changelist3 {} {
global Home
set ext .gif
set ext2 .ppm
set ext3 .jpg
set ext4 .png
set jok *
set currenttext [.w1.frame.recherche get]
	if {$currenttext !=""} {
	.w1.frame.list2 delete 0 end
		catch {
		image create photo kimage2 -file [file join $Home images blank.gif]
		.w1.frame.img configure -image kimage2
			foreach i [lsort [glob [file join $Home images $jok$currenttext$jok$ext] [file join $Home images $jok$currenttext$jok$ext2] [file join $Home images $jok$currenttext$jok$ext3] [file join $Home images $jok$currenttext$jok$ext4]]] {
			.w1.frame.list2 insert end [file tail $i]
			}
		}
	} else {
	peuplelist2
	}
}

proc changeimage {x y} {
	global Home
    #global currentimg
    set curimg [.w1.frame.list2 get @$x,$y]
    catch {
    image create photo kimage2 -file [file join $Home images $curimg]
    .w1.frame.img configure -image kimage2
    } 
}

proc changelist2 {x y} {
    global repbasecat Home
    set ext ".cat"
    catch {
    image create photo kimage2 -file [file join $Home images blank.gif]
    .w1.frame.img configure -image kimage2
    }
    set curcat [.w1.frame.list3 get @$x,$y]
       if {$curcat==[mc {tous}]} {
       peuplelist2
       } else {
       set f [open [file join $Home categorie $repbasecat $curcat$ext] "r"]
       set listimg [gets $f]
       close $f
       .w1.frame.list2 delete 0 end
           for {set i 0} {$i < [llength $listimg]} {incr i 1} {
           .w1.frame.list2 insert end [lindex [lindex $listimg $i] 0]
           }
       }
}


proc peuplelist1 {} {
    global ext repbasecat Home
    .frame1.list1 delete 0 end
     catch {
       foreach i [lsort [glob [file join $Home categorie $repbasecat *$ext]]] {
       .frame1.list1 insert end [file tail [string range $i 0 [expr [string length $i] - 5]]]
       }
    }
}

proc peuplelist2 {} {
global Home
   .w1.frame.list2 delete 0 end
   set ext .gif
set ext2 .ppm
set ext3 .jpg
set ext4 .png
      catch {
		foreach i [lsort [glob [file join $Home images *$ext] [file join $Home images *$ext2] [file join $Home images *$ext3] [file join $Home images *$ext4]]] {
     		.w1.frame.list2 insert end [file tail $i]
     		}
   	}
}

proc peuplelist3 {} {
    global repbasecat Home
    .w1.frame.list3 delete 0 end
    .w1.frame.list3 insert end [mc {tous}]
     catch {foreach i [lsort [glob [file join $Home categorie $repbasecat *cat]]] {
       .w1.frame.list3 insert end [file tail [string range $i 0 [expr [string length $i] - 5]]]
       }
    }
}
 

peuplelist1

bind .frame2.image <Double-ButtonRelease-1> "openimg"
bind .frame1.ajouter <ButtonRelease-1> "ajoutecategorie"
bind .frame2.ajouter2 <ButtonRelease-1> "ajouteitem"
bind .frame1.supprimer <ButtonRelease-1> "supprimecategorie"
bind .frame1.list1 <Double-ButtonRelease-1> "capturelist1"
bind .frame2.prec <ButtonRelease-1> "precedent"
bind .frame2.suiv <ButtonRelease-1> "suivant"
bind .frame2.supprimer2 <ButtonRelease-1> "supprimeitem"
bind . <Destroy> "enregistre"


proc supprimeitem {} {
global ind total listdata currentimg categorie Home
if {$categorie == ""} {
tk_messageBox -message [mc {Choisissez d'abord une categorie.}] -type ok -icon info 
return
}
enregistre
if {$total > 1} {
  set listdata [lreplace $listdata [expr $ind] [expr $ind]]
  set total [expr $total - 1]
     if {$ind > 0} {
     set ind [expr $ind - 1]
     } else {
     set ind 0
     }
    catch {
    image create photo kimage1 -file [file join $Home images [lindex [lindex $listdata $ind] 0]]
    }
  .frame2.text2 delete 0 end
  .frame2.text2 insert 0 [lindex [lindex $listdata $ind] 1]
  .frame2.mot delete 0 end
  .frame2.mot insert 0 [lindex [lindex $listdata $ind] 2]
  .frame2.syllabe delete 0 end
  .frame2.syllabe insert 0 [lindex [lindex $listdata $ind] 3]
  .frame2.graphie delete 0 end
  .frame2.graphie insert 0 [lindex [lindex $listdata $ind] 4]
  .frame2.image configure -image kimage1
  set currentimg [lindex [lindex $listdata $ind] 0]
.frame2.index configure -text "[expr $ind+1] / $total"
  enregistre
} else {
  bell
}
}

proc ajouteitem {} {
global ind total listdata currentimg categorie Home
if {$categorie == ""} {
tk_messageBox -message [mc {Choisissez d'abord une categorie.}] -type ok -icon info 
return
}
enregistre
incr total 
set ind [expr $total - 1]
set currentimg blank.gif
lappend listdata {blank.gif vide}
  catch {
  image create photo kimage1 -file [file join $Home images $currentimg]
  .frame2.image configure -image kimage1
  }
.frame2.text2 delete 0 end
.frame2.text2 insert 0 vide
.frame2.mot delete 0 end
.frame2.syllabe delete 0 end
.frame2.graphie delete 0 end
.frame2.index configure -text "[expr $ind+1] / $total"

enregistre
}

proc getdata {} {
global ind listdata currentimg
set space " "
set currenttext [.frame2.text2 get]
set currentmot [.frame2.mot get]
set currentsyllabe [.frame2.syllabe get]
set currentgraphie [.frame2.graphie get]
set liste \173$currentimg\175\040\173$currenttext\175\040\173$currentmot\175\040\173$currentsyllabe\175\040\173$currentgraphie\175
set listdata [lreplace $listdata [expr $ind] [expr $ind] $liste]
}

proc setimage {wind} {
global ind listdata currentimg Home
set currentimg [$wind.frame.list2 get active]
catch {
image create photo kimage1 -file [file join $Home images $currentimg]
.frame2.image configure -image kimage1
}
destroy $wind
}


proc precedent {} {
global ind categorie listdata currentimg Home total
if {$categorie!=""} {
    enregistre
    if {$ind > 0} {
    set ind [expr $ind - 1]
    catch {
    image create photo kimage1 -file [file join $Home images [lindex [lindex $listdata $ind] 0]]
    }
    .frame2.text2 delete 0 end
    .frame2.text2 insert 0 [lindex [lindex $listdata $ind] 1]
    .frame2.mot delete 0 end
    .frame2.mot insert 0 [lindex [lindex $listdata $ind] 2]
    .frame2.syllabe delete 0 end
    .frame2.syllabe insert 0 [lindex [lindex $listdata $ind] 3]
    .frame2.graphie delete 0 end
    .frame2.graphie insert 0 [lindex [lindex $listdata $ind] 4]
    .frame2.image configure -image kimage1
    set currentimg [lindex [lindex $listdata $ind] 0]
.frame2.index configure -text "[expr $ind+1] / $total"

    } else {
    bell
    }
}

}

proc suivant {} {
global ind categorie total listdata currentimg Home
if {$categorie!=""} {
    enregistre
    if {$ind < $total - 1} {
    incr ind
      catch {
      image create photo kimage1 -file [file join $Home images [lindex [lindex $listdata $ind] 0]]
      }
    .frame2.text2 delete 0 end
    .frame2.text2 insert 0 [lindex [lindex $listdata $ind] 1]
    .frame2.mot delete 0 end
    .frame2.mot insert 0 [lindex [lindex $listdata $ind] 2]
    .frame2.syllabe delete 0 end
    .frame2.syllabe insert 0 [lindex [lindex $listdata $ind] 3]
    .frame2.graphie delete 0 end
    .frame2.graphie insert 0 [lindex [lindex $listdata $ind] 4]
    .frame2.image configure -image kimage1
    set currentimg [lindex [lindex $listdata $ind] 0]
.frame2.index configure -text "[expr $ind+1] / $total"

    } else {
    bell
    }
}

}

proc enregistre {} {
global listdata categorie ext repbasecat Home

    if {$categorie != ""} {
    getdata
    set f [open [file join $Home categorie $repbasecat $categorie$ext] "w"]
    puts $f $listdata
    close $f
   }
}


proc capturelist1 {} {
global currentlist1
enregistre
set currentlist1 [.frame1.list1 get active]
ouvrecategorie
}

proc ouvrecategorie {} {
global categorie listdata total ind currentlist1 currentimg ext repbasecat Home
set ind 0
set total 0

  if {$currentlist1!=""} {
  set categorie $currentlist1
  .frame2.association configure -text $categorie
    catch {
    set f [open [file join $Home categorie $repbasecat $categorie$ext] "r"]
    set listdata [gets $f]
    close $f
    set total [llength $listdata] 
    } 
      if {$total!=0} {
      set currentimg [lindex [lindex $listdata 0] 0]
        catch {
        image create photo kimage1 -file [file join $Home images [lindex [lindex $listdata 0] 0]]
        }
      .frame2.text2 delete 0 end
      .frame2.text2 insert 0 [lindex [lindex $listdata 0] 1]
      .frame2.mot delete 0 end
      .frame2.mot insert 0 [lindex [lindex $listdata 0] 2]
      .frame2.syllabe delete 0 end
      .frame2.syllabe insert 0 [lindex [lindex $listdata 0] 3]
      .frame2.graphie delete 0 end
      .frame2.graphie insert 0 [lindex [lindex $listdata 0] 4]
      .frame2.image configure -image kimage1
      }
   } else {
   bell
   }
if {$total != 0} {
.frame2.index configure -text "1 / $total"
} else {
.frame2.index configure -text "1 / 1"
}
}

proc supprimecategorie {} {
global currentlist1 categorie ext repbasecat Home
set categ [.frame1.text1 get]
  if {$categ==""} {
tk_messageBox -message [mc {Veuillez indiquer d'abord une categorie dans la zone de saisie.}] -type ok -icon info 
  return
  }
enregistre
  catch {
file delete [file join $Home categorie $repbasecat $categ$ext]
file delete [file join $Home categorie $repbasecat $categ.di1]
file delete [file join $Home categorie $repbasecat $categ.di2]
file delete [file join $Home categorie $repbasecat $categ.dat]
}
peuplelist1
set currentlist1 ""
set categorie ""
.frame2.association configure -text Associations
.frame1.text1 delete 0 end
.frame2.text2 delete 0 end
.frame2.index configure -text "1 / 1"
}

proc ajoutecategorie {} {
global ext listdata categorie currentlist1 repbasecat Home
set categ [.frame1.text1 get]

  if {$categ==""} {
tk_messageBox -message [mc {Veuillez indiquer d'abord une categorie dans la zone de saisie.}] -type ok -icon info 
  return
  }
enregistre
set categ [.frame1.text1 get]
    if {[catch {set f [open [file join $Home categorie $repbasecat $categ$ext] "w"]}]} {
    bell
    return
    } else {
    set listdata {{blank.gif vide}}
    puts $f $listdata
    close $f
    peuplelist1
    set currentlist1 ""
    set categorie ""
   .frame2.association configure -text [mc {Associations}]
   .frame1.text1 delete 0 end
   .frame2.text2 delete 0 end
   }
.frame2.index configure -text "1 / 1"

set f [open [file join $Home categorie $repbasecat $categ.dat] "w" ]
set listacti \173$categ.wav\175\040\1731\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\175\040\1731\0401\0401\0401\0401\0401\0401\0401\175
puts $f $listacti
close $f
}



proc opendiscri {what} {
global sysFont Home repbasecat categorie basedir progaide baseHome
if {$categorie == ""} {
tk_messageBox -message [mc {Choisissez d'abord une categorie.}] -type ok -icon info 
return
}
catch {destroy .opendiscri}
toplevel .opendiscri -background grey -width 250 -height 400
wm geometry .opendiscri +50+50
wm transient .opendiscri .
wm title .opendiscri "[mc {Discrimination}] $what - $categorie"
frame .opendiscri.frame -background grey -width 250 -height 400
pack .opendiscri.frame -side top
label .opendiscri.frame.labobj -font $sysFont(s) -text [mc {Entrer le texte.}] -background grey
grid .opendiscri.frame.labobj -row 0 -column 0 -columnspan 2
text .opendiscri.frame.text -yscrollcommand ".opendiscri.frame.scroll set" -setgrid true -width 60 -height 8 -wrap word -background white -font $sysFont(s)
scrollbar .opendiscri.frame.scroll -command ".opendiscri.frame.text yview"
grid .opendiscri.frame.text -row 1 -column 0 -columnspan 2 -sticky ew
grid .opendiscri.frame.scroll -in .opendiscri.frame -row 1 -column 2 -sticky ns


set ext .di
set file $categorie$ext$what
if {[catch { set f [open [file join $Home categorie $repbasecat $file] "r" ] }] !=1 } {
while {![eof $f]} {
.opendiscri.frame.text insert end [read $f 10000]
}
close $f
}
button .opendiscri.frame.but -text [mc {OK}] -command "setdiscri $file"
grid .opendiscri.frame.but -row 2 -column 0 -sticky ew


	if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f lang
  	close $f
	set fich "_discri.htm"
	set fich $lang$fich
	set fichier [file join [pwd] aide $fich]
	} else {
	set fichier [file join [pwd] aide fr_discri.htm]
	}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_discri.htm]
}

button .opendiscri.frame.aide -text [mc {Aide}] -command "exec $progaide file:$fichier &"
#set fichier [file join $basedir aide discri.htm]
#button .opendiscri.frame.aide -text [mc {Aide}] -command "exec $progaide \042$fichier\042 &"
grid .opendiscri.frame.aide -row 2 -column 1 -sticky ew


}

proc setdiscri {file} {
global sysFont Home repbasecat categorie
set f [open [file join $Home categorie $repbasecat $file] "w" ]
set texte [.opendiscri.frame.text get 1.0 "end - 1 chars"]
set texte [string map {\" \\" \[ "" \] "" \{ "" \} ""} $texte]
puts $f $texte
close $f
catch {
destroy .opendiscri
}
}


proc openparam {} {
global categorie iwish
if {$categorie == ""} {
tk_messageBox -message [mc {Choisissez d'abord une categorie.}] -type ok -icon info 
return
} else {
exec $iwish paramcat.tcl $categorie &
}
}

proc importimg {wind } {
global basedir currentimg baseHome
set types { {{Fichiers jpg png gif bmp } {.jpg .png .gif .bmp}}}
set ext .kdb
set nomf [tk_getOpenFile -filetypes $types -initialdir [file join $basedir images]]
    if {[file exists $nomf]} {
    image create photo imagetmp -file [file join $nomf]
    $wind.frame.img configure -image imagetmp
    $wind.frame.list2 insert end [file tail $nomf]
    file copy -force  [file join $nomf] [file join $baseHome images]
    set currentimg [file tail $nomf]
    $wind.frame.list2 activate end

        }
raise $wind
}









