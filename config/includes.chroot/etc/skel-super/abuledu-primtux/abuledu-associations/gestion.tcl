############################################################################
# Copyright (C) 2006 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : gestion.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 04/04/2006
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 04/04/2006
#
#
#########################################################################
#!/bin/sh
#gestion.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

global plateforme LogHome nomutil iwish

source path.tcl
source fonts.tcl

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
initlog $plateforme $ident

source msg.tcl


. configure -background grey -width 640 -height 480
wm geometry . +0+0

frame .wleft -background grey -width 320 -height 480
pack .wleft -side left
label .wleft.util -text [mc {Utilisateur}] -background grey -font $sysFont(t) -pady 10 -padx 20 
pack .wleft.util -side top -anchor n

listbox .wleft.listsce -yscrollcommand ".wleft.scrollpage set" -width 15 -height 10
scrollbar .wleft.scrollpage -command ".wleft.listsce yview" -width 7
pack .wleft.listsce .wleft.scrollpage -side left -fill y -expand 1 -pady 10
bind .wleft.listsce <ButtonRelease-1> "changelistsce %x %y"




frame .wright -background white -width 320 -height 480
pack .wright -side left -expand 1 -fill both
button .wright.but0 -text [mc {Ajouter un utilisateur}] -command "additem"
pack .wright.but0 -pady 10 -padx 20 -side top
button .wright.but1 -text [mc {Supprimer l'utilisateur}] -command "delitem"
pack .wright.but1 -pady 10 -padx 20 -side top
button .wright.but2 -text [mc {Voir la fiche de l'utilisateur}] -command "voirbilan"
pack .wright.but2 -pady 10 -padx 20 -side top
button .wright.but3 -text [mc {Fermer}] -command "exit"
pack .wright.but3 -pady 10 -padx 20 -side top



proc changelistsce {x y} {
global nomutil
set ind [.wleft.listsce index @$x,$y]
set nomutil [.wleft.listsce get $ind]
}


proc additem {} {
catch {destroy .nomobj}
toplevel .nomobj -background grey -width 250 -height 100
wm geometry .nomobj +50+50
frame .nomobj.frame -background grey -width 250 -height 100
pack .nomobj.frame -side top
label .nomobj.frame.labobj -font {Helvetica 10} -text [mc {Nom de l'utilisateur :}] -background grey
pack .nomobj.frame.labobj -side top 
entry .nomobj.frame.entobj -font {Helvetica 10} -width 10
pack .nomobj.frame.entobj -side top 
button .nomobj.frame.ok -background gray75 -text [mc {Ok}] -command "verifnomutil"
pack .nomobj.frame.ok -side top -pady 10
}

proc verifnomutil {} {
global LogHome
set ext .log
set nom [join [.nomobj.frame.entobj get] "_"]
if {$nom !=""} {
set f [open [file join $LogHome $nom$ext] "w"]
close $f
}
catch {destroy .nomobj}
afficheliste
}

#######################################################################
proc delitem {} {
global LogHome nomutil
set nom [string map {\040 .log} $nomutil]
	if {[.wleft.listsce size] < 2} {
	tk_messageBox -message [mc {Impossible de supprimer le dernier utilisateur}] -type ok
	return
	}
set response [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer l'utilisateur %1$s ?}] $nomutil] -type yesno ]
	if {$response == "yes"} {
	file delete [file join $LogHome $nom]
	afficheliste
	} 

}


proc afficheliste {} {
global LogHome
.wleft.listsce delete 0 end
foreach i [lsort [glob [file join $LogHome *.log]]] {
.wleft.listsce insert end [string map {.log \040} [file tail $i]]
}
}

proc voirbilan {} {
global nomutil iwish
exec $iwish bilan.tcl [string map {\040 .log} $nomutil] &
}


afficheliste
set nomutil [.wleft.listsce get 0]




