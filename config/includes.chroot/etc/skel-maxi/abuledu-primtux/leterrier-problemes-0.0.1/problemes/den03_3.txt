set etapes 1
set niveaux {0 1 3}
::1
set niveau 1
set ope {{1 9} {1 9}}
set interope {{1 10 1} {1 10 1}}
set dejap [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope1 [expr int(rand()*(9-$dejap)) + $dejap]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set operations {{0+0}}
set volatil 0
set enonce "Les perles.\nTim a besoin de [expr $ope1*10 + $ope2] perles.\nIl a d�j� [expr $dejap] domino(s).\nCombien lui manque-t-il de dominos?\nCombien lui manque-t-il de perles?"
set cible [list [list 4 3 [list cartep.gif [expr $dejap]] source0] [list 4 3 [list ] source1]]
set intervalcible 60
set taillerect 70
set orgy 50
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {carte.gif perle.gif}
set orient 0
set labelcible {Dominos Perles}
set quadri 0
set ensembles [list [expr $ope1 - $dejap] [expr $ope2]]
set reponse [list [list {1 3} [list {Il lui manque} [expr $ope1 - $dejap] {domino(s) et} [expr $ope2] perles.]]]
set dessin 0
set canvash 360
set c1height 160
set oprequise {}
::


