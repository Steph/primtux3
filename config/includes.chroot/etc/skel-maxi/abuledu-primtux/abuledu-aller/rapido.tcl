#!/bin/sh
#reconnaitre.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto  categorie startdirect user Homeconf repertoire tabstartdirect initrep Home baseHome temps couleur
source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set couleur bisque4
set nbreu 0
set essais 0
set auto 0
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 0
set filuser [lindex $argv 1]

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right


tux_commence

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both
#ouverture du fichier de configuration pour r�cup�rer des variables
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_rapido [lindex $aller 10]
close $f
set temps [lindex $aller_rapido 0]
#set startdirect [lindex $aller_rapido 4]
#set lecture_mot [lindex $aller_flash 5]
#set lecture_mot_cache [lindex $aller_flash 6]
}

set initrep [file join $Home textes $repertoire]

bind .text <ButtonRelease-1> "lire"
bind . <KeyPress> "cancelkey %A"


#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]
if {$auto == 1} {
set temps $tabaide(rapido)
#set longchamp $tablongchamp(flash)
#set listevariable $tablistevariable(flash)
#set startdirect $tabstartdirect(flash)
#set vitesse $tablecture_mot(flash)
#set lecture_mot_cache $tablecture_mot_cache(flash)
}
focus .text

#.text configure -state disabled
wm title . "[mc {Rapido}] $categorie - [lindex [lindex $listexo 10] 1]"

label .menu.consigne -text "[lindex [lindex $listexo 10] 1] - [mc {Clique sur le mot convenable}]" -justify center
pack .menu.consigne -side left -fill both -expand 1

#######################"""""""""""""""""""""""
proc afficheaide {what} {
global sysFont temps compteur indp listemotscaches idt depassdelai
incr compteur -1
    .menu.consigne configure -text "[string repeat ". " $compteur] $what  [string repeat ". " $compteur]" -font $sysFont(l) -fg black
if {$compteur == 0} {
after cancel $idt
.menu.consigne configure -text "" -font $sysFont(l) -fg red
update
after 200
.menu.consigne configure -text "[string repeat ". " $compteur] $what  [string repeat ". " $compteur]" -font $sysFont(l) -fg red
update
after 200
.menu.consigne configure -text "" -font $sysFont(l) -fg red
update
after 200
.menu.consigne configure -text "[string repeat ". " $compteur] $what  [string repeat ". " $compteur]" -font $sysFont(l) -fg red
update
after 200
.menu.consigne configure -text "" -font $sysFont(l) -fg red
update
set compteur 10
incr depassdelai
set index [expr int(rand()*([llength $listemotscaches] - $indp)) + $indp]
set tmp [lindex $listemotscaches $indp]
set tmp2 [lindex $listemotscaches $index]
set listemotscaches [lreplace $listemotscaches $indp $indp $tmp2]
set listemotscaches [lreplace $listemotscaches $index $index $tmp]
.menu.consigne configure -text "[string repeat ". " $compteur] [lindex $listemotscaches $indp]  [string repeat ". " $compteur]" -font $sysFont(l) -fg black

}
set idt [after [expr $temps*1000] afficheaide [lindex $listemotscaches $indp]]
}

proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches texte auto listexo user startdirect indp compteur depassdelai
set indp 0
set compteur 11
set depassdelai 0
.menu.consigne configure -text [lindex [lindex $listexo 10] 2]

#button .menu.b2 -image [image create photo corriger -file [file join sysdata corriger.gif]] -command "abandon $t"
#pack .menu.b2 -side left

.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"


#bind .menu  <Destroy> "fin"
$t configure -state normal
    if {$auto==0} {
    pauto .text
    } else {
    pmanuel .text
    }
    
bind .text <ButtonRelease-1> ""
set what [mc {Clique sur le mot convenable}]
if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what
#wm geometry .wtux +0+340

set leng [llength $listemotscaches]

#on m�lange
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $listemotscaches $t1]
  set listemotscaches [lreplace $listemotscaches $t1 $t1 [lindex $listemotscaches $t2]]
  set listemotscaches [lreplace $listemotscaches $t2 $t2 $tmp]
  }
$t tag add text 1.0 end
.text tag configure rouge -background red
.text tag configure jaune -background yellow

$t tag bind text <ButtonRelease-1> "hit $t"
afficheaide [lindex $listemotscaches $indp]

catch {destroy .menu.bb1} 
}

proc pauto {t} {
global sysFont listemotscaches  iwish filuser listdata

set plist [parse2 $t]
set tmp ""
    foreach phrase $plist {
    set tmp [concat $tmp $phrase]
    }

regsub -all {[��'\"\�\�\[\]\240]} $tmp " " tmp
regsub -all {[,;.!?:-]} $tmp " " tmp
regsub -all {[\(\)\/%]} $tmp " " tmp
regsub -all {[\040+]} $tmp " " tmp

set ttmp ""
foreach mot $tmp {
if {[string length $mot]>1} {lappend ttmp $mot}
}

for {set i 0} {$i < 10} {incr i} {
     set k [expr int(rand()*[llength $ttmp])]
	if {[lindex $ttmp $k] != ""} {lappend listemotscaches [lindex $ttmp $k]}
	set ttmp [lreplace $ttmp $k $k]
	
}

   if {[llength $listemotscaches] < 2} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }


set listdata $listemotscaches

#if {$lecture_mot == "0" } {
catch {destroy .menu.bb1} 
#}

}

#######################################################################"
proc pmanuel {t} {
global sysFont couleur listdata listemotscaches
set plist ""
set listemotscaches ""
set tmp2 ""
set plist2 [parse2 $t]
    foreach phrase $plist2 {
    set tmp2 [concat $tmp2 $phrase]
    }


set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    #regsub -all \" $str \\\" str
    lappend plist $str        
    }
set tmp ""
    foreach phrase $plist {
    set tmp [concat $tmp $phrase]
    }
set ttmp ""
foreach mot $tmp {
if {[lsearch -exact $tmp2 $mot] != -1} {lappend ttmp $mot}
}
regsub -all {[��'\"\�\�\[\]\240]} $ttmp " " ttmp
regsub -all {[,;.!?:-]} $ttmp " " ttmp
regsub -all {[\(\)\/%]} $ttmp " " ttmp
regsub -all {[\040+]} $ttmp " " ttmp


foreach mot $ttmp {
if {[string length $mot]>1} {lappend listemotscaches $mot}
}

   	if {[llength $listemotscaches] < 4} {
    	pauto $t
    	return
   	}
}

##########################################################"
proc hit {t} {
global sysFont listemotscaches essais  nbreu listexo indp compteur idt
    incr essais

$t configure -state normal
	set cur [$t index insert]
	set cur1 "[$t search -backwards -regexp {[\240 \040 ' \- , . ; : ! ? \"]} $cur "insert linestart"]"
	if {$cur1 == ""} {set cur1 "[$t search -backwards "\n"  $cur 1.0]"}
	if {$cur1 != ""} {set cur1 "$cur1 + 1c"} 
	if {$cur1 == ""} {set cur1 "insert linestart"}
	set cur2 "[$t search -regexp {[\240 \040 ' \- , . ; : ! ? \"]} $cur "insert lineend"]"
	if {$cur2 == ""} {set cur2 "[$t search "\n"  $cur end]"}
	if {$cur2 != ""} {set cur2 "$cur2"} 
	if {$cur2 == ""} {set cur2 "insert lineend"}
	set mot [string map {- \040} [$t get $cur1 $cur2]]

	.text tag remove all 1.0 end
	if {$mot == [lindex $listemotscaches $indp]} {
	.text tag add jaune $cur1 $cur2
	tux_reussi
	incr nbreu
	set compteur 10
	after cancel $idt
	.wtux.speech configure -text "Bien jou�!\n$essais essai(s).\n$nbreu mot(s) trouv�(s) sur [llength $listemotscaches]."
	bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"

	update
	after 1000
	.text tag remove jaune 1.0 end
	update
      if {[testefin $nbreu $essais] == 1} {
	$t configure -state disabled
	return
	}
	incr indp
	afficheaide [lindex $listemotscaches $indp]
	tux_continue
	.wtux.speech configure -text "$essais essai(s).\n$nbreu mot(s) trouv�(s) sur [llength $listemotscaches].\n\n Trouve le mot"
	bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"

	} else {
	.text tag add rouge $cur1 $cur2
	tux_echoue1
	.wtux.speech configure -text "C'est faux!\n$essais essai(s).\n$nbreu mot(s) trouv�(s) sur [llength $listemotscaches]."
	bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"

	update
	after 1000
	.text tag remove rouge 1.0 end
	update
	}
	
$t configure -state disabled

}


proc testefin {nbreu essais} {
global sysFont listemotscaches user categorie listexo depassdelai
    if {$nbreu >= [llength $listemotscaches]} {
    catch {destroy .menu.lab}
    catch {destroy .menu.consigne}

    set str0 [mc {Exercice termine en }]
    set str2 "[format [mc {%1$s essai(s) pour %2$s mot(s).}] $essais $nbreu] $depassdelai d�passement(s) de d�lai."
    set str1 [lindex [lindex $listexo 10] 1]
set score [expr ($nbreu*100 )/([llength $listemotscaches] +($essais- $nbreu)) - $depassdelai*5]
if {$score < 0} {set score 0}
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

    #enregistreval $str1 $categorie $str2 $user
    label .menu.lab -text $str0$str2
    pack .menu.lab
    .text tag remove text 1.0 end
    bell
	return 1
        }
return 0
}


if {$startdirect == 0 } {
main .text
}

proc fin {} {
global sysFont categorie user essais listemotscaches nbreu listexo iwish filuser  startdirect repertoire temps depassdelai
variable repertconf
set score [expr ($nbreu*100 )/([llength $listemotscaches] +($essais- $nbreu)) - $depassdelai*5]
if {$score < 0} {set score 0}
  set str2 "[format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $nbreu [llength $listemotscaches]] $depassdelai d�passement(s) de d�lai."


switch $temps {
1 { set aideconf [mc {D�lai : court}]}
2 { set aideconf [mc {D�lai : moyen}]}
3 { set aideconf [mc {D�lai : long}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf $aideconf"

enregistreval [lindex [lindex $listexo 10] 1] \173$categorie\175 $str2 $score $repertconf 10 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}



