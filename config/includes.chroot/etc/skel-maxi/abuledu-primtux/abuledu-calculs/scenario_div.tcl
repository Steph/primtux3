#!/bin/sh
#editeurp.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean- Louis Sendral
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
#source pluie.conf

###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}
set bgcolor orange
. configure  -height 540 -width 680 -background $bgcolor
##bind . <F1> "showaide {division}"
global sysFont test_elev
source msg.tcl
source fonts.tcl
source path.tcl
set basedir [pwd]

##source repertoire.tcl
source config_sce.tcl
source calculs.conf
set  DOSSIER_EXOS1 [lindex $argv 0 ]
## pour interdire commun aux �l�ves
set test_elev 1
if { $tcl_platform(platform) == "unix" && [lindex [exec  id -G -n ] 0 ]  == "eleves" } {
if { [string match  "${basedir}*"  [lindex $argv 0 ] ] == 1 } {
set test_elev 0
} 
}
#cd $DOSSIER_EXOS
#set ext conf
wm geometry . +0+0
wm title . "[mc {Editeur de Sc�nario}], pour calcul de quotients"
##wm resizable . 0 0
frame .frame1 -background $bgcolor -height 540 -width 360
grid .frame1 -column 0 -row 0
if { $tcl_platform(platform) == "unix" } {
listbox .frame1.list1 -background #c0c0c0 -height 10 -width 20 -yscrollcommand ".frame1.scroll1 set"
scrollbar .frame1.scroll1 -command ".frame1.list1 yview"
place .frame1.list1 -x 60 -y 145
place .frame1.scroll1 -x 200 -y 145 -height 172 } else {
listbox .frame1.list1 -background #c0c0c0 -height 10 -width 20 -yscrollcommand ".frame1.scroll1 set"
scrollbar .frame1.scroll1 -command ".frame1.list1 yview"
place .frame1.list1 -x 60 -y 145
place .frame1.scroll1 -x 188 -y 145 -height 172

}



label .frame1.menu3 -text [mc {Double_clic_scena_div}] -background  $bgcolor 
place .frame1.menu3 -x 45  -y 125
label .frame1.categorie -text [mc {Sc�narios}] -background $bgcolor 
place .frame1.categorie -x 95 -y 10
 label .frame1.fich_scena -text [mc {choix_fich_scena}]   -background $bgcolor
 place    .frame1.fich_scena -x 0 -y 65
 ##label .frame1.fich_fich -text [mc {fich_scenarios_ed}]   -background $bgcolor
  place    .frame1.fich_scena -x 10 -y 85
 ## place    .frame1.fich_fich -x 10 -y 105
 entry  .frame1.nom_fich_scena -width 40
 place .frame1.nom_fich_scena  -x 10 -y 35
radiobutton .frame1.oui_scena -variable choix_ok_fich  -value oui -text [mc {oui}]  -background $bgcolor \
 -command " peuplelist1 " 
place .frame1.oui_scena -x 65 -y 60
radiobutton .frame1.non_scena -variable choix_ok_fich -value non -text [mc {non}] -background $bgcolor \
-command "peuplelist1  "
place .frame1.non_scena -x 155 -y 60

label .frame1.menu1 -text [mc {ajout_supp_scena}] -background $bgcolor
place .frame1.menu1 -x 10 -y 325
button .frame1.ajouter -text [mc {Ajouter}]
place .frame1.ajouter -x 140 -y 375
button .frame1.supprimer -text [mc {Supprimer} ]
place .frame1.supprimer -x 60 -y 375
entry .frame1.text1 -width 40
place .frame1.text1 -x 10 -y 345

set message "[mc {dabord}]:\n1)[mc {choix_repert_conf}] \n2) [mc {tap_nom_sce}]\n3) [mc {clic_ajsup}]\n4) [mc {double_clic_nom}]. "
message  .frame1.consigne -text $message -foreground red    -aspect 400
place .frame1.consigne -x 5 -y 405

frame .frame2 -background $bgcolor -height 540 -width 360
grid .frame2 -column 1 -row 0
set message2 "[mc  {Puis}]\n5) [mc {remp_champ_dr}]\n6) [mc {clic_sau_maj}]"
message  .frame2.consigne2 -text $message2 -foreground red    -aspect   250
place  .frame2.consigne2 -x 65 -y 435

label .frame2.scenario -text "[mc {parametres}] [.frame1.list1 get active] :"  -background $bgcolor 
        place .frame2.scenario -x 50 -y 10
	  button .frame2.aide -text [mc {Aide}]
	  place .frame2.aide -x 270 -y 400
	  bind .frame2.aide <1> "exec $progaide file:${basedir}/aide1.html#division &"
         button .frame2.ajoutsc -text [mc {sauver_maj_scena}]
        place .frame2.ajoutsc -x 30 -y 400
            checkbutton .frame2.sel1 -text [mc {repert_acc}] -variable sel1 -relief flat  -background  $bgcolor
            place .frame2.sel1 -x 20 -y 35
            .frame2.sel1 deselect
		checkbutton .frame2.sel2 -text [mc {quot_auto}] -variable sel2 -relief flat  -background $bgcolor
            place .frame2.sel2 -x 20 -y 60
            .frame2.sel2 deselect
		checkbutton .frame2.sel3 -text [mc {diff_auto}] -variable sel3 -relief flat  -background $bgcolor
            place .frame2.sel3 -x 20 -y 85
            .frame2.sel3 deselect
		checkbutton .frame2.sel4 -text [mc {quot_mult}] -variable sel4 -relief flat  -background $bgcolor
            place .frame2.sel4 -x 20 -y 110
            .frame2.sel4 deselect
		checkbutton .frame2.sel5 -text [mc {recup_reper}] -variable sel5 -relief flat  -background $bgcolor
            place .frame2.sel5 -x 20 -y 135
            .frame2.sel5 deselect
#		checkbutton .frame2.sel6 -text [mc {fix_has}] -variable sel6 -relief flat  -background $bgcolor
# -command desactive_h
#           place .frame2.sel6 -x 20 -y 160
#           .frame2.sel6 deselect
		label .frame2.choix -text [mc {divid_divis}] -bg $bgcolor 
		place .frame2.choix -x 25 -y 170
		radiobutton .frame2.f -variable choix_nombre -value f -text [mc {fix_ici}] -background $bgcolor -command "desactive f "
		place .frame2.f -x 15 -y 195
		radiobutton .frame2.h -variable choix_nombre -value h -text [mc {fix_has_ici}] -background $bgcolor -command "desactive h "
		place .frame2.h -x 95 -y 195
		radiobutton .frame2.af -variable choix_nombre -value af -text [mc {a_fixer}] -background $bgcolor -command "desactive af "
		place .frame2.af -x 195 -y 195
		.frame2.af select

		label .frame2.hasard -text [mc {hasar_control}] -background  $bgcolor 
		place .frame2.hasard -x 30 -y 230

		entry .frame2.sel7 -width 2 ; place .frame2.sel7 -x 0 -y 260
		label .frame2.has1 -text [mc {min_max_divid}] -background $bgcolor
		place .frame2.has1 -x 20  -y 260
         	entry .frame2.sel71 -width 2 ; place .frame2.sel71 -x 310 -y 260


	     entry .frame2.sel8 -width 2  ; place .frame2.sel8 -x 0 -y 280
		label .frame2.has2 -text [mc {min_max_quot}] -background $bgcolor
            place .frame2.has2 -x 20 -y 280
	     entry .frame2.sel81 -width 2  ; place .frame2.sel81 -x 310 -y 280

       label .frame2.fixe -text [mc {pas_hasar}] -background $bgcolor 
	 place .frame2.fixe -x 30 -y 320
       label .frame2.text_divid -text [mc {sera_dividende}] -background $bgcolor ; place .frame2.text_divid -x 65 -y 350
     	 entry .frame2.sel9 -width 5 ; place .frame2.sel9 -x 20 -y 350
       entry .frame2.sel10 -width 5 ; place .frame2.sel10 -x 20 -y 370
       label .frame2.text_divis -text [mc {sera_diviseur}]  -background $bgcolor ; place .frame2.text_divis -x 65 -y 370

#label .frame2.a_fixer -text [mc {A fixer plus tard :}]
#	 place .frame2.a_fixer -x 150 -y 290
#       checkbutton .frame2.sel11 -text [mc { plus tard}] -variable sel11 -relief flat  -activebackground grey
#	place .frame2.sel11 -x 150  -y 320
#bind .frame2.sel6 <Button-1> "desactive_h "
proc desactive { choix } {
global sel6 sel7 sel8 sel9 sel10
if { $choix == "f"  }  {
#.frame2.sel6 select
.frame2.sel9 configure -state normal ; .frame2.sel10 configure -state normal
.frame2.sel7 delete 0 end ; .frame2.sel8 delete 0 end
.frame2.sel7 configure -state disabled ; .frame2.sel8 configure -state disabled
.frame2.sel71 delete 0 end ; .frame2.sel81 delete 0 end
.frame2.sel71 configure -state disabled ; .frame2.sel81 configure -state disabled
focus .frame2.sel9
return
}
if { $choix == "h"  }  {
#.frame2.sel6 deselect
.frame2.sel7 configure -state normal ; .frame2.sel8 configure -state normal
.frame2.sel71 configure -state normal ; .frame2.sel81 configure -state normal

.frame2.sel9 delete 0 end ; .frame2.sel10 delete 0 end
.frame2.sel9 configure -state disabled ; .frame2.sel10 configure -state disabled
focus .frame2.sel7
return
}
if { $choix == "af"  }  {
.frame2.sel7 configure -state disabled ; .frame2.sel8 configure -state disabled
.frame2.sel71 configure -state disabled ; .frame2.sel81 configure -state disabled

.frame2.sel9 configure -state disabled ; .frame2.sel10 configure -state disabled

return
}
}
bind .frame2.ajoutsc <ButtonRelease-1> "ajoutescenario"
bind .frame1.ajouter <ButtonRelease-1> "nouveauscenario"
bind .frame1.supprimer <ButtonRelease-1> "supprimescenario"
bind .frame1.list1 <Double-ButtonRelease-1> "capturelist1"
proc dans { el liste } {
foreach el_gen $liste {
if { $el_gen == $el } {
return 1
}
}
return 0
}

# pour ajouter des noms de sc�narios et en  mettre � jour la liste##############

proc nouveauscenario {} {
    global DOSSIER_EXOS_DIV   basedir  DOSSIER_EXOS1 env   choix_lieu   Home  scena_div
##   cd $DOSSIER_EXOS1
    set scena [.frame1.text1 get]
	if { $scena  == "" } {
return
## set scena "oubli"
}
    if {[catch {set f [open [file join $DOSSIER_EXOS1 $scena_div ] "a"]}]} {
        bell
        return
    } else {
	set data ${scena}:
	puts $f $data
   }
      close $f

  #      peuplelist1
  #      set scena ""
  #      .frame1.list1 insert end $scena
	   .frame1.text1 delete 0 end
     .frame1.list1   delete 0 end
if {[catch {set f [open [file join  $DOSSIER_EXOS1  $scena_div] "r"]}]} {
        bell
        return
   	 } else {
	set liste_noms {}
	while { [gets $f ligne] >=0 } {
 	set liste_noms [linsert $liste_noms 0 [lindex [split $ligne ":"] 0]]
	}
set liste_noms [lsort $liste_noms ]
set liste_sans_doublons {}
foreach nom $liste_noms {
if { ! [dans $nom $liste_sans_doublons] } {
set liste_sans_doublons [linsert $liste_sans_doublons 0 $nom ]
.frame1.list1 insert end $nom
   }
}
close $f
}
##cd $basedir
focus .frame1.text1
}
#pour supprimer un sc�nario et en mettre � jour la liste######

proc supprimescenario {} {
   global DOSSIER_EXOS_DIV   basedir    DOSSIER_EXOS1  env   choix_lieu     Home     scena_div
   cd  $DOSSIER_EXOS1
set nom [string trim [.frame1.text1 get]]
set f [open [file join   $scena_div] "r"]
set f1 [open [file join  scenar_div.dconf] "w+"]
while { [gets $f ligne] >=0 } {
              if {  [lindex [split $ligne ":"] 0]  != $nom } {
puts $f1 $ligne
}
}
close $f 
close $f1
file delete  [file join  $scena_div  ]
file copy -force [file join $DOSSIER_EXOS1 scenar_div.dconf]  [file join $DOSSIER_EXOS1 $scena_div  ]
file delete  [file join  $DOSSIER_EXOS1 scenar_div.dconf   ]
#cd $basedir
##peuplelist1
  .frame1.list1   delete 0 end
if {[catch {set f [open [file join  $DOSSIER_EXOS1  $scena_div] "r"]}]} {
        bell
        return
   	 } else {
	set liste_noms {}
	while { [gets $f ligne] >=0 } {
 	set liste_noms [linsert $liste_noms 0 [lindex [split $ligne ":"] 0]]
	}
}
set liste_noms [lsort $liste_noms ]
set liste_sans_doublons {}
foreach nom $liste_noms {
if { ! [dans $nom $liste_sans_doublons] } {
set liste_sans_doublons [linsert $liste_sans_doublons 0 $nom ]
.frame1.list1 insert end $nom
   }
}
close $f
.frame1.text1 delete 0 end
focus .frame1.text1
}


 ###choix du fichier liste de sc�narios �prr�s avoir choisi le dossier le contenant.
proc choix_scenaconf {} {
  global scena_div  DOSSIER_EXOS1 test_elev DOSSIER_EXOS choix_lieu  choix_ok_fich  basedir Home
 ###   .frame1.nom_fich_scena     insert end  "$basedir + $DOSSIER_EXOS1"
    set types {
    {"Cat�gories"		{.dconf}	}
}
 if { $DOSSIER_EXOS1  ==  $basedir || $test_elev == 0 } {
set scena_div "scenarios_div.dconf"
##.frame1.nom_fich_scena configure -state disabled
return
}
 if { $choix_ok_fich == "oui" && [.frame1.nom_fich_scena get ] != ""} {
 set scena_div_prov   "[.frame1.nom_fich_scena get ].dconf"
set f [open [file join  $DOSSIER_EXOS1  $scena_div_prov ] "a+"]
close $f
}

set scena_div  [tk_getOpenFile -filetypes $types -initialdir $DOSSIER_EXOS1  -title "[mc {Choix du fichier de sc�narios}]"] 

}
###.frame1.nom_fich_scena

proc peuplelist1 {} {
   global DOSSIER_EXOS   basedir test_elev choix_lieu    env    DOSSIER_EXOS1   Home   scena_div
#   .frame1.supprimer configure  -state  normal
 #.frame1.ajouter configure  -state  normal
 #.frame2.ajoutsc configure  -state  normal
 #focus .frame1.text1
   choix_scenaconf
      if { $DOSSIER_EXOS1  ==   $basedir || $test_elev == 0 } {
  ## r�pertoire choisi, choisir le fichierconf qui passe en param�tre
.frame1.supprimer configure  -state  disabled
 .frame1.ajouter configure  -state  disabled
 .frame2.ajoutsc configure  -state  disabled
set scena_div "scenarios_div.dconf"
}
cd  $DOSSIER_EXOS1
      .frame1.list1   delete 0 end
##	  set scena_div "scenarios_div.dconf"
if {[catch {set f [open [file join $scena_div] "r"]}]} {
        bell
		close $f
        return
   	 } else {
	set liste_noms {}
	while { [gets $f ligne] >=0 } {
 	set liste_noms [linsert $liste_noms 0 [lindex [split $ligne ":"] 0]]
	}
set liste_noms [lsort $liste_noms ]
set liste_sans_doublons {}
foreach nom $liste_noms {
if { ! [dans $nom $liste_sans_doublons] } {
set liste_sans_doublons [linsert $liste_sans_doublons 0 $nom ]
.frame1.list1 insert end $nom
   }
}
close $f
##cd $basedir
}
.frame1.fich_scena configure -text "[mc {choix_fich_scena}]\n[file tail $scena_div]"
}
if { $DOSSIER_EXOS1  ==  $basedir  }  {
 .frame1.nom_fich_scena configure -state disabled
peuplelist1
}
focus .frame1.text1

proc capturelist1 {} {
 global DOSSIER_EXOS_DIV   basedir  DOSSIER_EXOS1  Home    choix_lieu     scena_div
    cd  $DOSSIER_EXOS1
 .frame2.sel7 delete 0 end ; .frame2.sel8 delete 0 end 
 .frame2.sel71 delete 0 end ; .frame2.sel81 delete 0 end 

 .frame2.sel9 delete 0 end ; .frame2.sel10 delete 0 end 
for { set i 1} { $i <= 5} {incr i} {
.frame2.sel$i deselect
}
#.frame2.sel11 deselect
.frame2.sel7 configure -state normal ; .frame2.sel8 configure -state normal
.frame2.sel71 configure -state normal ; .frame2.sel81 configure -state normal
.frame2.sel9 configure -state normal ; .frame2.sel10 configure -state normal
  set scenario [.frame1.list1 get active]
  set f [open [file join  $DOSSIER_EXOS1 $scena_div] "r"]
        
set liste_noms {}
	while { [gets $f ligne] >=0 } {
              if { [lindex [split $ligne ":"] 0] == $scenario } {
	set nom  [lindex [split $ligne ":"] 0] ; #frame2. insert $nom
	set reper [lindex [split $ligne ":"] 1]
	if {$reper == 1} {
	.frame2.sel1 select	
      } else {
     .frame2.sel1 deselect
	}
	set  calc_quot [lindex [split $ligne ":"] 2] 
	if {$calc_quot == 1 } {
	.frame2.sel2 select	
      } else {
     .frame2.sel2 deselect
	}
set calc_rest [lindex [split $ligne ":"] 3]
if {$calc_rest == 1 } {
	.frame2.sel3 select	
      } else {
     .frame2.sel3 deselect
	}
	set quot_noeud [lindex [split $ligne ":"] 4]
if {$quot_noeud == 1 } {
	.frame2.sel4 select
      } else {
     .frame2.sel4 deselect
	}
	set recup [lindex [split $ligne ":"] 5]
if {$recup == 1 } {
	.frame2.sel5 select	
      } else {
     .frame2.sel5 deselect
	}
set choix_fixe [lindex [split $ligne ":"] 6]
 switch $choix_fixe {
      "f" {
	.frame2.f  select
	.frame2.sel9 configure -state normal ; .frame2.sel10 configure -state normal 
	.frame2.sel7 configure -state disabled ; .frame2.sel8 configure -state disabled 
	}
	"h" {
	.frame2.h  select
	.frame2.sel7 configure -state normal ; .frame2.sel8 configure -state normal 
	.frame2.sel71 configure -state normal ; .frame2.sel81 configure -state normal 
	.frame2.sel9 configure -state disabled ; .frame2.sel10 configure -state disabled 
	}
	"af" {
   .frame2.af  select
	.frame2.sel7 configure -state disabled ; .frame2.sel8 configure -state disabled 
	.frame2.sel9 configure -state disabled ; .frame2.sel10 configure -state disabled 
	}
}


#if {$nbre_fixe == 1 } {
# .frame2.sel11 select  
#} else {
#.frame2.sel11 deselect  
#}
#set  list_div [lindex [split $ligne ":"] 7]
.frame2.sel7 insert 0 [lindex [split $ligne ":"] 7]
.frame2.sel71 insert 0 [lindex [split $ligne ":"] 11]

##set list_quot [lindex [split $ligne ":"] 8]
.frame2.sel8 insert 0 [lindex [split $ligne ":"] 8]
.frame2.sel81 insert 0 [lindex [split $ligne ":"] 12]

set  divid_fixe [lindex [split $ligne ":"] 9]
.frame2.sel9 insert 0 $divid_fixe 
	set divis_fixe [lindex [split $ligne ":"] 10]
.frame2.sel10 insert 0 $divis_fixe
close $f      
break		
	}

}
	.frame2.scenario configure -text  "[mc {Param�tres de }] [.frame1.list1 get active]  :"
    .frame2.ajoutsc configure -text "[mc {Ajouter-Maj sc�nario de nom :}] $scenario"
cd $basedir
}

proc ajoutescenario {} {
global sel1 sel2 sel3 sel4 sel5 choix_nombre sel7 sel71 sel8 sel81 sel9 sel10 sel11    basedir  \
  DOSSIER_EXOS1 choix_lieu  Home    scena_div
    cd  $DOSSIER_EXOS1
#tester ici les pbs du radio
switch -exact $choix_nombre {
       "f" {
	.frame2.sel7 delete 0 end ; .frame2.sel8 delete 0 end 
	}
	"h" {
	.frame2.sel9 delete 0 end ; .frame2.sel10 delete 0 end 
	if { [.frame2.sel8 get]   >= [.frame2.sel71 get]  } {
	.frame2.sel8 delete 0 end ; .frame2.sel8 insert 0 [.frame2.sel7 get]
	.frame2.sel81 delete 0 end ; .frame2.sel81 insert 0 [.frame2.sel71 get]
	}
	
	}
	"af" {
	.frame2.sel7 delete 0 end ; .frame2.sel8 delete 0 end 
	.frame2.sel9 delete 0 end ; .frame2.sel10 delete 0 end 
}
}
#tester si tout vide
if { $sel1 == "" } {
return
}
set nom [.frame1.list1 get active] 
set data "[.frame1.list1 get active]:$sel1:$sel2:$sel3:$sel4:$sel5:$choix_nombre:[.frame2.sel7 get]:[.frame2.sel8 get]:[.frame2.sel9 get]:[.frame2.sel10 get]:[.frame2.sel71 get]:[.frame2.sel81 get]:" 
set f [open [file join $DOSSIER_EXOS1 $scena_div ] "r"]
set f1 [open [file join $DOSSIER_EXOS1 scenar_div.dconf] "w+"]
set data_neuf {}
while { [gets $f ligne] >=0 } {
              if { [lindex [split $ligne ":"] 0] != $nom } {
puts $f1 $ligne
}
}
puts $f1 $data
close $f ; file delete [file  join  $DOSSIER_EXOS1 $scena_div  ]
close $f1
file copy -force [file  join  $DOSSIER_EXOS1 scenar_div.dconf]  [file  join  $DOSSIER_EXOS1  $scena_div  ]
file delete [file  join  $DOSSIER_EXOS1 scenar_div.dconf ]
cd $basedir
}





