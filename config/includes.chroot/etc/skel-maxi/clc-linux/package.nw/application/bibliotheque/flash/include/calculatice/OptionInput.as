﻿class OptionInput {
	private var cadre_input:MovieClip;
	public var champ_input:TextField;
	private var cadre_x:Number;
	private var cadre_y:Number;
	private var nom;
	public function OptionInput(mc_conteneur:MovieClip, nom:String, sTexte:String) {
		this.nom = nom;
		this.cadre_input = mc_conteneur.createEmptyMovieClip(nom, mc_conteneur.getNextHighestDepth());
		var input_label = this.cadre_input.createTextField(nom+"_label", this.cadre_input.getNextHighestDepth(), 0, 0, 0, 0);
		input_label.embedFonts=true;
		input_label.antiAliasType = "advanced";
		input_label.text = sTexte;
		var format = new TextFormat();
		format.size = 14;
		format.bold = false;
		format.font = "emb_tahoma";
		format.align = "center";
		format.leftMargin = 2;
		format.rightMargin = 2;
		input_label.setTextFormat(format);
		input_label.autoSize = true;
		this.champ_input = this.cadre_input.createTextField(nom+"_input", this.cadre_input.getNextHighestDepth(), input_label._width+5, 0, 10, 0);
		this.champ_input.embedFonts=true;
		this.champ_input.antiAliasType = "advanced";
		this.champ_input.type = "input";
		this.champ_input.border = false;
		this.champ_input.background = true;
		this.champ_input.backgroundColor = 0xEAEAFF;
		this.champ_input.autoSize = true;
		this.champ_input.text = "";
		this.champ_input._height = input_label._height;
		format.align = "left";
		this.champ_input.setNewTextFormat(format);
		//Selection.setFocus(this.champ_input);
		//Selection.setSelection(0,0);
	}
	public function SetPos(pos_x:Number, pos_y:Number) {
		this.cadre_input._x = pos_x;
		this.cadre_input._y = pos_y;
	}
	public function SetWidth(nLargeur){
		this.champ_input.autoSize = false;
		this.champ_input._width=nLargeur;
	}
	public function SetValeur(sValeur) {
		this.champ_input.text = sValeur;
	}
	public function RestrictValeur(sPattern) {
		this.champ_input.restrict = sPattern;
	}
	public function GetValeur() {
		return this.champ_input.text;
	}
	public function SetFocus(){
		Selection.setFocus(this.champ_input);
		Selection.setSelection(0,0);
	}
}
