﻿//Français :
p1_1=Le croupier
p1_2=Comparez le total de points des trois tas de cartes
// Les questions
p2_1=Le tas qui totalise le plus grand nombre de points est le tas n° $clip$
p2_2=Clique sur les cartes pour les retourner et trouve quel tas totalise le plus grand nombre de points.
//
p3_1=Tas n° 1
p3_2=Tas n° 2
p3_3=Tas n° 3