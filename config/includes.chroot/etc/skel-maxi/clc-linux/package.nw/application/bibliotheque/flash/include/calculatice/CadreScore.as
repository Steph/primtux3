﻿class CadreScore {
	private var objExo:CalcExo;
	public var cadre_score:MovieClip;
	private var titre:TextField;
	public var numessai:Number;
	public var score:Number;
	public var score_premier_essai:Number;
	public function CadreScore(obj) {
		this.objExo=obj;
		this.cadre_score = objExo.rootMovie.createEmptyMovieClip("cadre_score", objExo.rootMovie.getNextHighestDepth());
		this.score = 0;
	}
	public function SetScore(valeur, numquestion) {
		if (this.titre == undefined) {
			var d = this.cadre_score.getNextHighestDepth();
			this.titre = this.cadre_score.createTextField("CadreScore_titre", d, 10, 425, 100, 20);
			this.titre.embedFonts=true;
			this.titre.antiAliasType = "advanced";
		}
		this.score += valeur;
		if (this.numessai == 1) {
			this.score_premier_essai += valeur;
		}
		this.titre.text = objExo.GetTradCommun("p5_2")+" : "+this.score+" "+objExo.GetTradCommun("p5_3")+" "+numquestion;
		var fmt = new TextFormat();
		fmt.size = 18;
		fmt.bold = true;
		fmt.font = "emb_tahoma";
		this.titre.setTextFormat(fmt);
		this.titre.autoSize = true;
		this.cadre_score._visible = true;
	}
	public function Show() {
		this.cadre_score._visible = true;
	}
	public function Hide() {
		this.cadre_score._visible = false;
	}
	public function SetPos(x,y){
		this.cadre_score._x=x;
		this.cadre_score._y=y;
	}
}
