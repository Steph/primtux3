﻿class Bouton {
	public var bouton:MovieClip;
	private var etiquette:TextField;
	//public var _x:Number;
	//public var _y:Number;
	public function Bouton(targetClip:MovieClip, name:String, texte:String) {
		this.bouton = targetClip.createEmptyMovieClip(name, targetClip.getNextHighestDepth());
		this.etiquette = this.bouton.createTextField("etiquette", this.bouton.getNextHighestDepth(), 10, 0, 0, 0);
		this.etiquette.embedFonts=true;
		this.etiquette.antiAliasType = "advanced";
		this.etiquette.text = texte;
		var fmt = new TextFormat();
		fmt.size = 18;
		fmt.bold = true;
		fmt.font = "emb_tahoma";
		fmt.align = "center";
		this.etiquette.setTextFormat(fmt);
		this.etiquette.autoSize = true;
		trace_fond(this.bouton, this.bouton._x, this.bouton._y, this.etiquette._width+20, this.etiquette._height, 10, 0x003366, 0xFFFFFF);
	}
	public function Hide() {
		this.bouton._visible = false;
	}
	public function Show() {
		this.bouton._visible = true;
	}
	public function SetPos(posx:Number,posy:Number){
		this.bouton._x=posx;
		this.bouton._y=posy;
	}
	private function trace_fond(clip:MovieClip, x:Number, y:Number, largeur:Number, hauteur:Number, rayon:Number, couleur1, couleur2) {
		with (clip) {
			lineStyle(2, couleur1, 100, true);
			beginFill(couleur2);
			moveTo(x+rayon, y);
			lineTo(x+largeur-rayon, y);
			curveTo(x+largeur, y, largeur+x, rayon+y);
			lineTo(x+largeur, hauteur-rayon+y);
			curveTo(x+largeur, hauteur+y, x+largeur-rayon, hauteur+y);
			lineTo(x+rayon, hauteur+y);
			curveTo(x, hauteur+y, x, hauteur-rayon+y);
			lineTo(x, rayon+y);
			curveTo(x, 0+y, rayon+x, 0+y);
			endFill();
		}
	}
}
