var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.operationsatrous = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var cartouche, champReponse;
var aNombres=[], param_a, param_b, param_c, param_d, param_operateur;
var reponse_juste;

// Référencer les ressources de l'exercice (images, sons)
// {
//      nom-de-la-ressource : chemin-du-fichier
// }

exo.oRessources = { 
    illustration:"operationsatrous/images/illustration.png",
    cartouche:"operationsatrous/images/cartouche.png"
};

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
	totalQuestion:10, 
	totalEssai:1,
	tempsQuestion:0,
	nombre_donne:"1-10",
	nombre_a_trouver:"1-5",
	nDecimalesA:1,
	nDecimalesB:1,
	operateur:[1],
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
//Creation des donnees numeriques de l'exo
//Tirage des nombres en fonction des paramètres nombre_donne et nombre_b
//
    param_operateur=0;
    var i;
    var aTemp = [];

    for(i=0;i<exo.options.operateur.length;i++){
        param_operateur+=exo.options.operateur[i];
    }
    
    var pNa=String(exo.options.nombre_donne);
    var pNb=String(exo.options.nombre_a_trouver);
    var pDa=String(exo.options.nDecimalesA);
    var pDb=String(exo.options.nDecimalesB);
    aNombres=creer_couples(pNa,pNb,pDa,pDb);
    };
/**************
 * Fonctions Utiles
 *****************/ 
function choisir_operateur(paramOperateur,indiceQuestion){
    var type_operation=0;
    //choix de l'operateur
    //Selon le numéro de question, on fait varier les opérateurs
    switch(paramOperateur){
        case 1 :
        case 10 :
        case 100 :
        case 1000 :
            //un seul operateur coché
            type_operation=paramOperateur;
            break;
        case 11 ://addition soustraction
            type_operation = indiceQuestion%2===0 ? 1 : 10;
            break;
        case 101 ://addition multiplication
            type_operation = indiceQuestion%2===0 ? 1 : 100;
            break;
        case 1001 ://addition division
            type_operation = indiceQuestion%2===0 ? 1 : 1000;
            break;
        case 110 ://multiplication soustraction
            type_operation = indiceQuestion%2===0 ? 100 : 10;
            break;
        case 1100 ://multiplication division
            type_operation = indiceQuestion%2===0 ? 100 : 100;
            break;
        case 1010 ://division soustraction
            type_operation = indiceQuestion%2===0 ? 1000 : 10;
            break;
        case 1011 ://division soustraction addition
            if(indiceQuestion%3===0){type_operation=1000;}
            else if(indiceQuestion%3==1){type_operation=10;}
            else{type_operation=1;}
            break;
        case 1101 ://division multiplication addition
            if(indiceQuestion%3===0){type_operation=1000;}
            else if(indiceQuestion%3==1){type_operation=100;}
            else{type_operation=1;}
            break;
        case 1111 ://division mutiplication soustraction addition
            if(indiceQuestion%4===0){type_operation=1000;}
            else if(indiceQuestion%3==1){type_operation=100;}
            else if(indiceQuestion%3==2){type_operation=10;}
            else{type_operation=1;}
            break;
    }
    return type_operation;
}
function creer_couples(param_nombre_a,param_nombre_b,param_dec_a,param_dec_b){
    var param_a=param_nombre_a;
    var param_b=param_nombre_b;
    var aNombresA=[];
    var aNombresB=[];
    //si il n'y a pas d'option permettant de choisir le nombre
    // de chiffres après la virgule on travaille sur des entiers
    if(param_dec_a===undefined){param_dec_a=0;}
    if(param_dec_b===undefined){param_dec_b=0;}
    var aTemp = [],i,j;
    if(param_a.indexOf("-")<0){
    //il s'agit d'une liste de 1 ou plusieurs nombres
        aNombresA=param_a.split(";");
        for (i=0;i< aNombresA.length;i++) {
            aNombresA[i] = virgToPoint(aNombresA[i]);
        }
    } 
    else {
        //il s'agit d'un intervale
        aTemp=param_a.split("-");
        //generation des decimaux
        var nMiniA=Number(virgToPoint(aTemp[0]))*Math.pow(10,param_dec_a);
        var nMaxiA=Number(virgToPoint(aTemp[1]))*Math.pow(10,param_dec_a);
        var nRangeA=nMaxiA-nMiniA;
        for(i=0;i<=nRangeA;i++){
            aNombresA[i]=(nMiniA+i)/Math.pow(10,param_dec_a);
        }
    }
    if(param_b.indexOf("-")<0){
        //il s'agit d'une liste de 1 ou plusieurs nombres
        aNombresB=param_b.split(";");
        for (i=0;i< aNombresB.length;i++) {
            aNombresB[i] = virgToPoint(aNombresB[i]);
        }
    } 
    else {
        //il s'agit d'un intervale
        aTemp=param_b.split("-");
        //generation des decimaux
        var nMiniB=Number(virgToPoint(aTemp[0]))*Math.pow(10,param_dec_b);
        var nMaxiB=Number(virgToPoint(aTemp[1]))*Math.pow(10,param_dec_b);
        var nRangeB=nMaxiB-nMiniB;
        for(i=0;i<=nRangeB;i++) {
                aNombresB[i]=(nMiniB+i)/Math.pow(10,param_dec_b);
        }
    }
    var aPossibles=[];
    for(i=0;i<aNombresA.length;i++){
        for(j=0;j<aNombresB.length;j++){
            aPossibles.push([Number(aNombresA[i]),Number(aNombresB[j])]);
        }
    }
    util.shuffleArray(aPossibles);
    //aPossibles.sort(function(A,B){return Math.floor(Math.random()*3)-1});
	//on evite de travailler sur un tableau trop grand (100 maxi)
    aPossibles = aPossibles.slice(0,100);
	//on supprime les doublons dus à la commutativité des opérations
    var aCouples=[];
    aCouples[0]=aPossibles[0];
    for(i=1;i<aPossibles.length;i++){
        var existe_deja=false;
        for (j=0;j<aCouples.length;j++){
            if(aPossibles[i][0]==aCouples[j][1] && aPossibles[i][1]==aCouples[j][0] ){
                existe_deja=true;
                break;
            }
        }
        if(!existe_deja){
            aCouples.push(aPossibles[i]);
        }
    }
	//on Mélange le tableau
    util.shuffleArray(aCouples);
	//on ne laisse sortir le zero qu'une seule fois
    var supprime_zero=false;
    for(i=0;i<aCouples.length;i++){
        if(aCouples[i][0]===0 || aCouples[i][1]===0 ){
            if(supprime_zero){
                aCouples.splice(i,1);
                i--;
            }
            supprime_zero=true;
        }
    }
    console.log("aCouples");
    console.log(aCouples);
//on ajoute des couples si le nombre de couples obtenus est inférieur au nombre de questions
    var n=aCouples.length;
    var dif=exo.options.totalQuestion-aCouples.length;
    if(dif>0){
        for(i=n;i<exo.options.totalQuestion;i++){
            aCouples.push(aCouples[i%n]);
        }
    }
    console.log(aCouples);
    return aCouples;
}
//réécrit un nombre en fonction des normes françaises (virgule et séparateur des milliers)
//renvoie un string
function numToString(nNumber){
    var string=String(nNumber);
    var temp=string.split(".");
    var partie_entiere;
    var partie_decimale;	
    if(temp.length>1){
        partie_entiere=temp[0];
	partie_decimale=temp[1];		
    }else{
	partie_entiere=string;
    }
    if(partie_entiere.length>3){
	partie_entiere= partie_entiere.substr(0,partie_entiere.length-3) + " " + partie_entiere.substr(-3,3);
    }
    if(temp.length>1){
	return partie_entiere+","+partie_decimale;
    }else{
        return partie_entiere;
    }
}
//remplace la virgule par un point
function virgToPoint(sExpre) {
	return sExpre.split(",").join(".");
}
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Opérations à trous");
    exo.blocConsigneGenerale.html("Trouve le nombre qui manque.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    var operateur=choisir_operateur(param_operateur,q);
    var texte, resultat;
    var nA = new Big(aNombres[q][0]);
    var nB = new Big(aNombres[q][1]);
    if (operateur==1) {
        resultat = nA.plus(nB);
        reponse_juste=nB;
        if (q%3===0) {
            texte = nA.toFrench()+" + ? " + " = " + resultat.toFrench();
        }
        else if (q%3==1){
            texte = "? + "+nA.toFrench()+" = " + resultat.toFrench();
        }
        else if (q%3==2) {
            texte = nA.toFrench() + " pour aller à " + resultat.toFrench();
        }
    } 
    else if (operateur == 10) {
        resultat = nA.plus(nB);
        reponse_juste=nB;
        if (q%3===0) {
            texte = resultat.toFrench() +" - ? " + " = " + nA.toFrench();
        }
        else if (q%3==1){
            texte = resultat.toFrench() +" - " + nA.toFrench() + " =  ?" ;
        }
        else if (q%3==2) {
            texte = nA.toFrench() + " pour aller à " + resultat.toFrench();
        }
    } 
    else if (operateur == 100) {
        resultat = nA.times(nB);
        reponse_juste=nB;
        if (q%3===0) {
            texte = nA.toFrench()+" x ? " + " = " + resultat.toFrench();
        }
        else if (q%3==1){
            texte = "? x "+nA.toFrench()+" = " + resultat.toFrench();
        }
        else if (q%3==2) {
            texte = "Dans " + resultat.toFrench() + " combien de fois " + nA.toFrench();
        }
    } 
    else if (operateur == 1000) {
        resultat = nA.times(nB);
        reponse_juste=nB;
        if (q%3===0) {
            texte = resultat.toFrench() +" : ? " + " = " + nA.toFrench();
        }
        else if (q%3==1){
            texte = resultat.toFrench() +" : " + nA.toFrench() + " =  ?" ;
        }
        else if (q%3==2) {
            texte = "Dans " + resultat.toFrench() + " combien de fois " + nA.toFrench();
        }
    }

    cartouche = disp.createImageSprite(exo,"cartouche");
    exo.blocAnimation.append(cartouche);
    cartouche.position({
        my:'center center',
        at:'center center-50',
        of:exo.blocAnimation
    });
    //
    var etiquette = disp.createTextLabel(texte);
    etiquette.css({
        fontSize:26,
        fontWeight:'bold',
        color:'#fff'
    });
    cartouche.append(etiquette);
    etiquette.position({
        my:'center center',
        at:'center center',
        of:cartouche
    });
    //
    champReponse = disp.createTextField(exo,6);
    //cartouche.append(champReponse);
	exo.blocAnimation.append(champReponse);
    champReponse.position({
		my:'center center',
        at:'center center+100',
        of:cartouche
    });
    champReponse.focus();
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    if (champReponse.val() === "") {
       return "rien" ;
    } 
    else if( Big(virgToPoint(champReponse.val())).eq(reponse_juste) ) {
        return "juste";
    } 
    else {
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    cartouche.position({
        my:'center top',
        at:'center top+100',
        of:exo.blocAnimation,
    });
    //
    var correction = disp.createCorrectionLabel(reponse_juste.toFrench());
    exo.blocAnimation.append(correction);
    correction.position({
        my:"left center",
        at:"right+20 center",
        of:champReponse,
    });
    var barre = disp.drawBar(champReponse);
	//.css({left:champReponse.position().left,top:champReponse.position().top});
	exo.blocAnimation.append(barre);
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var conteneur = disp.createOptConteneur();
    exo.blocParametre.append(conteneur);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsQuestion",
        texte:"Temps pour réaliser chaque question (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:"Nombre d'essais : ",
        aValeur:[1,2],
        aLabel:["Un seul essai","Deux essais"]
    });
    exo.blocParametre.append(controle);
	
    /*
     * Création du conteneur contenant Nombre A et Décimale A
     */
    conteneur = disp.createOptConteneur();
    exo.blocParametre.append(conteneur);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:200,
        taille:50,
        nom:"nombre_donne",
        texte:"Nombre A : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"nDecimalesA",
        texte:"Décimales : "
    });
    conteneur.append(controle);
	
    /*
     * Création du conteneur contenant Nombre B et Décimale B
     */
    conteneur = disp.createOptConteneur();
    exo.blocParametre.append(conteneur);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:200,
        taille:50,
        nom:"nombre_a_trouver",
        texte:"Nombre B : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"nDecimalesB",
        texte:"Décimales : "
    });
    conteneur.append(controle);
	
    controle = disp.createOptControl(exo,{
        type:"checkbox",
        largeur:24,
        taille:2,
        nom:"operateur",
        texte:"Opérateur : ",
        aValeur:[1,10,100,1000],
        aLabel:["Addition","Soustraction","Multiplication","Division"]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));