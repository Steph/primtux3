	var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.ticketdecaisse = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var enonce,caissetab,WIN;
var alt=0;
var scale;


// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
		totalTentative:1,       
		tempsQuestion : 40,
        billet: "5;10;20;50;100",  // 1 billet par question  
        article: 3,  // nombre d'articles par ticket
        caisse: 3,  // nombre de caisses et donc de propositions
        format: 0.1,  // format du prix de chaque article (à 0,01/0,10/0,25/1 €)
        ecart: 2 //écart entre le billets et la valeur des tickets, exprimé en "format". Par exemple, un écart de 2 avec un format de 0,25 vaut 2*0.25.  Le bon ticket sera à billet-2*ecart, et les mauvais à billet+2*ecart.
        
        //Attention ! il y a une certaine contrainte dans les paramètres: 
        // Par exemple, avec un billet de 5€, des prix formatés à 1€ et un écart de 2 (donc 2€), le ticket gagnant sera à 5-2=3€, et donc il ne sera
        // pas possible et faire 4 articles sous peine de mettre le programme dans une boucle infinie (4 articles au prix minimal de 1€ ne feront 
        // jamais une somme de 3€)
        
        
		
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "ticketdecaisse/textes/ticketdecaisse_fr.json",
	caisse:"ticketdecaisse/images/caissiere.png",
	caisse2:"ticketdecaisse/images/caissiere2.png",
	bras:"ticketdecaisse/images/bras.png",
	art1:"ticketdecaisse/images/obj1.png",
	art2:"ticketdecaisse/images/obj2.png",
	art3:"ticketdecaisse/images/obj3.png",
	ticket:"ticketdecaisse/images/ticket.png",	
	illustration:"ticketdecaisse/images/Ticketdecaisse.jpg",
	b5:"ticketdecaisse/images/5.png",
	b10:"ticketdecaisse/images/10.png",
	b20:"ticketdecaisse/images/20.png",
	b50:"ticketdecaisse/images/50.png",
	b100:"ticketdecaisse/images/100.png"
		
};

function Article(){
	
	this.clip=new createjs.Container();
	var fond=new createjs.Shape();
	fond.graphics.beginFill(getRandomColor()).drawRect(0, 0, 60, 60);
	var mask=new createjs.Shape();
	var bitmap;
	var r=Math.floor(Math.random()*3);
	switch(r){
		case 0: bitmap=new createjs.Bitmap(exo.getURI("art1"));
		        mask.graphics.f().s("#000000").ss(1,1,1).p("ACWh8IAAD5QAAAUgsAPQgtAPg9AAQg8AAgtgPQgrgPgBgTIAAj6QAAgUAsgPQAtgPA8AAQA9AAAtAPQAsAPAAAUg");
				mask.graphics.f("#000000").s().p("AhoCfQgsgOgBgUIAAj4QABgVAsgPQArgPA9AAQA9AAAsAPQAtAPgBAVIAAD3QABAVgtAOQgsAPg9ABQg9gBgrgPg");
				mask.setTransform(15,17.5);
				break;	
		case 1: bitmap=new createjs.Bitmap(exo.getURI("art2"));
		        mask.graphics.f().s("#000000").ss(1,1,1).p("AAgjCQBJAwgIBSQgPBBgDBAQgFBkAXBfIjBAAQAXhfgFhkQgDhAgPhBQgIhSBJgwIAAhBIA/AAg");
				mask.graphics.f("#000000").s().p("AhgEEQAXhfgFhlQgDg/gPhAQgIhTBJgwIAAhBIA/AAIAABBQBJAwgIBTQgPBAgDA/QgFBlAXBfg");
				mask.setTransform(9.8,26);
				break;
		case 2: bitmap=new createjs.Bitmap(exo.getURI("art3"));
		        mask.graphics.f().s("#000000").ss(1,1,1).p("ADIiVIAADHIhkBkIkrAAIAAjHIBkhkg");
				mask.graphics.f("#000000").s().p("AjHCVIAAjGIBkhkIEqAAIAADGIhkBkg");
				mask.setTransform(20,15)
				break;	
	}
	fond.mask=mask;
	bitmap.x=bitmap.y=-1
	this.clip.addChild(fond);	
	this.clip.addChild(bitmap);	
	this.clip.regY=this.clip.getBounds().height;
	this.clip.cache(0,0,60,60);
	
	
}

function Caisse(prix,n,s){
	this.clip=new createjs.Container();
	this.clip.win=false;
	this.clip.numero=n;
	var k=new String(s);			 
	var c=k.substr(0,k.length-2)+","+k.substr(k.length-2,2);
			  if(k.length==1){
				 c="0,0"+k;
			  }
			  if(k.length==2){
				  c="0"+c;
			  }
	var solution=c+" €";
	//console.log(solution);
	var bras=new createjs.Bitmap(exo.getURI("bras"));
	bras.regX=5;
	bras.regY=5;
	bras.x=270;
	bras.y=50;
	this.clip.addChild(bras);	
	var caisse;
	if(alt==0){
	caisse=new createjs.Bitmap(exo.getURI("caisse"));
	alt++;
	}
	else {
		caisse=new createjs.Bitmap(exo.getURI("caisse2"));
		alt=0;
		caisse.y=9
		
	}
	caisse.x=50;
	this.clip.addChild(caisse);
	var ticket=new createjs.Container();
	var ticketFond=new createjs.Bitmap(exo.getURI("ticket"));
	ticket.addChild(ticketFond);
	var listeC=["B","C","D","F","G","J","M","N","P","R","S","T","V"];
	var listeV=["A","E","I","O","U"];
	for(var i=0;i<prix.length;i++){
		var ch=listeC[Math.floor(Math.random()*listeC.length)]+listeV[Math.floor(Math.random()*listeV.length)]+listeC[Math.floor(Math.random()*listeC.length)]+"...";
		var label=new createjs.Text(ch, "18px Calibri", "#FFFFFF")	
		label.x=5;
		label.y=20+25*i;
		label.textBaseline = "alphabetic";
		label.textAlign = "left";		
		ticket.addChild(label);
		var cout=new createjs.Text(prix[i], "18px Calibri", "#000000")	
		cout.x=100;
		cout.y=20+25*i;
		cout.textBaseline = "alphabetic";
		cout.textAlign = "right";		
		ticket.addChild(cout);
		var a=new Article();
		a.clip.alpha=0;
		a.clip.y=120;
		a.clip.x=20;
		this.clip.addChild(a.clip);
		var r=Math.random()*50;
		var r2=Math.random()*500;
		createjs.Tween.get(a.clip).wait(1000*i+r2).to({alpha:1},500).to({x:210+r},1000,createjs.Ease.elasticOut()).call(TicketOut,[i]);
	}
		
	ticket.cache(0,0,110,140);
	var mask=new createjs.Shape();
	mask.graphics.beginFill("#000000").drawRect(0,0,110,140);		
	var machine=new createjs.Container();
	ticket.mask=mask;
	machine.x=345;
	machine.addChild(ticket);
	var s=new createjs.Shape();
	s.graphics.beginStroke("#000000",1).moveTo(-20,0).lineTo(120,0);
	machine.addChild(s);
	ticket.visible=false;	
	this.clip.addChild(machine);
	machine.mouseChildren=false;
	
	var indicateurV=new createjs.Shape();
	indicateurV.graphics.beginFill("#000000").drawRect(0,0,100,130);
	indicateurV.filters =[new createjs.BlurFilter(5, 5, 10) ,new createjs.ColorFilter(0, 0, 0, 1, 0, 255, 0)];
	indicateurV.cache(-10,-10,130,170)	
	indicateurV.x=350;
	indicateurV.y=0;
	indicateurV.visible=false;
	this.clip.addChildAt(indicateurV,0);
	
	var indicateurF=new createjs.Shape();
	indicateurF.graphics.beginFill("#000000").drawRect(0,0,100,130);
	indicateurF.filters =[new createjs.BlurFilter(5, 5, 10) ,new createjs.ColorFilter(0, 0, 0, 1, 255, 0, 0)];
	indicateurF.cache(-10,-10,130,170)	
	indicateurF.x=350;
	indicateurF.y=0;
	indicateurF.visible=false;
	this.clip.addChildAt(indicateurF,0);		
	
	
	function TicketOut(m){
		var Y=ticket.y
		createjs.Tween.get(ticket).to({y:Y+25},500,createjs.Ease.elasticOut());
		if(m==prix.length-1){
			createjs.Tween.removeTweens(bras);
			machine.cursor="pointer";
			machine.addEventListener("click",Verification);
			
		}
	}
	
	this.GetTicket=function(n){
		 ticket.y=-135;
		 ticket.visible=true;
		 createjs.Tween.get(ticket).to({y:-135+130-25*n},500,createjs.Ease.elasticOut());
		 createjs.Tween.get(bras, {loop:true}).to({rotation:20},100).to({rotation:0},100);	   
	}
	this.Select=function(){
		indicateurV.visible=true;
	}
	this.Reset=function(){
		indicateurF.visible=indicateurV.visible=false;
	}
	this.Reveal=function(cl){
		var shadow
		var soltext
		if(cl.win){
			indicateurV.visible=true;			
			soltext=new createjs.Text(solution, "22px Calibri", "#00FF00")	
			soltext.x=400;
			soltext.y=50;
			soltext.textBaseline = "alphabetic";
			soltext.textAlign = "right";
			shape = new createjs.Shape();			
            shape.x = 400;
            shape.y = 50;			
            cl.addChild(shape);
			//console.log(soltext.getMeasuredLineHeight);
			shape.graphics.beginStroke("green").beginFill("white").drawRoundRect(-soltext.getBounds().width, -soltext.getBounds().height+3,soltext.getBounds().width,soltext.getBounds().height,5)
			cl.addChild(soltext);
		} 
		else {
			indicateurF.visible=true;
			soltext=new createjs.Text(solution, "22px Calibri", "#FF0000")	
			soltext.x=400;
			soltext.y=50;
			soltext.textBaseline = "alphabetic";
			soltext.textAlign = "right";
			shape = new createjs.Shape();			
            shape.x = 400;
            shape.y = 50;			
            cl.addChild(shape);
	        shape.graphics.beginStroke("green").beginFill("white").drawRoundRect(-soltext.getBounds().width, -soltext.getBounds().height+3,soltext.getBounds().width,soltext.getBounds().height,5) 
			cl.addChild(soltext);
		}
		
				
	}
	
	
}

function Verification(e){
	if(e.target.parent.win){
		WIN=true;
	}
	else {
		WIN=false;
	}
	for(var i=0;i<caissetab.length;i++){
		caissetab[i].Reset();
		if(caissetab[i].clip.numero==e.target.parent.numero){
			caissetab[i].Select();
		}
	}
		
		
}

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {  
   ///  paramétrage Rallye
  // var e_Caisse=[3,3,3,3,4];   // Le nombre de caissière
  // var e_Article=[2,2,3,3,3];  // Le nombre d'articles
 //  var e_Prix=[50,20,10,10,20];  // Les sommes totales proposées
 //  var e_Resultat=["-","-","-","-","-"];  // La somme relative demandée
  // var e_Arrondi=[0.1,0.25,0.25,0.1,0.1];  // Prix donné, arrondi au centième,au dixième d'euro,au 25 centièmes ou à l'unité d'euro.
 //  var e_Delta=[2,2,2,2,2]; // L'écart entre les propositions fausses et les sommes proposées dans E_Prix, en dixièmes,centièmes ou quart d'euro (dépend de e_Arrondi)
//   var e_Unite=[100,100,100,10,40]; // L'écart en centième d'euros entre les prix proposés et un nombre entier d'euros, AVANT modification par e_delta. Ne peut être inférieur à e_Arrondi. 
    
     var e_Caisse=[];  
     var e_Article=[];  
     var e_Prix=[];  
     var e_Resultat=[];  
     var e_Arrondi=[];  
    var e_Delta=[]; 
     var e_Unite=[]
    var t=exo.options.billet.split(";");
    var b=[];
    for(var i=0;i<t.length;i++){
        b.push(Number(t[i]));
    }
    
   for( var i=0;i<exo.options.totalQuestion;i++){
       e_Caisse.push(exo.options.caisse);
       e_Article.push(exo.options.article);
       e_Prix.push(b[i]);
       e_Resultat.push("-");
       e_Arrondi.push(Number(exo.options.format));
       e_Delta.push(exo.options.ecart);
       e_Unite.push(100)
   }
  
   enonce=[e_Caisse,e_Article,e_Prix,e_Resultat,e_Arrondi,e_Delta,e_Unite]; 
    console.log(enonce);
}

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
	var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    // on ajoute un element canvas au bloc animation 
    // Attention ses dimensions doivent être fixées avec .attr(width:xx,height:xx)
    // et pas avec .css({width:xx,height:yy})
	var canvas = $("<canvas id='cnv'></canvas>");
    canvas.attr({width:735,height:400});
    canvas.css({position:'absolute',top:0,background:"#FFFFFF"});
    exo.blocAnimation.append(canvas);
    //on cree la scene avec createjs
    stage = new createjs.Stage("cnv");
	stage.enableMouseOver()

    /**************************************/
    exo.aStage.push(stage); // pour qu'on puise appeler Touch.disable() au déchargement de l'exercice
    createjs.Ticker.timingMode = createjs.Ticker.RAF; // pour une animation Tween plus fluide
    createjs.Ticker.addEventListener("tick", stage); // pour que la scene soit redessinée à chaque "tick"
    createjs.Ticker.addEventListener("tick", createjs.Tween); // pour que Tween fonctionne après un Ticker.reset() au déchargement de l'exercice;
    createjs.Touch.enable(stage);// pour compatibilité avec les tablettes tactiles
	stage.enableMouseOver()
    /*************************************/
	
	exo.keyboard.config({
		numeric : "disabled",
		arrow : "disabled",
		
    });
	
	var i,j,k,l;
	WIN="rien";
	// Enonce=[E_Caisse,E_Article,E_Prix,E_Resultat,E_Arrondi,E_Delta]; 
	var n=enonce[0][exo.indiceQuestion] // le nombre de caissière
	var q=[];
	var solution=[]; 
	for(i=0;i<enonce[0][exo.indiceQuestion];i++){
		var panier=[];		
		panier=getPrix(enonce[2][exo.indiceQuestion],enonce[1][exo.indiceQuestion],enonce[4][exo.indiceQuestion],enonce[6][exo.indiceQuestion]);		
		 if(i==0){
			   switch(enonce[3][exo.indiceQuestion]){						  
					   case "-": var e=enonce[5][exo.indiceQuestion]*100*enonce[4][exo.indiceQuestion];
					            var l=0
								while(panier[l]<=e){
									l++;
								}
								panier[l]=panier[l]-e
					     		break;
						case "+": var e=enonce[5][exo.indiceQuestion]*100*enonce[4][exo.indiceQuestion];					           
								panier[0]=panier[0]+e
					     		break;
					   }
			  
		  }		
		  if(i>0){	
		         			
				  switch(enonce[3][exo.indiceQuestion]){					  
					  case "=": var e=enonce[5][exo.indiceQuestion]*100*enonce[4][exo.indiceQuestion];
					            if(panier[0]-e>0 && Math.random()<0.5){
									panier[0]=panier[0]-e
								} else{
									panier[0]=panier[0]+e
								}
					     		  break;
					   case "+": var e=enonce[5][exo.indiceQuestion]*100*enonce[4][exo.indiceQuestion];
					            var l=0
								while(panier[l]<=e){
									l++;
								}
								panier[l]=panier[l]-e
					     		break;
						case "-": var e=enonce[5][exo.indiceQuestion]*100*enonce[4][exo.indiceQuestion];					           
								panier[0]=panier[0]+e
					     		break;
					   }
			  }
		  var soluce=0;
		  for(k=0;k<panier.length;k++){	
		      soluce=soluce+panier[k];	  
			  panier[k]=new String(panier[k]);			 
			  var c=panier[k].substr(0,panier[k].length-2)+","+panier[k].substr(panier[k].length-2,2);
			  if(panier[k].length==1){
				 c="0,0"+panier[k];
			  }
			  if(panier[k].length==2){
				  c="0"+c;
			  }
			  panier[k]=c;
			  
		  }
		  solution.push(soluce);
		  q.push(panier);
		
		
		
	}
		
	var game=new createjs.Container();	
	caissetab=[];	
	
	for(i=0;i<n;i++)
	{   var s=new Number(solution[i])
	    caissetab.push(new Caisse(q[i],i,s));	    		
	}
	caissetab[0].clip.win=true;
	util.shuffleArray(caissetab);
	
		
	for(i=0;i<caissetab.length;i++)
	{		
	caissetab[i].clip.x=20+50*i;
	caissetab[i].clip.y=10+100*i;
	game.addChild(caissetab[i].clip);
	}
	
	scale=Math.min(380/game.getBounds().height,735/game.getBounds().width);
	if(scale<1){
	game.scaleX=game.scaleY=scale;}
	game.y=35;
	stage.addChild(game);
	var billet;
	switch(enonce[2][exo.indiceQuestion]){
		case 5:billet=new createjs.Bitmap(exo.getURI("b5"));break;
		case 10:billet=new createjs.Bitmap(exo.getURI("b10"));break;
		case 20:billet=new createjs.Bitmap(exo.getURI("b20"));break;
		case 50:billet=new createjs.Bitmap(exo.getURI("b50"));break;
		case 100:billet=new createjs.Bitmap(exo.getURI("b100"));break;
	}
	
	billet.x=625;
	billet.y=-200;
	stage.addChild(billet);
	createjs.Tween.get(billet).wait(1000).to({y:(400-billet.getBounds().height)/2},500)
	
	
	for(i=0;i<n;i++){
		caissetab[i].GetTicket(q[i].length);
	}
	var adj;
	switch (enonce[3][exo.indiceQuestion]){
		case"+": adj=exo.txt.superieur;break;
		case"=": adj=exo.txt.egal;break;
		case"-": adj=exo.txt.inferieur;break;
	}
	var text=exo.txt.rappel+enonce[2][exo.indiceQuestion]+exo.txt.unite;
	var consigne = disp.createTextLabel(text);
    	consigne.css({
        fontSize:14,
        fontWeight:'bold',
       // border:"1px solid #FFCCCC",
		"background-color":"#fff",
		padding:5,
		top:10,
		left:-1000
    });
	
	
	
	 exo.blocAnimation.append(consigne);
	 consigne.transition({x:1300},2000,'linear');
	 
	 exo.btnValider.css({
		 left:600
	 })
	
	
		
}

function getPrix(p,n,a,d){
	var prix=100*p;
	var i=0;
	if(a==0.1){
		c=0.1;
		pas=1;
	} 
	if(a==0.01){
		c=1
		pas=1;
	}
	if(a==1){
		c=0.01
		pas=1;
	}
	if(a==0.25){
		c=0.01
		pas=4;
	}
	
	var coef=1-(n-i)*a/p;
	//console.log(coef);
	var res=[];	
	var t=Math.floor(Math.floor(coef*Math.random()*prix*pas*c)/c)/pas;
	
	while((t==0) || (Math.abs(Math.round(t/100)-t/100)>d/100)){
		//console.log("1::::"+t+"/"+Math.round(t/100)+"-"+(t/100)+"<?"+d);
		t=Math.floor(Math.floor(coef*Math.random()*prix*pas*c)/c)/pas;
		}
	
	res.push(t);	
	i++;
	coef=1-(n-i)*a/p;
	while(i<n-1){
		//alert("ok->"+i+":"+res);
		prix=prix-res[i-1];
		 t=Math.floor(Math.floor(coef*Math.random()*prix*pas*c)/c)/pas;
		while((t==0) || (Math.abs(Math.round(t/100)-t/100)>d/100)){
		//console.log("2::::"+t+"/"+Math.round(t/100)+"-"+(t/100)+"<?"+d);
		t=Math.floor(Math.floor(coef*Math.random()*prix*pas*c)/c)/pas;
		}
		
	res.push(t);	
		i++;
	}
	res.push(prix-res[i-1])	
	// console.log(res);
	return(res);	
}



function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}




// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {		
	//console.log(WIN);
	if(WIN=="rien"){
		return("rien");
	}
	if(WIN){
	return "juste";	
	} else {
		return "faux";	
	}
	
	    
};

// Correction (peut rester vide)
exo.corriger = function() {
    for(var i=0;i<caissetab.length;i++){
		caissetab[i].Reset();
		caissetab[i].Reveal(caissetab[i].clip);		
		stage.update();
	}
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;

    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
   //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsQuestion",
        texte:exo.txt.opt2
    });
    exo.blocParametre.append(controle);
      //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:180,
        taille:20,
        nom:"billet",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:3,
        nom:"article",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[2,3,4]
    });
    exo.blocParametre.append(controle);
     //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:3,
        nom:"caisse",
        texte:exo.txt.opt5,
        aLabel:exo.txt.label5,
        aValeur:[2,3,4,5]
    });
    exo.blocParametre.append(controle);
    //
      controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"format",
        texte:exo.txt.opt6,
        aLabel:exo.txt.label6,
        aValeur:[0.01,0.1,0.25,1]
    });
    exo.blocParametre.append(controle);
       //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"ecart",
        texte:exo.txt.opt7
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));