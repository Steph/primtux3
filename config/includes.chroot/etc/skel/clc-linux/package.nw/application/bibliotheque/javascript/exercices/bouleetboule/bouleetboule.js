var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.bouleetboule = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];

var aNombre ;
var aNombreB;
var aCible = [];
var cible, cartouche;
var boule = [];
var groupeBouleEtiquette = [];
var texteBoule = [];
var nombreBoulesCliquees, premiereValeurCliquee, deuxiemeValeurCliquee;
var reponseEleve;
var nbrePaireSortie = 0;
var alignementBouleSortie = 0;

/*************************
 * requestID : stockera l'ID du requestFrameAnimation pour le stoper à la fin de l'exo
 * timeoutID : stockera l'ID du setTimeout pour la disparition automatique des bulles
 * evtType : stockera l'événement écouté (touchstart ou mousedown)
 ************************/
var requestID, timeoutID, evtType;

// Référencer les ressources de l'exercice (textes, image, son)
exo.oRessources = { 
    txt : "bouleetboule/textes/bouleetboule_fr.json",
    illustration : "bouleetboule/images/illustration.png"
};

// Définir les exo.options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalTentative:5,
        cible:[10,20,30,40,50],
        typeComplement:1,
        delai:7,
        totalPaires:5
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    exo.options.totalQuestion = exo.options.cible.length;
    aNombre = [];
    var cible;
    
    if (exo.options.typeComplement === 0)
    {
        for (var i=0;i<exo.options.totalQuestion;i++)
        {
            aNombreB = [];
            cible = exo.options.cible[i];
            for (var j=1;j<=5;j++)
            {
                aNombreB.push(j,cible-j);
            }
            //aNombreB.sort(function(){return Math.floor(Math.random()*3)-1});
            util.shuffleArray(aNombreB);
            aNombre.push(aNombreB);
        }
    }
    else if (exo.options.typeComplement == 1)
    {
        for (var k=0;k<exo.options.totalQuestion;k++)
        {
            aNombreB = [];
            cible = exo.options.cible[k];
            var nD = Math.floor(cible/10);// le nombre de dizaine de la cible
            for(var l=0;l<nD;l++){
                for (var o=1;o<=5;o++)
                {
                    var nA = l*10 + o;
                    var nB  = cible - nA;
                    aNombreB.push([nA,nB]);
                }
            }
            
            //aNombreB.sort(function(){return Math.floor(Math.random()*3)-1});
            util.shuffleArray(aNombreB);
            var aTemp = [];
            for(var p=0;p<5;p++){
                aTemp.push(aNombreB[p][0],aNombreB[p][1]);
            }
            aNombre.push(aTemp);
        }
    }

    console.log(aNombre);
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
	/*********************
	 * Le bouton valider est inutile
	 ********************/
	exo.btnValider.hide();
//Initialisation du nombre de boules sélectionnées
    nombreBoulesCliquees = 0;
//Clavier : pas besoin de touches actives
    exo.keyboard.config({
		numeric:"disabled",
		arrow:"disabled",
		large:"disabled",
	});
//Initialisation du nombre de paires de boules mises sur le côté (ira jusqu'à 5)    
    nbrePaireSortie = 0;
    alignementBouleSortie = 0;
//Raccourci pour la cible    
    cible = exo.options.cible[q];
/*************************
 * conteneur svg, dans lequel on crée un rectangle blanc à bords arrondis
 * les boules rebondiront dans ce rectangle
 ************************/    
    cartouche = disp.createSvgJsContainer(735,450);
    cartouche.css({left:20,top:20});
    var rectangle = cartouche.paper.rect();
    rectangle.attr({
        fill:"white",
        stroke:"#cccccc",
        width:540,
        height:350,
        rx:10,//Radius
        ry:10
    });
    exo.blocAnimation.append(cartouche);
    
    //Inscrustation de la cible
    var etiquetteCible = cartouche.paper.text(util.numToStr(cible));
    etiquetteCible.font({size:90,align:"center",anchor:"middle",fill:"#cccccc",opacity:0.7,leading:"1em"});
    etiquetteCible.center(270,150);

    //Réinitialisation des tableaux
    groupeBouleEtiquette = [];
    boule = [];
    etiquetteBoule = [];
    
    //Dégradés sur les boules
    //Bleu
    var gradientBleu = cartouche.paper.gradient('linear', function(stop) {
		stop.at({ offset: 0, color: '#00CCCC', opacity: 1 });
		stop.at({ offset: 1, color: '#0099CC', opacity: 1 });
	});
	gradientBleu.from(0, 0).to(0, 1);
    //Rouge
    var gradientRouge = cartouche.paper.gradient('linear', function(stop) {
		stop.at({ offset: 0, color: '#ee1a21', opacity: 0.4 });
		stop.at({ offset: 1, color: '#ee1a21', opacity: 1 });
	});
	gradientRouge.from(0, 0).to(0, 1);    
    //Vert
    var gradientVert = cartouche.paper.gradient('linear', function(stop) {
		stop.at({ offset: 0, color: '#72b339', opacity: 0.4 });
		stop.at({ offset: 1, color: '#72b339', opacity: 1 });
	});
	gradientVert.from(0, 0).to(0, 1);    
    //Orange
    var gradientOrange = cartouche.paper.gradient('linear', function(stop) {
		stop.at({ offset: 0, color: '#FAD1A4', opacity: 1 });
		stop.at({ offset: 1, color: '#FAAC58', opacity: 1 });
	});
	gradientOrange.from(0, 0).to(0, 1);
    //Affichage des boules
    for(var i=0;i<10;i++) {
        /******
         * Boules
         ******/
        boule[i] = cartouche.paper.circle(60);
        //boule[i].fill({color:"#61cbff"});
        boule[i].fill(gradientBleu);
        /******
         * Etiquettes
         ******/
        etiquetteBoule[i] = cartouche.paper.plain(util.numToStr(aNombre[q][i]));
        etiquetteBoule[i].font({size:30,align:"center",fill:"white",leading:"1em"});
        etiquetteBoule[i].center(30,30);//Ne modifie pas le x du groupe, car la boule est plus large que l'étiquette.
        /******
         * Groupes boule + étiquette
         * L'étiquette se trouvant entièrement dans la boule, les dimensions du groupe sont celles de la boule,
         * idem pour x et y.
         ******/
        groupeBouleEtiquette[i] = cartouche.paper.group();
        groupeBouleEtiquette[i].style({cursor:"pointer"});
        groupeBouleEtiquette[i].add(boule[i]);
        groupeBouleEtiquette[i].add(etiquetteBoule[i]);
        groupeBouleEtiquette[i].data("Valeur",aNombre[q][i]);
        groupeBouleEtiquette[i].data("Indice",i);
        groupeBouleEtiquette[i].data("Select","non");
        groupeBouleEtiquette[i].data("EnMouvement","oui");
        /******
         * Gestion de l'événement
         ******/        
        var evtType;
        evtType = exo.tabletSupport === true ? "touchstart" : "mousedown";
        groupeBouleEtiquette[i].on(evtType,gestionClick);
        /******
         * Placement du groupe
         ******/        
        var x = 40+i*35;
        var y = 20 + (i%5) * 35;
        groupeBouleEtiquette[i].move(x,y);
        /******
         * Dans quelle direction part chaque groupe ?
         * La direction est stockée par deux data, dX et dY
         ******/        
/*        if (i%2==0) {
            groupeBouleEtiquette[i].data("dX",-0.5);
            groupeBouleEtiquette[i].data("dY",-0.5);
        } else {
            groupeBouleEtiquette[i].data("dX",0.5);
            groupeBouleEtiquette[i].data("dY",0.5);
        }
*/
        if (i%4===0) {
            groupeBouleEtiquette[i].data("dX",-0.5);
            groupeBouleEtiquette[i].data("dY",-0.5);
        } else if (i%4===1) {
            groupeBouleEtiquette[i].data("dX",0.5);
            groupeBouleEtiquette[i].data("dY",0.5);
        } else if (i%4===2) {
            groupeBouleEtiquette[i].data("dX",0.5);
            groupeBouleEtiquette[i].data("dY",-0.5);
        } else {
            groupeBouleEtiquette[i].data("dX",-0.5);
            groupeBouleEtiquette[i].data("dY",0.5);
            
        }
    }
//Affichage de la consigne
    var consigne = cartouche.paper.text(function(add){
        add.tspan(exo.txt.question);
        add.tspan(String(cible)).fill({color:"#ee1a21"});
        add.tspan(".");
    });
    consigne.font({size:20,align:"center",fill:"black",leading:"1em"});
    consigne.move(0,370);
/*************************
 * le chronomètre n'est à lancer que dans le cas où le délai est != de 0
 *  -> au bout d'un certain nombre de secondes, deux boules disparaissent du rectangle
 *  Lancement de l'animation
 ************************/    
    lancementChronometre();
    boucleAnimation();
        
    function boucleAnimation(){
/*************************
 * On stocke l'ID pour pouvoir arrêter l'animation
 * On parcourt les boules, et on ne bouge que celles qui doivent être en mouvement
 ************************/    
        requestID = requestAnimationFrame(boucleAnimation);      
        for(var i=0;i<10;i++){
            if (groupeBouleEtiquette[i].data("EnMouvement")=="oui") {
                bougerBoule(groupeBouleEtiquette[i]);
            }
        }
    }
/*************************
 * Fonction qui sera appelée par le requestFrameAnimation, et qui fera bouger les groupes boule+etiquette
 ************************/    
    function bougerBoule(element){
        //posX et posY sont les coordonnées exacte du groupe à bouger
        var posX = element.bbox().x;
        var posY = element.bbox().y;
        //dX et dY sont les variables qui indiquent dans quelle direction a eu lieu le dernier mouvement
        var dX = element.data("dX");
        var dY = element.data("dY");
        //Si on arrive sur une extrémité, on inverse une des directions pour "rebondir"
        if ((posX<=0) || (posX>480)) {
            dX = -dX;
        }
        if ((posY<=0) || (posY>290)) {
            dY = -dY;
        }
        //On stocke la nouvelle direction grâce à data, et on fait bouger le groupe
        element.data("dX",dX);
        element.data("dY",dY);
        element.move(posX+dX,posY+dY);        
    }
/*************************
 * Gestion du click sur un groupe
 ************************/    
    function gestionClick(e) {
        nombreBoulesCliquees++;
        e.preventDefault();
        var elementClique = e.target.instance;
        /***************
         * groupe : variable contenant l'objet svg groupe
         * fond : variable contenant l'objet svg boule
         * texte : variable contenant l'objet svg texte
         * On recherche le parent de l'élément sur lequel on a cliqué, pour obtenir le groupe
         **************/
        var groupe,fond,texte;
        if (elementClique.type == "ellipse") {
            //Clic dans la boule
            groupe = elementClique.parent;
        }
        else if (elementClique.type == "text") {
            //Clic sur le texte
            groupe = elementClique.parent;
        }
        else if (elementClique.type == "tspan") {
            //Clic sur le texte
            groupe = elementClique.parent.parent;
        }
        if (groupe.data("EnMouvement")=="non") {
            nombreBoulesCliquees--;
            return false;
        }
        /***************
         * groupe est un objet, dont une des propriétés est children :
         * un Array dans laquel se trouve les éléments enfants (boule, texte et tspan)
         * La boule a l'indice 0, le texte a l'indice 1
         * On arrive donc à agir sur la boule, que l'on ait cliqué sur la boule ou le texte
         **************/
        fond = groupe._children[0];
        texte = groupe._children[1];
        //On vérifie si l'élève n'a pas cliqué deux fois sur la même boule :
        if (groupe.data("Select")=="oui") {
            reinitDoubleClickBoule(groupe,fond);//Cette manip désélectionnera la boule
        } else {
            //On change la couleur de la boule sélectionnée
            fond.fill(gradientOrange);
            if (nombreBoulesCliquees == 1) {
                premiereBouleCliquee = groupe.data("Valeur");
                groupe.data("Select","oui");
            }
            if (nombreBoulesCliquees == 2) {
                deuxiemeBouleCliquee = groupe.data("Valeur");
                groupe.data("Select","oui");
                //Calcul de la réponse de l'élève
                var ret = (premiereBouleCliquee + deuxiemeBouleCliquee == cible) ? 1 : 2;
                reinitialiserBoules(ret);
            }
        }
    }

/*************************
 * Réinitialisation des boules lorsque 2 ont été sélectionnées. Si la réponse est bonne, k vaut 1
 ************************/    
    function reinitialiserBoules(k) {
        var nbreBouleDeplacee = 0;
        var boule1, boule2;
        var indice1, indice2;
        indice1=indice2 = -1;
        var faux = "non";
        //On parcourt les groupes, et on ne garde que ceux qui ont été sélectionnés
        for(i=0;i<10;i++) {
            if (groupeBouleEtiquette[i].data("Select")=="oui") {
                //On Stocke l'indice des boules concernées
                (indice1==-1) ? indice1 = i : indice2 = i;
                if (k==1) {
                    //On réinitialise après une bonne réponse
                    groupeBouleEtiquette[i].data("EnMouvement","non");
                    //On relance le chronomètre
                    remiseAZeroChronometre();
                    //On stocke les deux boules dans boule1 et boule2
                    (nbreBouleDeplacee == 0) ? boule1 = groupeBouleEtiquette[i] : boule2 = groupeBouleEtiquette[i];
                    nbreBouleDeplacee++;
                    //On colorie en vert puisque c'est une bonne réponse
                    boule[i].fill(gradientVert);
                } else {
                    faux = "oui";
                    boule[i].fill(gradientRouge);
                }
                //Que la réponse soit bonne ou non, on désélectionne et on remet la couleur d'origine
                groupeBouleEtiquette[i].data("Select","non");
            }
            
        }
        if (faux=="oui") {
            exo.setTimeout(function(){
                bouleBleue(boule[indice1]);
                if (indice2!=-1) {
                    bouleBleue(boule[indice2]);
                }
            },600);
        }
        //Réinitialisation des variables qui permettent de calculer la réponse de l'élève
        premiereBouleCliquee = deuxiemeBouleCliquee = 0;
        nombreBoulesCliquees = 0;
        
        //Si des boules sont à déplacer...
        if (nbreBouleDeplacee !== 0) {
            var x1, x2, y1, y2;
            //Calcul des nouvelles coordonnées
            x1 = 590;
            x2 = 660;
            y1 = y2 = 30 + 55 * alignementBouleSortie;
            //Suppression des écouteurs
            boule1.off(evtType,gestionClick);
            boule2.off(evtType,gestionClick);
            //Animation des groupes
            boule1.animate().scale(0.8).center(x1,y1);
            boule2.animate().scale(0.8).center(x2,y2);
            nbrePaireSortie++;
            alignementBouleSortie++;
            reponseEleve = "ok";
/*            if (nbrePaireSortie == 5) {
                //Fin de l'exo
                cancelAnimationFrame(requestID);
                clearTimeout(timeoutID);
                exo.poursuivreExercice();
            }*/
            exo.poursuivreQuestion();
        }
    }
    function reinitDoubleClickBoule(groupe,fond) {
        //Réinitialisation des variables qui permettent de calculer la réponse de l'élève
        premiereBouleCliquee = deuxiemeBouleCliquee = 0;
        nombreBoulesCliquees = 0;
        groupe.data("Select","non");
        fond.fill(gradientBleu);
    }
    function bouleBleue(bouleRouge) {
        bouleRouge.fill(gradientBleu);
    }
/*************************
 * Fonction qui lance le chronomètre 
 ************************/        
    function lancementChronometre(){
        var delai = exo.options.delai;
        if (delai>0) {
            timeoutID = exo.setTimeout(chronometre,exo.options.delai*1000);
        } else {
            timeoutID = 0;
        }
    }
/*************************
 * Fonction réinitialise le chronomètre
 ************************/        
    function remiseAZeroChronometre(){
        if (timeoutID!==0) {
            clearTimeout(timeoutID);
        }
        lancementChronometre();
    }
/*************************
 * Que se passe-t-il quand le delai est écoulé ?
 ************************/        
    function chronometre(){
        var boule1, boule2;
        var nbreBouleDeplacee = 0;
        
        for(i=0;i<10;i++){
            if (groupeBouleEtiquette[i].data("EnMouvement")=="oui") {
                //On recherche la première boule en mouvement et qui n'est pas encore en dehors du rectangle blanc
                if (nbreBouleDeplacee === 0) {
                    //On la stocke dans boule1
                    boule1 = groupeBouleEtiquette[i];
                    nbreBouleDeplacee++;
                    boule[i].fill(gradientRouge);
                } else {
                    if (groupeBouleEtiquette[i].data("Valeur")+boule1.data("Valeur")==cible) {
                        //On cherche la seconde boule (le complément de boule1)
                        boule2 = groupeBouleEtiquette[i];
                        boule[i].fill(gradientRouge);
                    }
                }
            }
        }
/*************************
 * Pour éviter que les groupes ne continuent à bouger...
 * On supprime aussi l'écouteur d'événements
 ************************/                
        boule1.data("EnMouvement","non");
        boule2.data("EnMouvement","non");
        boule1.data("Select","non");
        boule2.data("Select","non");
        boule1.off(evtType,gestionClick);
        boule2.off(evtType,gestionClick);
        //Calcul des nouvelles coordonnées du centre
        x1 = 800;
        x2 = 800;
        y1 = y2 = 30 + 55 * alignementBouleSortie;
/*************************
 * On anime disparition et réapparition, et on bouge le groupe lorsqu'il n'est plus visible
 ************************/        
        boule1.animate(500,"-").attr({opacity:"0"}).after(function(){
            this.center(x1,y1).scale(0.8);
            this.animate(100,"-").attr({opacity:"1"});
        });
        boule2.animate(500,"-").attr({opacity:"0"}).after(function(){
            this.scale(0.8).center(x2,y2);
            this.animate(100,"-").attr({opacity:"1"});
            
        });
//On relance le chrono
        lancementChronometre();
//On incrémente le nombre de paires de boules hors rectangle blanc
        nbrePaireSortie++;
        reponseEleve = "nok";
        exo.poursuivreQuestion();
/*        if (nbrePaireSortie == 5) {
            //Fin de l'exo
            cancelAnimationFrame(requestID);
            clearTimeout(timeoutID);
            exo.poursuivreExercice();
        }*/
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    exo.blocInfo.css({
        left:560,
        top:360
    });
    if (nbrePaireSortie == 5) {
        //Fin de l'exo
        cancelAnimationFrame(requestID);
        clearTimeout(timeoutID);
    }
    if (reponseEleve=="ok") {
        return "juste";
    } else {
        return "faux";
    }
};


// Correction (peut rester vide)
exo.corriger = function() {
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
    //
    controle = disp.createOptControl(exo,{
        type:"checkbox",
        largeur:24,
        taille:2,
        nom:"cible",
        texte:exo.txt.opt6,
        aLabel:exo.txt.label6,
        aValeur:[10,20,30,40,50,60,70,80,90,100]
    });

    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeComplement",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[0,1]
    });
    exo.blocParametre.append(controle);

    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"delai",
        texte:exo.txt.opt5,
        aLabel:exo.txt.label5,
        aValeur:[0,15,10,7,5]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));