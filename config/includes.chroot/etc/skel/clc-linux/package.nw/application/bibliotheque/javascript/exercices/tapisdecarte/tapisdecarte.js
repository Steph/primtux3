var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.tapisdecarte = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var conteneurTapis, conteneurEmpreinteOrigine,aIndex, aIndexSoluce, aCible, cible, aEmpreinteCible;

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt             :   "tapisdecarte/textes/tapisdecarte_fr.json",
    jeuComplet      :   "tapisdecarte/images/cartes.png",
    illustration    :   "tapisdecarte/images/illustration.png"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    aCible=[12,13,14,15,16,17,18,19,20];
    aCible = util.shuffleArray(aCible);
    aIndex=[];
    aIndexSoluce=[];
    for (var i=0;i<5;i++){
        var cible = aCible[i];
        var aTemp = [];
        aIndex[i] = [];
        aIndexSoluce[i] = [];
        for(j=0;j<3;j++){
            // 40 cartes indexées de 1 à 40
            var indexA = 1 + Math.floor(Math.random()*(40-1+1));
            var valA = indexA%10 === 0 ? 10 : indexA%10;
            var indexB = 1 + Math.floor(Math.random()*(40-1+1));
            var valB = indexB%10 === 0 ? 10 : indexB%10;
            var indexC = 1 + Math.floor(Math.random()*(40-1+1));
            var valC = indexC%10 === 0 ? 10 : indexC%10;

            if(valA+valB+valC == cible && aTemp.indexOf(indexA)<0 && aTemp.indexOf(indexB)<0 && aTemp.indexOf(indexC)<0) {
                aTemp.push(indexA,indexB,indexC);
                aIndex[i].push(indexA,indexB,indexC);
                aIndexSoluce[i].push(indexA,indexB,indexC);
            }
            else{
                j--;
            }
        }
    }
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigneA);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.append(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    // 
    exo.btnValider.css({left:600});
    //
    exo.str.info.msgFauxQuestionSuivante = "Faux. Voici une solution.";
    exo.str.info.msgRien = "Il faut placer toutes les cartes.";
    // les tapis
    var tapis,empreinteCible;
    conteneurTapis = disp.createEmptySprite();
    conteneurTapis.css({left:280,top:20,width:250,height:280});
    exo.blocAnimation.append(conteneurTapis);
    aEmpreinteCible=[];
    for(var j=0;j<3;j++){
        tapis = disp.createEmptySprite();
        conteneurTapis.append(tapis);
        tapis.css({backgroundColor:"#00A500",width:250,height:120,left:0,top:130*j,boxShadow:"3px 3px 3px #aaa"});
        for(var k=0;k<3;k++){
            empreinteCible = disp.createEmptySprite();
            aEmpreinteCible.push(empreinteCible);
            tapis.append(empreinteCible);
            empreinteCible.css({width:250/3,height:120,left:k*250/3});
            empreinteCible.droppable({
                drop:gestionDropCarte
            });
        }
    }
    // les cartes
    var aIndexQuestion = aIndex[exo.indiceQuestion];
    util.shuffleArray(aIndexQuestion);
    conteneurEmpreinteOrigine = disp.createEmptySprite();
    exo.blocAnimation.append(conteneurEmpreinteOrigine);
    conteneurEmpreinteOrigine.css({left:20,top:55,width:225,height:315});
    var empreinteOrigine,carte;
    for(var i=0;i<9;i++){
        empreinteOrigine = disp.createEmptySprite();
        empreinteOrigine.css({width:70,height:100,left:75*(i%3),background:"#eee",top:105*Math.floor(i/3)});
        conteneurEmpreinteOrigine.append(empreinteOrigine);
        empreinteOrigine.droppable({
            drop:gestionDropCarte
        });
        carte = disp.createMovieClip(exo,"jeuComplet",70,100,500);
        carte.conteneur.addClass("carte");
        empreinteOrigine.append(carte.conteneur);
        carte.gotoAndStop(aIndexQuestion[i]);
        var valeur = aIndexQuestion[i]%10 === 0 ? 10 : aIndexQuestion[i]%10;
        carte.conteneur.data("valeur",valeur);
        carte.conteneur.draggable({
            start:gestionDragCarte,
            revert:"invalid",
            stack:".carte"
        });
    }

    // la consigne
    cible = aCible[exo.indiceQuestion];
    var labelConsigne = disp.createTextLabel(exo.txt.consigneB);
    labelConsigne.css({width:160,fontSize:18,fontWeight:"bold",textAlign:"center",left:560,top:100});
    exo.blocAnimation.append(labelConsigne);
    var labelCible = disp.createTextLabel(String(cible));
    labelCible.css({fontSize:48,fontWeight:"bold",textAlign:"center"});
    exo.blocAnimation.append(labelCible);
    var posX = labelConsigne.position().left+(labelConsigne.width()-labelCible.width())/2;
    var posY = labelConsigne.height()+labelConsigne.position().top + 20;
    labelCible.css({left:posX,top:posY});
    
    function gestionDragCarte(e,ui){
        var carte = $(ui.helper);
        carte.draggable({revert:"invalid"});
    }

    function gestionDropCarte(e,ui){
        var empreinte = $(e.target);
        var carte = $(ui.helper);
        if(empreinte.children().length === 0){
            empreinte.append(carte);
            carte.css({
                left:(empreinte.width()-carte.width())/2,
                top:(empreinte.height()-carte.height())/2,
            });
        }
        else{
            carte.draggable({revert:true});
        }
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    var testToutesPlacee=0;
    var totalTapisA = 0;
    var totalTapisB = 0;
    var totalTapisC = 0;
    for(var i=0;i<9;i++){
        var carte = aEmpreinteCible[i].children();
        if( carte.length !== 0){
            testToutesPlacee++;
            if(i<3){
                totalTapisA+=carte.data().valeur;
            }
            else if(i>=3 && i<6){
                totalTapisB+=carte.data().valeur;
            }
            else if(i>=6){
                totalTapisC+=carte.data().valeur;
            }
        }
    }
    if(testToutesPlacee < 9){
        return "rien";
    }
    else {
        for(var j=0;j<9;j++){
            aEmpreinteCible[j].children().draggable("destroy");
        }
        if (totalTapisA == cible && totalTapisB == cible && totalTapisC == cible){
            return "juste";
        }
        else {
            return "faux";
        }
    }
};

// Correction (peut rester vide)
exo.corriger = function() {
    exo.cacherBtnSuite();
    // on deplace la reponse de l'eleve
    conteneurEmpreinteOrigine.remove();
    for(var i=0;i<3;i++){
        conteneurTapis.children().css({background:"#ff0000"});
    }
    conteneurTapis.transition({scale:0.8,opacity:0.5});
    conteneurTapis.transition({x:'-340px'},400,function(){
        // on affiche la solution
        var conteneurCorrection, tapis, empreinteCible;
        conteneurCorrection = disp.createEmptySprite();
        conteneurCorrection.css({left:280,top:450,width:250,height:280});
        exo.blocAnimation.append(conteneurCorrection);
        var aIndexSoluceQuestion = aIndexSoluce[exo.indiceQuestion];
        for(var j=0;j<3;j++){
            tapis = disp.createEmptySprite();
            conteneurCorrection.append(tapis);
            tapis.css({backgroundColor:"#00A500",width:250,height:120,left:0,top:130*j});
            var carte;
            for(var k=0;k<3;k++){
                carte = disp.createMovieClip(exo,"jeuComplet",70,100,500);
                carte.gotoAndStop(aIndexSoluceQuestion[3*j+k]);
                tapis.append(carte.conteneur);
                carte.conteneur.css({left:15 + 75*(k%3),top:10});
            }
        }
        conteneurCorrection.transition({y:'-415px',delay:400},400,function(){
            exo.afficherBtnSuite();
        });
    });
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:1,
        nom:"totalQuestion",
        texte:"Nombre de questions (maximum 9) : "
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));