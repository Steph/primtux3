<?php
header('Access-Control-Allow-Origin: http://rallye-calculatice.net');
echo json_encode(array(
"bouton"=>array(
    "commencer"=>"Commencer",
    "valider"=>"Valider",
    "suite"=>"Suite",
    "recommencer"=>"Recommencer",
    "suivant"=>"Suivant",
    "resultat"=>"Résultats",
    "tester"=>"Valider"
    )
,
"info"=>array(
    "msgJuste"=>"Bravo !",
    "msgFaux"=>"Faux mais tu as encore un essai.",
    "msgFauxQuestionSuivante"=>"Faux.<br>Regarde la correction.",
    "msgFin"=>"Exercice terminé",
    "msgFinQuestion"=>"Question terminée.",
    "msgRien"=>"Il faut donner une réponse.",
    "msgTempsDepasseJuste"=>"Temps dépassé !",
    "msgTempsDepasseFaux"=>"Temps dépassé !<br>Regarde la correction."
    )
,
"misc"=>array(
    "score"=>"score",
    "essai"=>"essai",
    "temps"=>"temps",
    "sur"=>"sur",
    "min"=>"minute(s)",
    "sec"=>"seconde(s)",
    "resultat"=>"résultats",
    "option"=>"options",
    "chargement"=>"Chargement en cours, patientez ..."
    )
)
)
?>