﻿package code {
	public class CalcScore
	{
		public var score:uint;
		public var scorePremierEssai:uint;
		
		public function CalcScore()
		{
			score=0;
			scorePremierEssai=0;
		}
		
		public function incrementerScore(n:uint)
		{
			score+=n;
		}
		
		public function incrementerScorePremierEssai(n:uint)
		{
			scorePremierEssai+=n;
		}
	}
}